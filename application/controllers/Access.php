<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Getdat_model');
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->db_users = $this->load->database('db_users', TRUE);
	}

	public function index()
	{
		$data['title'] = 'Role Access';
		$this->db_users = $this->load->database('db_users', TRUE);
		$role_id = $this->input->get('role_id');
		$data['role'] = $this->db_users->get_where('user_role', ['id' => $role_id])->row_array();		
		$data['menu'] = $this->db->get('user_menu')->result_array();
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/rl',$data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}

	public function ajax()
	{
		$role_id = $this->input->post('roleId');
		$menu_id = $this->input->post('menuId');
		$data = [
				'role_id' => $role_id,
				'menu_id' => $menu_id
		];

		$result = $this->db->get_where('user_access_menu', $data);
		
		if ($result->num_rows() < 1) {
			$this->db->insert('user_access_menu', $data);
		} else {
			$this->db->delete('user_access_menu', $data);
		}

		$this->session->set_flashdata("Pesan", "Akses berhasil diganti");
	}
		public function delete()
	{
		$role_id = $this->input->get('role_id');
		$this->db_users->delete('user_role', ['id' => $role_id]);
		$this->session->set_flashdata("Pesan", "Role success delete");
		redirect('role','refresh');

	}




}

/* End of file Access.php */
/* Location: ./application/controllers/Access.php */