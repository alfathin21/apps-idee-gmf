<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Batch extends CI_Controller {
	public function index()
	{
		$this->load->model('Getdat_model');	
		error_reporting(0);
		if (isset($_POST['tekan'])) {
			$parse   = $this->Getdat_model->getEvent();
			$response = [];
			$response2 = [];
			foreach ($parse as $key ) 
			{
				if ($this->Getdat_model->cektdam($key['p_id']) == 0) {
					$h['p_id'] = $key['p_id'];
					$h['EventID'] = $key['p_eventid'];
					$h['FltNo'] = $key['FltNo'];
					$h['DateDeptEvent'] = $key['DateDeptEvent'];
					$h['ACTypeOrig'] = $key['ACTypeOrig'];
					$h['ACRegOrig'] = $key['ACRegOrig'];
					$h['atachap'] = $key['atachap'];
					$h['Note'] = $key['Note'];
					$h['p_target_date'] = $key['p_target_date'];
					$h['Preanalis'] = $key['Preanalis'];
					$h['p_description'] = $key['p_description'];
					$h['insert_date'] = date('Y-m-d H:i:s');
					$h['status'] = 'OPEN';
					array_push($response, $h);
				} else {
					$b['p_id'] = $key['p_id'];
					$b['EventID'] = $key['p_eventid'];
					$b['FltNo'] = $key['FltNo'];
					$b['DateDeptEvent'] = $key['DateDeptEvent'];
					$b['ACTypeOrig'] = $key['ACTypeOrig'];
					$b['ACRegOrig'] = $key['ACRegOrig'];
					$b['atachap'] = $key['atachap'];
					$b['Note'] = $key['Note'];
					$b['p_target_date'] = $key['p_target_date'];
					$b['Preanalis'] = $key['Preanalis'];
					$b['p_description'] = $key['p_description'];
					$b['insert_date'] = date('Y-m-d H:i:s');
					array_push($response2, $b);
				}
			}

			if (count($response) > 1) {
				$this->db->insert_batch('problem_definition_tdam',$response);
			} else {
				$this->db->update_batch('problem_definition_tdam', $response2, 'p_id');
			}
 			$this->session->set_flashdata("Pesan", "Successful Synchronization");
 			redirect('Sync','refresh');
		}
	}
	public function batchor()
	{
	    $this->load->model('Getdat_model');	
		$parse   = $this->Getdat_model->getEvent();
		$response = [];
		$response2 = [];
		foreach ($parse as $key ) 
		{
			if ($this->Getdat_model->cektdam($key['p_id']) == 0) {
				$h['p_id'] = $key['p_id'];
				$h['EventID'] = $key['p_eventid'];
				$h['FltNo'] = $key['FltNo'];
				$h['DateDeptEvent'] = $key['DateDeptEvent'];
				$h['ACTypeOrig'] = $key['ACTypeOrig'];
				$h['ACRegOrig'] = $key['ACRegOrig'];
				$h['atachap'] = $key['atachap'];
				$h['Note'] = $key['Note'];
				$h['p_target_date'] = $key['p_target_date'];
				$h['Preanalis'] = $key['Preanalis'];
				$h['p_description'] = $key['p_description'];
				$h['insert_date'] = date('Y-m-d H:i:s');
				$h['status'] = 'OPEN';
				array_push($response, $h);
			} else {
				$b['p_id'] = $key['p_id'];
				$b['EventID'] = $key['p_eventid'];
				$b['FltNo'] = $key['FltNo'];
				$b['DateDeptEvent'] = $key['DateDeptEvent'];
				$b['ACTypeOrig'] = $key['ACTypeOrig'];
				$b['ACRegOrig'] = $key['ACRegOrig'];
				$b['atachap'] = $key['atachap'];
				$b['Note'] = $key['Note'];
				$b['p_target_date'] = $key['p_target_date'];
				$b['Preanalis'] = $key['Preanalis'];
				$b['p_description'] = $key['p_description'];
				$b['insert_date'] = date('Y-m-d H:i:s');
				array_push($response2, $b);
			}
		}

		if (count($response) > 1) {
			$this->db->insert_batch('problem_definition_tdam',$response);
		} else {
			$this->db->update_batch('problem_definition_tdam', $response2, 'p_id');
		}
	}
}

/* End of file Batch.php */
/* Location: ./application/controllers/Batch.php */

?>