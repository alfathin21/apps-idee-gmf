<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('Getdat_model');	
		error_reporting(0);
	}
	public function index()
	{
		$b = time();
		$hour = date("G",$b);
		if ($hour>=0 && $hour<=11)
		{
		$data['ucapan']= "Selamat Pagi";
		}
		elseif ($hour >=12 && $hour<=14)
		{
		$data['ucapan']= "Selamat Siang ";
		}
		elseif ($hour >=15 && $hour<=17)
		{
		$data['ucapan']= "Selamat Sore ";
		}
		elseif ($hour >=17 && $hour<=18)
		{
		$data['ucapan']= "Selamat Petang ";
		}
		elseif ($hour >=19 && $hour<=23)
		{
		$data['ucapan']= "Selamat Malam ";
		}
		$data['title'] = 'Dashboard';
		$a =  date('Y');
		$data['tahun'] = $a;
		$data['eval'] = $this->Getdat_model->eval($a);
		$data['problem'] = $this->Getdat_model->problem($a);
		$data['user'] = $this->Getdat_model->getusers($a);
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/dashboard',$data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}

	public function chartter()
	{

// untuk evaluasi jenis TDAM
// UNTUK TER 1 
		$tahun =  date('Y');
		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' AND status != 'HIDDEN' ")->num_rows();

		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN'")->num_rows();

// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE UIC = 'TER-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter5_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// akhir jenis TDAM


// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN'")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter5_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter5_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI



// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING'")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter5_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter5_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
			"ter5_tdam" => $ter5_tdam,
			"ter5_tdam_open" => $ter5_tdam_open,
			"ter5_tdam_progress" => $ter5_tdam_progress,
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,
			"ter5_erc" => $ter5_erc,
			"ter5_erc_open" => $ter5_erc_open,
			"ter5_erc_progress" => $ter5_erc_progress,
	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
			"ter5_type" => $ter5_type,
			"ter5_type_open" => $ter5_type_open,
			"ter5_type_progress" => $ter5_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress,
			"ter5_others" => $ter5_others,
			"ter5_others_open" => $ter5_others_open,
			"ter5_others_progress" => $ter5_others_progress
		];
		
		echo json_encode($response);
	}

	public function charttea()
	{
		
// untuk evaluasi jenis TDAM
// UNTUK TER 1 

$tahun =  date('Y');

		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
	
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,

	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress
		];
		
		echo json_encode($response);
	}


	public function chartter2()
	{

// untuk evaluasi jenis TDAM
// UNTUK TER 1 
	$tahun = $this->input->post('tahun');
		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' AND status != 'HIDDEN' ")->num_rows();

		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN'")->num_rows();

// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();

		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE UIC = 'TER-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' AND status != 'HIDDEN'")->num_rows();
		$ter5_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// akhir jenis TDAM


// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN'")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' AND status != 'HIDDEN'")->num_rows();
		$ter5_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN'")->num_rows();
		$ter5_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI



// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING'")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter5_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 5
		$ter5_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter5_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter5_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' and  UIC = 'TER-5' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// AKHIR JENIS ERC SRI

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
			"ter5_tdam" => $ter5_tdam,
			"ter5_tdam_open" => $ter5_tdam_open,
			"ter5_tdam_progress" => $ter5_tdam_progress,
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,
			"ter5_erc" => $ter5_erc,
			"ter5_erc_open" => $ter5_erc_open,
			"ter5_erc_progress" => $ter5_erc_progress,
	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
			"ter5_type" => $ter5_type,
			"ter5_type_open" => $ter5_type_open,
			"ter5_type_progress" => $ter5_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress,
			"ter5_others" => $ter5_others,
			"ter5_others_open" => $ter5_others_open,
			"ter5_others_progress" => $ter5_others_progress
		];
		
		echo json_encode($response);
	}

public function charttea2()
	{
		
// untuk evaluasi jenis TDAM
// UNTUK TER 1 

$tahun = $this->input->post('tahun');

		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TEA-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
	
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,

	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress
		];
		
		echo json_encode($response);
	}



























	public function chartted2()
	{
		$tahun = $this->input->post('tahun');
// untuk evaluasi jenis TDAM
// UNTUK TER 1 
		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND  UIC = 'TED-1' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
	
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,

	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress
		];
		
		echo json_encode($response);
	}



































	public function chartted()
	{
		$tahun =  date('Y');
// untuk evaluasi jenis TDAM
// UNTUK TER 1 
		$ter1_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND  UIC = 'TED-1' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_tdam = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM'AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_tdam_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TDAM' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS ERC SRI
// UNTUK TER 1 
		$ter1_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_erc = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI'AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_erc_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'ERC - SRI' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();


// JENIS ERC TYPE MEETING
// UNTUK TER 1 
		$ter1_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 2
		$ter2_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 3
		$ter3_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_type = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING'AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING' and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_type_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and  evaluationtype = 'TYPE MEETING' and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();



// JENIS OTHERS MRC DE-DT Safety Meeting
// UNTUK TER 1 
		$ter1_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter1_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-1' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 2
		$ter2_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter2_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-2' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();
// UNTUK TER 3
		$ter3_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter3_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-3' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

// UNTUK TER 4
		$ter4_others = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE')AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_open = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'PROGRESS' OR status = 'OPEN') AND status != 'HIDDEN' ")->num_rows();
		$ter4_others_progress = $this->db->query("SELECT evaluationtype FROM problem_definition WHERE YEAR(create_at) = '$tahun' AND UIC = 'TED-4' and (evaluationtype = 'OTHERS' or evaluationtype = 'MRC' or evaluationtype = 'DE-DT' or evaluationtype = 'Safety Meeting' or evaluationtype = 'PERFORMANCE') and (status = 'REVIEW' OR status = 'CLOSE') AND status != 'HIDDEN' ")->num_rows();

		$response =
		[
			"ter1_tdam" => $ter1_tdam,
			"ter1_tdam_open" => $ter1_tdam_open,
			"ter1_tdam_progress" => $ter1_tdam_progress,
			"ter2_tdam" => $ter2_tdam,
			"ter2_tdam_open" => $ter2_tdam_open,
			"ter2_tdam_progress" => $ter2_tdam_progress,
			"ter3_tdam" => $ter3_tdam,
			"ter3_tdam_open" => $ter3_tdam_open,
			"ter3_tdam_progress" => $ter3_tdam_progress,
			"ter4_tdam" => $ter4_tdam,
			"ter4_tdam_open" => $ter4_tdam_open,
			"ter4_tdam_progress" => $ter4_tdam_progress,
	
	// KASIH BATAS BOY BIAR ENGGA PUSING
			"ter1_erc" => $ter1_erc,
			"ter1_erc_open" => $ter1_erc_open,
			"ter1_erc_progress" => $ter1_erc_progress,
			"ter2_erc" => $ter2_erc,
			"ter2_erc_open" => $ter2_erc_open,
			"ter2_erc_progress" => $ter2_erc_progress,
			"ter3_erc" => $ter3_erc,
			"ter3_erc_open" => $ter3_erc_open,
			"ter3_erc_progress" => $ter3_erc_progress,
			"ter4_erc" => $ter4_erc,
			"ter4_erc_open" => $ter4_erc_open,
			"ter4_erc_progress" => $ter4_erc_progress,

	//
			"ter1_type" => $ter1_type,
			"ter1_type_open" => $ter1_type_open,
			"ter1_type_progress" => $ter1_type_progress,
			"ter2_type" => $ter2_type,
			"ter2_type_open" => $ter2_type_open,
			"ter2_type_progress" => $ter2_type_progress,
			"ter3_type" => $ter3_type,
			"ter3_type_open" => $ter3_type_open,
			"ter3_type_progress" => $ter3_type_progress,
			"ter4_type" => $ter4_type,
			"ter4_type_open" => $ter4_type_open,
			"ter4_type_progress" => $ter4_type_progress,
	// 
			"ter1_others" => $ter1_others,
			"ter1_others_open" => $ter1_others_open,
			"ter1_others_progress" => $ter1_others_progress,
			"ter2_others" => $ter2_others,
			"ter2_others_open" => $ter2_others_open,
			"ter2_others_progress" => $ter2_others_progress,
			"ter3_others" => $ter3_others,
			"ter3_others_open" => $ter3_others_open,
			"ter3_others_progress" => $ter3_others_progress,
			"ter4_others" => $ter4_others,
			"ter4_others_open" => $ter4_others_open,
			"ter4_others_progress" => $ter4_others_progress
		];
		
		echo json_encode($response);
	}


	public function tahun()
	{
		$tahun = htmlspecialchars($this->input->post('tahun'));
		$angkat_data = $this->Getdat_model->angkat($tahun);
		echo json_encode($angkat_data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */