<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Delete extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
		$this->load->model('Problem_model');
		$this->load->model('Solution_model');
		error_reporting(0);
	}
	public function index()
	{
		$data['title'] = 'Problem Definition Delete';
		$data['histori']  =  $this->Getdat_model->histori();
		$this->load->view('template/head',$data);
 		$this->load->view('template/side',$data);
 		$this->load->view('page/delete',$data);
 		$this->load->view('template/fo2');
 		$this->load->view('template/foot');			
	}
	public function dl_histori()
	{
		$number = htmlspecialchars($this->input->post('number'));
		$status = htmlspecialchars($this->input->post('status'));
		$this->db->SET('status', $status);
		$this->db->where('number', $number);
		$this->db->update('problem_definition');
		$this->session->set_flashdata("Pesan", "Problem is update");
		echo json_encode('success');
	} 
	public function get_data_problem()
	{
		$list = $this->Problem_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$cki = "return confirm ('Delete problem ?');";
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['number'];
			$row[] = '<a  onclick="'.$cki.'" href="'.base_url('pd/dp/').$field['number'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Problem_model->count_all(),
			"recordsFiltered" => $this->Problem_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}
	public function get_data_solution()
	{
		$list = $this->Solution_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$cki = "return confirm ('Delete solution ?');";
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['numbersolution'];
			$row[] = $field['solution'];
			$row[] = $field['status'].' %';
			$row[] = date('d F Y, h:i:s A', strtotime($field['last_update']));
			$row[] = '<a  onclick="'.$cki.'" href="'.base_url('delete/soldel/').$field['numbersolution'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Solution_model->count_all(),
			"recordsFiltered" => $this->Solution_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}


	public function soldel($number)
	{
		if ($number == '' or empty($number)) {
			redirect('delete','refresh');
		}

		$this->Solution_model->delete($number);
		$this->session->set_flashdata("Pesan", "Solution delete !");
		redirect('delete','refresh');
	}
  
}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */