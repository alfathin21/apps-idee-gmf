<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Draft extends CI_Controller {

	public function index()
 	{
 		is_log_in();
 		cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
		$data['title'] = 'List Draft';
		$user = $this->session->userdata('nopeg');
		$data['ld'] = $this->Getdat_model->getdraft($user);
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/ld', $data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
		error_reporting(0);
 	}
 	public function deleteattach()
	{
		$number = $this->input->post('number');
		$file 	= $this->input->post('file');
		$id_file = $this->input->post('ui');
		$out = $this->db->query("SELECT file from draft where number = '$number' ")->row_array();
    	$op = $out['file'];
    	$array_awal = explode(',',$file);
    	$array_db   = explode(',', $op);
    	$result=array_diff($array_db,$array_awal);
    	$update_file = implode('', $result);
    	$save = $update_file.',';
    	$this->db->set('file', $save);
    	$this->db->where('number',$number);
    	$this->db->update('draft');
    	$this->db->delete('attach', ['token' => $file]);
    		if(file_exists($file=FCPATH.'/files/'.$id_file)){
			unlink($file);
		}
    	echo json_encode($id_file);
	}
}

/* End of file Access.php */
/* Location: ./application/controllers/Access.php */