<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Follow extends CI_Controller {

	public function index()
	{
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
		$email = strtolower($this->session->userdata('email'));
		$number = $this->input->get('number');
		$data['title'] = 'Add Solutions';
		$cek_awal = $this->Getdat_model->getevaluation($number,$email);
	
			$data['detail'] = $cek_awal;
			$this->load->view('template/head', $data);
			$this->load->view('template/side');
			$this->load->view('page/follow');
			$this->load->view('template/fo2');
			$this->load->view('template/foot');
		
	}

}

/* End of file Follow.php */
/* Location: ./application/controllers/Follow.php */