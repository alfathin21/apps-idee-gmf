<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Log extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
 		$this->load->model('Log_model');
 		$this->load->model('Action_model');
 		error_reporting(0);
	}

	public function index()
	{
		$data['title'] = 'Log User Login';
		$data['action'] = $this->Getdat_model->getaction();
 		$this->load->view('template/head', $data);
 		$this->load->view('template/side', $data);
 		$this->load->view('page/log');
 		$this->load->view('template/fo2');
 		$this->load->view('template/foot');		
	}

	public function get_data_user()
	{
		$list = $this->Log_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['id'];
			$row[] = $field['nama'];
			$row[] = date('d F Y h:i:s', strtotime($field['datetime']));
			$row[] = $field['log_description'];
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Log_model->count_all(),
			"recordsFiltered" => $this->Log_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

    public function get_data_action()
	{
		$list = $this->Action_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['numberproblem'];
			$row[] = $field['user'];
			$row[] = $field['action'];
			$row[] = date('d F Y h:i:s', strtotime($field['date']));
			$row[] = $field['description'];
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Action_model->count_all(),
			"recordsFiltered" => $this->Action_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}


}

/* End of file Log.php */
/* Location: ./application/controllers/Log.php */