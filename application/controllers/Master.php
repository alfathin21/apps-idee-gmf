<?php 
class Master extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
	}
	public function index(){
		$data['title'] = 'Master Data';
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/master');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');	
	}
	public function list_uic()
	{
			$query = $this->db->get("uic_master")->result_array();
		echo json_encode($query);
	}
	public function list_user()
	{
		$sv = $this->User_model->list_user();
		echo json_encode($sv);
	}
	public function list_alert()
	{
		$sv = $this->db->get('alert_master')->result_array();
		echo json_encode($sv);
	}
	public function list_operator()
	{
		$sv = $this->db->get('operator_master')->result_array();
		echo json_encode($sv);
	}
	public function list_actype()
	{
		$sv = $this->db->get('actype_master')->result_array();
		echo json_encode($sv);
	}
	public function list_evaluation()
	{
		$sv = $this->db->get('evaltype_master')->result_array();
		echo json_encode($sv);
	}
	
}
?>