<?php 
 defined('BASEPATH') OR exit('No direct script access allowed');
 class Pd extends CI_Controller {
 	public function __construct()
 	{
 		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
 		is_log_in();
 		// cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
 		$this->load->library('phpmailer_lib');
 		error_reporting(0);
 		
 	}
 	public function index()
 	{
 		$data['title'] = 'New Issue';
 		$data['te'] = $this->Getdat_model->gette();
 		$data['uic'] = $this->db->query("SELECT uic from uic_master WHERE status = 'OPEN' ")->result_array();
 		$data['alert'] = $this->db->query("SELECT alert from alert_master WHERE status = 'OPEN' ")->result_array();
 		$data['evaltype'] = $this->db->query("SELECT evaltype from evaltype_master WHERE status = 'OPEN' ")->result_array();
 		$data['operator'] = $this->db->query("SELECT operator, value from operator_master WHERE status = 'OPEN' ")->result_array();
 		$data['actype'] = $this->db->query("SELECT actype, value from actype_master WHERE status = 'OPEN' ")->result_array();
 		$this->load->view('template/head', $data);
 		$this->load->view('template/side', $data);
 		$this->load->view('page/pd', $data);
 		$this->load->view('template/fo2');
 		$this->load->view('template/foot',$data);	
 	}
 	public function s_d()
 	{
 		$data['title'] = 'Search Draft';
 		$nopeg = htmlspecialchars($this->input->post('nopeg'));
 		$number_draft = htmlspecialchars($this->input->post('number_draft'));
 		if (empty($nopeg || $number_draft)) {
 			$this->session->set_flashdata("Pesan", "Please Insert Number Draft !");
 			redirect('pd','refresh');
 		} 
 		$get = $this->Getdat_model->draft($number_draft, $nopeg);
 		if ($get == 0) {
 			$this->session->set_flashdata("Pesan", "Number Draft is not found !");
 			redirect('pd','refresh');
 		}else{
 			$data['re'] = $get;
 			$data['te'] = $this->Getdat_model->gette();
 			$this->load->view('template/head', $data);
 			$this->load->view('template/side', $data);
 			$this->load->view('page/ds', $data);
 			$this->load->view('template/fo2');
 			$this->load->view('template/foot',$data);
 		}

 	}
 	public function generate()
 	{
 		$actype = htmlspecialchars($this->input->post('actype'));
 		$operator = htmlspecialchars($this->input->post('operator'));
 		$nilai = $this->db->query("SELECT max(urutan) as maxKode FROM generatenumber where operator = '$operator' and actype = '$actype' ")->result_array();
 		$kode = $nilai[0]['maxKode'];
 		$noUrut = (int) substr($kode,0,5);
 		$noUrut++;
 		$response = sprintf("%05s", $noUrut);
 		echo json_encode($response);
 	}


 	public function savedraft()
 	{
 		$number = $this->input->post('number');
 		$operator = htmlspecialchars($this->input->post('operator'));
 		$actype = htmlspecialchars($this->input->post('actype'));
 		$crossreff = htmlspecialchars($this->input->post('crossreff'));
 		$evaluation = htmlspecialchars($this->input->post('evaluation'));
 		$editor = html_entity_decode($this->input->post('editor'));
 		$urutan = htmlspecialchars($this->input->post('urutan'));
 		$alert = htmlspecialchars($this->input->post('alert'));
 		$acreg = htmlspecialchars($this->input->post('acreg'));
 		$ata = htmlspecialchars($this->input->post('ata'));
 		$system = htmlspecialchars($this->input->post('system'));
 		$problem_title = htmlspecialchars($this->input->post('problem_title'));
 		$detail = html_entity_decode($this->input->post('detail'));
 		$uic = htmlspecialchars($this->input->post('uic'));
 		$target_date = htmlspecialchars($this->input->post('target_date'));
 		$releated = htmlspecialchars( $this->input->post('releated'));
 		$file_gabung = htmlspecialchars( $this->input->post('file_gabung'));
 		$cc = $this->input->post('cc');
 		$pc = $this->input->post('pc');
 		@$h_cc = count($cc);
 		@$h_pc = count($pc);
 		$user = $this->session->userdata('nopeg');
 		$token = $this->Getdat_model->gettoken($user,$number);
 		$h_token = count($token);
 		if (!isset($_POST['simpan_draft']) and !isset($_POST['update_draft']) and !isset($_POST['assign']) ) {
 			$this->session->set_flashdata("Pesan", "Access forbiden !");	
 			redirect('pd','refresh');
 		}


 		if (isset($_POST['simpan_draft'])) {

 			if ($h_token == 0) {
 				$ts= '';
 			} else {
 				foreach ($token as $key) 
 				{
 					$out[] = $key['token'].',';
 				}
 				$ts = implode("",$out);
 			}
 			if ($h_pc > 0 ) {
			// Jika PIC lebih dari satu
 				for ($i=0; $i < $h_pc ; $i++) { 
 					$pic[] = $pc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => strtolower($pc[$i]),
 						'send' => date('Y-m-d'),
 						'status' => 'PIC'
 					];
 					$this->Getdat_model->save_pic($data3);
 				}
 				$p_i  = implode(',',$pic);
 				$pic_gabung = str_replace(',,',',', $p_i);

 				$pic_save = strtolower($pic_gabung);
 			} else if ($h_pc == 1) {	
			// jika PIC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => strtolower($pc),
 					'send' => date('Y-m-d'),
 					'status' => 'PIC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$pic_save = strtolower($pc);
 			}
 			if ($h_cc > 0 ) { 
			// Jika CC lebih dari satu
 				for ($i=0; $i < $h_cc ; $i++) { 
 					$c_c[] = $cc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => strtolower($cc[$i]),
 						'send' => date('Y-m-d'),
 						'status' => 'CC'
 					];
 					$this->Getdat_model->save_pic($data3);					
 				}
 				$c_i  = implode(',',$c_c);
 				$cc_gabung = str_replace(',,',',', $c_i);
 				$cc_save = strtolower($cc_gabung);
 			} else if ($h_cc == 1) {	
			// jika CC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => $cc,
 					'send' => date('Y-m-d'),
 					'status' => 'CC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$cc_save = strtolower($cc);
 			} else {
 				$cc_save = '';
 			}
 			if ($ts == '') {
 				if ($file_gabung == '') {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'file'	=> $file_gabung,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}
 			} else {

 				if (empty($file_gabung)) {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $file_gabung.$ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}

 				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
 			}

 			$data2 = 
 			[
 				'rnumber' => $number,
 				'revisionnumber'=> $number,
 				'dateinsert' => date('Y-m-d'),
 				'user'	=> $this->session->userdata('nopeg')
 			];
 			$data4 = 
 			[
 				'number' 	=> $number,
 				'actype'	=> $actype,
 				'operator'	=> $operator,
 				'urutan'	=> $urutan
 			];
 			$action = 
 			[
 				'numberproblem' 	 => $number,
 				'user' 	 => $this->session->userdata('nama'),
 				'date'	 => date('Y-m-d H:i:s'),
 				'action' => 'save to draft',
 				'description'	=> 'Success save to draft table'
 			];
 			$save_number = $this->Getdat_model->save_number($data4);
 			$save_draft = $this->Getdat_model->save_draft($data);
 			$save_revisi = $this->Getdat_model->save_revisi($data2);
 			$this->Getdat_model->saveaction($action);
 			if (!$save_draft) 
 			{
 				$this->session->set_flashdata("Pesan", "Draft not is Saved !");	
 				redirect('pd','refresh');
 			}
 			$this->session->set_flashdata("Pesan", "Draft is Saved !");
 			redirect('pd','refresh');
 		}



 		if (isset($_POST['update_draft'])) {

 			if ($h_token == 0) {
 				$ts= '';
 			} else {
 				foreach ($token as $key) 
 				{
 					$out[] = $key['token'].',';
 				}
 				$ts = implode("",$out);
 			}
 			if ($h_pc > 0 ) {
			// Jika PIC lebih dari satu
 				for ($i=0; $i < $h_pc ; $i++) { 
 					$pic[] = $pc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => strtolower($pc[$i]),
 						'send' => date('Y-m-d'),
 						'status' => 'PIC'
 					];
 					$this->Getdat_model->save_pic($data3);
 				}
 				$p_i  = implode(',',$pic);
 				$pic_gabung = str_replace(',,',',', $p_i);

 				$pic_save = strtolower($pic_gabung);
 			} else if ($h_pc == 1) {	
			// jika PIC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => strtolower($pc),
 					'send' => date('Y-m-d'),
 					'status' => 'PIC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$pic_save = strtolower($pc);
 			}
 			if ($h_cc > 0 ) { 
			// Jika CC lebih dari satu
 				for ($i=0; $i < $h_cc ; $i++) { 
 					$c_c[] = $cc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => strtolower($cc[$i]),
 						'send' => date('Y-m-d'),
 						'status' => 'CC'
 					];
 					$this->Getdat_model->save_pic($data3);					
 				}
 				$c_i  = implode(',',$c_c);
 				$cc_gabung = str_replace(',,',',', $c_i);
 				$cc_save = strtolower($cc_gabung);
 			} else if ($h_cc == 1) {	
			// jika CC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => strtolower($cc),
 					'send' => date('Y-m-d'),
 					'status' => 'CC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$cc_save = strtolower($cc);
 			} else {
 				$cc_save = '';
 			}
 			if ($ts == '') {
 				if ($file_gabung == '') {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'file'	=> $file_gabung,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}
 			} else {

 				if (empty($file_gabung)) {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $file_gabung.$ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}

 				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
 			}

 			$data2 = 
 			[
 				'rnumber' => $number,
 				'revisionnumber'=> $number,
 				'dateinsert' => date('Y-m-d'),
 				'user'	=> $this->session->userdata('nopeg')
 			];
 			$data4 = 
 			[
 				'number' 	=> $number,
 				'actype'	=> $actype,
 				'operator'	=> $operator,
 				'urutan'	=> $urutan
 			];
 			$action = 
 			[
 				'numberproblem' 	 => $number,
 				'user' 	 => $this->session->userdata('nama'),
 				'date'	 => date('Y-m-d H:i:s'),
 				'action' => 'save to draft',
 				'description'	=> 'Success save to draft table'
 			];
 			$save_number = $this->Getdat_model->save_number($data4);
 			$save_draft = $this->Getdat_model->update_draft($data);
 			$save_revisi = $this->Getdat_model->save_revisi($data2);
 			$this->Getdat_model->saveaction($action);
 			$this->session->set_flashdata("Pesan", "Draft is Update !");
 			redirect('draft','refresh');
 		}


 		
 		if (isset($_POST['assign'])) {
 			date_default_timezone_set("Asia/Jakarta");
 			if ($h_token == 0) {
 				$ts= '';

 			} else {
 				foreach ($token as $key) 
 				{ 				
 					$out[] = $key['token'].',';
 				}
 				$ts = implode("",$out);
 			}
 			if ($h_pc > 0 ) {
		// Jika PIC lebih dari satu
 				for ($i=0; $i < $h_pc ; $i++) { 
 					$pic[] = $pc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => strtolower($pc[$i]),
 						'send' => date('Y-m-d'),
 						'status' => 'PIC'
 					];
 					$e_pic =  
 					[
 						'PIC' => strtolower($pc[$i])
 					]; 
 					$this->Getdat_model->save_pic($data3);
 				}
 				$p_i  = implode(',',$pic);
 				$pic_gabung = str_replace(',,',',', $p_i);
 				$pic_save = $pic_gabung;
 			} else if ($h_pc == 1) {	
			// jika PIC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => strtolower($pc),
 					'send' => date('Y-m-d'),
 					'status' => 'PIC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$pic_save = strtolower($pc);
 			}
 			if ($h_cc > 0 ) { 
			// Jika CC lebih dari satu
 				for ($i=0; $i < $h_cc ; $i++) { 
 					$c_c[] = $cc[$i].',';
 					$data3 = [
 						'numberproblem' => $number,
 						'user' => $cc[$i],
 						'send' => date('Y-m-d'),
 						'status' => 'CC'
 					];
 					$this->Getdat_model->save_pic($data3);					
 				}
 				$c_i  = implode(',',$c_c);
 				$cc_gabung = str_replace(',,',',', $c_i);
 				$cc_save = strtolower($cc_gabung);
 			} else if ($h_cc == 1) {	
			// jika CC hanya satu
 				$data3 = [
 					'numberproblem' => $number,
 					'user' => strtolower($cc),
 					'send' => date('Y-m-d'),
 					'status' => 'CC'
 				];
 				$this->Getdat_model->save_pic($data3);
 				$cc_save = strtolower($cc);
 			} else {
 				$cc_save = '';
 			}
 			if ($ts == '') {
 				if ($file_gabung == '') {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'file'	=> $file_gabung,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}
 			} else {
 				if (empty($file_gabung)) {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				} else {
 					$data = 
 					[
 						'number' => $number,
 						'crossreff' => $crossreff,
 						'evaluationtype' => $evaluation,
 						'operator' => $operator,
 						'actype' => $actype,
 						'alert' => $alert,
 						'acreg' => $acreg,
 						'ata' => $ata,
 						'systems' => $system,
 						'ProblemTitle' => $problem_title,
 						'DetailProblem' => $editor,
 						'PIC' => strtolower($pic_save),
 						'cc' => strtolower($cc_save),
 						'UIC' => $uic,
 						'TargetDate' => $target_date,
 						'status'	=> 'OPEN',
 						'file'		=> $file_gabung.$ts,
 						'remarks'	=> '-',
 						'releated'	=> $releated,
 						'user'		=> $this->session->userdata('nopeg'),
 						'create_at'	=> date('Y-m-d')
 					];
 				}

 				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
 			}
 			$data2 = 
 			[
 				'rnumber' => $number,
 				'revisionnumber'=> $number,
 				'dateinsert' => date('Y-m-d'),
 				'user'	=> $this->session->userdata('nopeg')
 			];
 			$data4 = 
 			[
 				'number' 	=> $number,
 				'actype'	=> $actype,
 				'operator'	=> $operator,
 				'urutan'	=> $urutan
 			];	
 			$action = 
 			[
 				'numberproblem' 	 => $number,
 				'user' 	 => $this->session->userdata('nama'),
 				'date'	 => date('Y-m-d H:i:s'),
 				'action' => 'save to problem definition',
 				'description'	=> 'Success save to problem definition'
 			];
 		    $email_penerima = [];
 			$cek_number = $this->Getdat_model->ceknumberdraft($number);
 			$user_email = strtolower($this->session->userdata('email'));
 			if ($cek_number == 1) {
 				$delete = $this->Getdat_model->delete_draft($number);
 				$save_draft = $this->Getdat_model->save_assign($data);
 				$save_revisi = $this->Getdat_model->save_revisi($data2);
 			} else {
 				$save_number = $this->Getdat_model->save_number($data4);
 				$save_draft = $this->Getdat_model->save_assign($data);
 				$save_revisi = $this->Getdat_model->save_revisi($data2);
 			}
 			if (!$save_draft) 
 			{
 				$this->session->set_flashdata("Pesan", "ASSIGN IS NOT SEND");	
 				redirect('pd','refresh');
 			}
 			if ($cc == '') {
 				$email_penerima =  $pc;
 				array_push($email_penerima,$user_email);

 			} else {
 				$email_penerima =  array_merge($pc,@$cc);
 				array_push($email_penerima,$user_email);
 			}
 			$mail = $this->phpmailer_lib->load();
 			$date = date('Y-m-d');
 			$ac = cekactype($actype);
 			$op = cekoperator($operator);
 			$pengirim = $this->session->userdata('nama');
 			$target = date('d F Y', strtotime($target_date));
 			for ($s=0; $s < count($email_penerima) ; $s++) { 
 				$mail->isSMTP();	
 				$mail->CharSet = 'UTF-8';
 				$mail->Host = 'mail.gmf-aeroasia.co.id';
 				$mail->Port = 25;
 				$mail->SMTPAuth = true;
 				$mail->Username = "reliability.management@gmf-aeroasia.co.id";
 				$mail->Password = "Aeroas1@";
 				$mail->setFrom('reliability.management@gmf-aeroasia.co.id', 'APPS IDEE');
 				$mail->Subject = 'Task Assignment:'.$evaluation.':'.$problem_title.'';
 				$mail->isHTML(true);  
 				$mail->addAddress(strtolower($email_penerima[$s]));
 				$mail->Body = '
 				<!doctype html>
 				<html>
 				<body>
 				<h4><b>Greetings!</b> </h4> </br>
 				<p>Dear Fellow engineer, you have been assigned a task:</p></br>
 				<body>
 				<div>
 				<div>
 				<div>
 				<ul>
 				<li>PROBLEM TITLE :  '.$problem_title.'</li>
 				<li>A/C TYPE  : '.$ac.'</li>
 				<li>OPERATOR : '.$op.'</li>
 				<li>ATA : '.$ata.'</li>
 				<li>PIC : '.strtolower($pic_save).' </li>
 				<li>Target Date : '.$target.' </li>
 				<li>PROBLEM NO : <b>'.$number.'</b></li>
 				<li>EVALUATION TYPE :  '.$evaluation.'</li>
 				<li>Assigned By : <b><i>'.$pengirim.'</i></b></li>
 				</ul>
 				</div> 
 				</div>
 				</div>
 				<p>Thank you for your time and assistance</p>
 				<p><i>This message was automatically generated by  IDEE apps</i></p><br>
 				<p><a target="_blank"  href="'.base_url().'">IDEE Web Link</a></p><br>
 				<p>Regards,</p><br>
 				<p>Reliability Management (TER-1)</p>
 				</br>
 				</body>
 				</html>
 				';	

 			}
 			if(!$mail->send()) {
 				echo 'Pesan email tidak terkirim.';
 				echo 'Mailer Error: ' . $mail->ErrorInfo;
 			} else {
 				$this->db->set('status', 'Sent Success');
 				$this->db->where('numberproblem', $number);
 				$this->db->update('assigntemp');
 				$this->Getdat_model->saveaction($action);
 			}
 			$this->session->set_flashdata("Pesan", "ASSIGN IS SEND");
 			redirect('pd','refresh');
 		}
 	}

 	public function searchdraft()
 	{
 		$number_draft = htmlspecialchars($this->input->post('number_draft'));
 		$nopeg_act = htmlspecialchars($this->input->post('nopeg_act'));
 		$get = $this->Getdat_model->draft($number_draft, $nopeg_act);
 		echo json_encode($get);
 	}
 	

 	public function searchsolution()
 	{
 		$solution = $this->input->post('solution');
 		$this->db->where('numbersolution', $solution); 
 		$query= $this->db->get('solutions')->row_array();
 		if (!empty($query)) {
 		$res = [];
 		if ($query["remarks"] == 'null' or is_null($query["remarks"])) {
 			$remarks = '-';
 		} else {
 			$remarks = $query["remarks"];
 		}

 		if (!empty($query['file']) and $query['file'] != ',') {
 			$h['numbersolution'] = $query["numbersolution"];
 			$h['solution'] = $query["solution"];
 			if ($query['status'] == null or $query['status'] == '' or empty($query['status'])) {
 				$h['status'] = '0 %';
 			}  else {
 				$h['status'] = $query["status"].' %';
 			}
 			$h['remarks'] = $remarks;
 			$h['uic'] = $query['uic'];
 			$new_s = rtrim($query['file'],',');
 			$exp = explode(',',$new_s);
 			for ($i=0; $i < count($exp) ; $i++) { 
 				 $row= $this->Getdat_model->getfile($exp[$i]); 
 				 if(empty($row) or is_null($row)){
 				 	$b[] = '';
 				 } else {

 				 	 if ($row['nama_baru'] == null or empty($row['nama_baru']) ) {

		 				 $url = base_url().'files/'.$row['nama_file'];
 				 	 } else {
 				 	 	 $url = base_url().'files/'.$row['nama_baru'];
 				 	 }
 				 $b[] = "<a class='badge badge-success' target='_blank' href='".$url."'>'".$row['nama_file']."'</a><hr>";
 				 }
 			}
 			$h['file'] = $b;
 		} else {
 			
 			if ($query['status'] == null or $query['status'] == '' or empty($query['status'])) {
 				$h['status'] = '0 %';
 			}  else {
 				$h['status'] = $query["status"].' %';
 			}
 			$h['numbersolution'] = $query["numbersolution"];
 			$h['solution'] = $query["solution"];
 			$h['uic'] = $query['uic'];
 			$h['remarks'] = $remarks;
 			$h['file']	  = 'No';
 		}
 		array_push($res,$h);
 		echo json_encode($res);			
 		}

 	}

 	public function searchsolutionupdate()
 	{
 		$solution = $this->input->post('solution');
 		$this->db->where('numbersolution', $solution); 
 		$query= $this->db->get('solutions')->row_array();
 		if (!empty($query)) {
 		$res = [];
 		if ($query["remarks"] == 'null' or is_null($query["remarks"])) {
 			$remarks = '-';
 		} else {
 			$remarks = $query["remarks"];
 		}
 		if (!empty($query['file']) and $query['file'] != ',' ) {
 			$h['numbersolution'] = $query["numbersolution"];
 			$h['solution'] = $query["solution"];
 			$h['status'] = $query["status"];
 			$h['remarks'] = $remarks;
 			$new_s = rtrim($query['file'],',');
 			$exp = explode(',',$new_s);
 			for ($i=0; $i < count($exp) ; $i++) { 
 				 $row= $this->Getdat_model->getfile($exp[$i]); 

 				  if(empty($row) or is_null($row)){
 				 	$b[] = '';
 				 } else {
 				 	if ($row['nama_baru'] == null or empty($row['nama_baru']) ) {
 				 			$url = base_url().'files/'.$row['nama_file'];
 				 	} else {
 				 		$url = base_url().'files/'.$row['nama_baru'];
 				 	}
 				 
 				 $b[] = "<li id='".$row['nama_file']."'><a class='btn btn-success btn-sm' target='_blank' href='".$url."'>'".$row['nama_file']."'</a>&nbsp;<a id='point_of' data-unik1='".$query["numbersolution"]."' data-delete='".$row['nama_file']."' data-unik2='".$row['token']."' type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a></li><br>";
 				 }
 				 
 			}
 			$h['file'] = $b;
 			$h['hidden'] =  $query['file'];
 		} else {
 			$h['numbersolution'] = $query["numbersolution"];
 			$h['solution'] = $query["solution"];
 			$h['status']   = $query["status"];
 			$h['remarks']  = $remarks;
 			$h['file']	   = '';
 			$h['hidden']   = '';
 		}
 		array_push($res,$h);
 		echo json_encode($res);			
 		}

 	}

public function teas()
 	{
 		$num = $this->input->post('arr');
 		$remarks_problem = $this->input->post('remarks_problem');
 		$eval_status = $this->input->post('eval_status');
 		$output_pic = $this->input->post('output_pic');
 		$target_date_update = date('d F Y', strtotime($this->input->post('target_date_update')));
 		$target_date_update2 = $this->input->post('target_date_update');
 		$number_p = $this->input->post('number_p');
 		$comments = $this->input->post('comments');
 		$sri = $this->input->post('sri');
 		$user = $this->session->userdata('nopeg');
 		$token = $this->Getdat_model->gettoken($user,$number_p);
 		$file = $this->Getdat_model->getnumber($number_p);
 		$awal_1 = $file['file'];
 		$status_hidden = $this->input->post('status_hidden');
 		$target_date_update_hidden = date('d F Y', strtotime($this->input->post('target_date_update_hidden')));
 		$pic_hidden = $this->input->post('pic_hidden');
 		$onwatch = $this->input->post('onwatch');
 		foreach ($token as $key) 
 		{
 			$out[] = $key['token'].',';
 		}
		
 		
 		 if ($target_date_update != $target_date_update_hidden && $pic_hidden != $output_pic && $status_hidden != $eval_status) {
			

			$desc = "Update Target Date,PIC and Status success, From Target Date : ". $target_date_update_hidden." To Target Date : ".$target_date_update.' & From PIC  : '.strtolower(cek_email($pic_hidden)).' To PIC : '. strtolower(cek_email($output_pic)).' From Status : '.$status_hidden." To status : ".$eval_status;
		}
 		else if ($pic_hidden != $output_pic && $status_hidden != $eval_status) {
			$desc = "Update Status and PIC success, From status : ".$status_hidden." To status : ".$eval_status.' & From PIC  : '.strtolower(cek_email($pic_hidden)).' To PIC : '. strtolower(cek_email($output_pic)); 
		} else if ($target_date_update != $target_date_update_hidden && $status_hidden != $eval_status) {
		   $desc = "Update Status and Target Date success, From status : ".$status_hidden." To status : ".$eval_status.' & From Target Date  : '.$target_date_update_hidden.' To PIC : '. $target_date_update;
		} else if ($target_date_update != $target_date_update_hidden && $pic_hidden != $output_pic) {
			$desc = "Update Target Date and PIC success, From Target Date : ". $target_date_update_hidden." To Target Date : ".$target_date_update.' & From PIC  : '.strtolower(cek_email($pic_hidden)).' To PIC : '. strtolower(cek_email($output_pic)); 
		} 
		else if ($status_hidden != $eval_status) {
			$desc = "Update status success, from : ".$status_hidden." To : ".$eval_status; 
		} else if ($pic_hidden != $output_pic) {
			$desc = 'Update PIC success, From : '.strtolower(cek_email($pic_hidden)).' To : '.strtolower(cek_email($output_pic));
		} else if ($target_date_update != $target_date_update_hidden) {
			$desc =  "Update target date success, change target date From : ".$target_date_update_hidden." To : ".$target_date_update;
		} else  {
 			$desc = "Update status success "; 
 		} 




 			$jam = date('Y-m-d H:i:s');
 			$act = 
 			[
 				'numberproblem' 	 => $number_p,
 				'user' 	 => strtolower($this->session->userdata('nama')),
 				'date'	 => $jam,
 				'action' => 'Update Status',
 				'description'	=>  $desc
 			];

 			$this->db->insert('action_log', $act);


 		@$op = implode("",$out);
 		$file_update = $op.$awal_1;
 		if (empty($num)) {
 			$this->db->set('remarks', $remarks_problem);
 			$this->db->set('status', $eval_status);
 			$this->db->set('PIC', $output_pic);
 			$this->db->set('file', $file_update);
 			$this->db->set('comments', $comments);
 			$this->db->set('TargetDate', $target_date_update2);
 			$this->db->set('srieffectivenees', $sri);
 			$this->db->set('onwatch', $onwatch);
 			$this->db->where('number', $number_p);
 			$this->db->update('problem_definition');
 			$this->db->delete('temp_token',['user' => $user, 'number' => $number_p]);
 			$this->session->set_flashdata("Pesan", "Problem is update");
 		} else {
 			foreach ($num as $key) 
 			{
 				$number_solusi  = $key['n_s_s'];
 				$percent  = $key['per'];
 				$remarks  = $key['rem'];
 				$file_hidden  = $key['hid'];
 				if ($key['fil'] == 'File_ada') {
 					if ($file_hidden != ',' and !empty($file_hidden)) {
 						$in =  [];
 						$save_file = $this->Getdat_model->gettoken($user,$number_solusi);	
 						foreach ($save_file as $yek) 
 						{
 							$in [] = $yek['token'].',';
 						}
 						$po = implode("",$in);
 						$p_sace = $file_hidden.$po;
 						$this->db->set('file',$p_sace);
 						$this->db->set('status', $percent);
 						$this->db->set('remarks', $remarks);
 						$this->db->set('last_update', date('Y-m-d H:i:s'));
 						$this->db->set('user_update',$this->session->userdata('nama'));
 						$this->db->where('numbersolution', $number_solusi);
 						$this->db->update('solutions');
 						$p_sace = '';
 						$po = '';
 						$this->db->delete('temp_token',['user' => $user, 'number' => $number_solusi]);
 					} else {
 						$in =  [];
 						$save_file = $this->Getdat_model->gettoken($user,$number_solusi);	
 						foreach ($save_file as $yek) 
 						{
 							$in [] = $yek['token'].',';
 						}
 						$po = implode("",$in);
 						$this->db->set('file',$po);
 						$this->db->set('status', $percent);
 						$this->db->set('remarks', $remarks);
 						$this->db->set('last_update', date('Y-m-d H:i:s'));
 						$this->db->set('user_update',$this->session->userdata('nama'));
 						$this->db->where('numbersolution', $number_solusi);
 						$this->db->update('solutions');
 						$po = '';
 						$this->db->delete('temp_token',['user' => $user, 'number' => $number_solusi]);
 					}
 				} else if (empty($key['fil'])) {
 					$this->db->set('status', $percent);
	 				$this->db->set('remarks', $remarks);
	 				$this->db->set('last_update', date('Y-m-d H:i:s'));
	 				$this->db->set('user_update',$this->session->userdata('nama'));
	 				$this->db->where('numbersolution', $number_solusi);
	 				$this->db->update('solutions');
 				}
 					$this->db->set('remarks', $remarks_problem);
	 				$this->db->set('status', $eval_status);
	 				$this->db->set('PIC', $output_pic);
	 				$this->db->set('comments', $comments);
	 				$this->db->set('TargetDate', $target_date_update2);
	 				$this->db->set('srieffectivenees', $sri);
	 				$this->db->set('onwatch', $onwatch);
	 				$this->db->set('file', $file_update);
	 				$this->db->where('number', $number_p);
	 				$this->db->update('problem_definition');   
	 				$this->db->delete('temp_token',['user' => $user, 'number' => $number_p]);
	 	
 			}

$this->session->set_flashdata("Pesan", "Problem is update");
 			



 		}		
 	}
 	public function searchfile()
 	{
 		$token = $this->input->post('token');
 		$replace = str_replace(',', '',$token);
 		$row= $this->Getdat_model->getfile($replace); 
 		echo json_encode($row);
 	} 	

 	public function dp($number)
 	{
 		$this->Getdat_model->delete_problem($number); 
 		$this->session->set_flashdata("Pesan", "Problem successfully deleted");
 		redirect('delete','refresh');
 	}


 	
 }

 /* End of file Auth.php */
 /* Location: ./application/controllers/Auth.php */

 ?>

