<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pe extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
		error_reporting(0);
	}
	public function index()
	{
	
		$user = strtolower($this->session->userdata('email'));
		$data['title'] = 'List Problem Evaluation';
		$data['problem'] = $this->Getdat_model->getproblem($user);
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/db', $data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}
	public function getsolusi()
	{
		$actype = htmlspecialchars($this->input->post('typeac'));
		$ata = htmlspecialchars($this->input->post('ata'));
		$hasil = $this->Getdat_model->solusi($actype,$ata);
		echo json_encode($hasil);
	}
	// Public function proses_upload()
	// {
		    
	// 	$nama_file = $_FILES['userfile']['name'];
	// 	$save = str_replace(' ', ',', $nama_file);
	// 	$save1 = str_replace('#', ',', $save);
	// 	$save2 = str_replace('&', '', $save1);
	// 	$save3 = str_replace('_', ',', $save2);
	// 	$save5 = str_replace("'", '', $save3);
	// 	$_FILES['userfile']['name'] =  $save5;
	// 	$config['upload_path']   = FCPATH.'/files/';
	// 	$config['allowed_types'] = '|jpg|png|pdf|docx|xlsx|doc|txt|csv';
	// 	$this->load->library('upload',$config);
	// 	if($this->upload->do_upload('userfile')){
	// 		$token=$this->input->post('token_file');
	// 		$number=$this->input->post('number');
			
	// 		$data = 
	// 		[
	// 			"token" => $token,
	// 			"user"	=> $this->session->userdata('nopeg'),
	// 			"number" => $number
	// 		];
	// 		$this->db->insert('temp_token',$data);
	// 		$this->db->insert('attach',array('nama_file'=>$save5,'token'=>$token));
	// 	}
	// }

	Public function proses_upload()
	{
		//var_dump($_FILES['userfile']);
		$token=$this->input->post('token_file');
		$number=$this->input->post('number');
		$nama_file = $_FILES['userfile']['name'];
		$ekstensiGambarValid = ['jpg','jpeg','png','JPG','JPEG','PNG','jpg','png','pdf','docx','xlsx','doc','txt','csv'];
		$ekstensiGambar = explode('.', $nama_file);
        $ekstensiGambar = strtolower(end($ekstensiGambar));
		$nama_fileBaru  = uniqid();
		$nama_fileBaru .= '.';
		$nama_fileBaru .= $ekstensiGambar;
		$_FILES['userfile']['name'] =  $nama_fileBaru;
		$config['upload_path']   = FCPATH.'/files/';
		$config['allowed_types'] = '|jpg|png|pdf|docx|xlsx|doc|txt|csv';
		$this->load->library('upload',$config);
		if($this->upload->do_upload('userfile')){
			$data = 
			[
				"token" => $token,
				"user"	=> $this->session->userdata('nopeg'),
				"number" => $number
			];
			$data2 = 
			[
				"id"		=> '',
				"nama_file" => $nama_file,
				"token"	=> $token,
				"nama_baru" => $nama_fileBaru
			];
			$this->db->insert('temp_token',$data);
			$this->db->insert('attach',$data2);
			$eksport= 
			[
				"nama_awal" => $nama_file,
				"nama_baru" => $nama_fileBaru
			];
			echo json_encode($eksport);
		}
	}





	Public function remove_file()
	{
		$token=$this->input->post('token');
		$foto=$this->db->get_where('attach',array('token'=>$token));
		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_file=$hasil->nama_file;
			if(file_exists($file=FCPATH.'/files/'.$nama_file)){
				unlink($file);
			}
			$this->db->delete('temp_token',array('token'=>$token));
			$this->db->delete('attach',array('token'=>$token));
		}

		echo json_encode($nama_file);
	}
	public function numbersolutionsave(){
		date_default_timezone_set("Asia/Jakarta");
		$actype = $this->input->post('actype');
		$ata  = $this->input->post('ata');
		$solution_name = $this->input->post('solution_name');
		$uic = $this->input->post('uic');
		$target_date = $this->input->post('target_date');
		$revisi_target = $this->input->post('revisi_target');
		$cek_angka = $this->db->query("SELECT max(numbersolution) as maxnumber FROM solutions WHERE actype = '$actype' AND ata = '$ata' ")->row_array();
		$cek = $cek_angka['maxnumber'];
		$noUrut = (int) substr($cek, 7,6);
		$noUrut++;
		$response = sprintf("%06s", $noUrut);
		$hasil_number = $actype.'-'.$ata.'-'.$response;
		$data = 
		[
			'numbersolution'  => $hasil_number,
			'solution'		  => $solution_name,
			'actype'		  => $actype,
			'targetdate' 	  => $target_date,
			'ata'			  => $ata,
			'status'		  => '0',
			'last_update'	  => date('Y-m-d H:i:s'),
			'user_update'	  => $this->session->userdata('nama'),
			'revisidate' 	  => $revisi_target,
			'uic'			  => $uic
		];
		$this->db->insert('solutions', $data);
		echo json_encode($hasil_number);
	}
	public function numbersolutiondelete()
	{
		$js = $this->input->post('js');
		$this->db->delete('solutions', ['numbersolution' => $js]);
		echo json_encode('sukses');
	}
	public function solutionupdate()
	{
		$numbersolusi 		= 	$this->input->post('numbersolusi');
		$uic 				= 	$this->input->post('uic');
		$solution_name  	= 	htmlspecialchars($this->input->post('solution_name'));
		$target_date 		= 	$this->input->post('target_date');
		$revisi_target 		= 	$this->input->post('revisi_target');	
		$data = 
		[
			"solution"  	=> $solution_name,
			"uic"			=> $uic,
			"target_date"	=> $target_date,
			"revisi_date"	=> $revisi_target
		];
		$this->db->set('solution', $solution_name);
		$this->db->set('uic', $uic);
		$this->db->set('targetdate', $target_date);
		$this->db->set('status', '0');
		$this->db->set('revisidate', $revisi_target);
		$this->db->where('numbersolution', $numbersolusi);
		$this->db->update('solutions');
		echo json_encode($data);
	}
	
		public function save()
	{
		$this->load->library('phpmailer_lib');
		$solusinumber = htmlspecialchars($this->input->post('numbersolusi'));
		$number = htmlspecialchars($this->input->post('number'));
		$crossreff = htmlspecialchars($this->input->post('crossreff'));
		$idf = htmlspecialchars($this->input->post('idf'));
		$actype = htmlspecialchars($this->input->post('actype3'));
		$ata = htmlspecialchars($this->input->post('ata'));
		$analysis = html_entity_decode($this->input->post('analysis'));
		$file_gabung = htmlspecialchars($this->input->post('file_gabung'));
		$ko = $this->input->post('p');
		$sao = $this->input->post('solution');
		$user = $this->session->userdata('nopeg');
		$token = $this->Getdat_model->gettoken($user,$number);
		$user_assign = $this->session->userdata('email');
		$b = $idf + 1;
		$solusi_temp = [];
	
		foreach ($token as $key) 
		{
			$out[] = $key['token'].',';
		}	
		@$ts = implode("",$out);
		if (!isset($_POST['progress']) and !isset($_POST['review'])) {
			$this->session->set_flashdata("Pesan", "Access forbiden !");	
			redirect('pe','refresh');
		}

			for ($u=0; $u < 10 ; $u++) { 

				$this->db->set('solution'.$u,'');
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
			}

			for ($y=0; $y < count($ko) ; $y++) { 
				$name_solution = htmlspecialchars($this->input->post('solution'.$i));
				$this->db->set('solution'.$y, $ko[$y]);
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
				array_push($solusi_temp, $sao[$ko]);
			}

		if (isset($_POST['progress'])) {
			$action = 
			[
				'numberproblem' 	 => $number,
				'user' 	 => $this->session->userdata('nama'),
				'date'	 => date('Y-m-d H:i:s'),
				'action' => 'Update solusi to problem definition',
				'description'	=> 'Success update solusi to PROGRESS'
			];
			if (empty($file_gabung)) {
				$this->db->set('problemanalisis', $analysis);
				$this->db->set('status', 'PROGRESS');
				$this->db->set('file', $ts);
				$this->db->set('remarks', '');
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);	
			} else {
				$work = $ts.$file_gabung;
				$this->db->set('problemanalisis', $analysis);
				$this->db->set('status', 'PROGRESS');
				$this->db->set('file', $work);
				$this->db->set('remarks', '');
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
			}
			$this->session->set_flashdata("Pesan", "Solution was successfully added !");
			$this->Getdat_model->saveaction($action);
			redirect('Pe','refresh');
		} else if (isset($_POST['review'])){
			$action = 
			[
				'numberproblem' 	 => $number,
				'user' 	 => $this->session->userdata('nama'),
				'date'	 => date('Y-m-d H:i:s'),
				'action' => 'Update solusi to problem definition',
				'description'	=> 'Success update solusi to REVIEW'
			];

			if (empty($file_gabung)) {
				$this->db->set('problemanalisis', $analysis);
				$this->db->set('status', 'REVIEW');
				$this->db->set('file', $ts);
				$this->db->set('remarks', '');
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
			} else {
				$work = $ts.$file_gabung;
				$this->db->set('problemanalisis', $analysis);
				$this->db->set('status', 'REVIEW');
				$this->db->set('file', $work);
				$this->db->set('remarks', '');
				$this->db->where('number', $number);
				$this->db->update('problem_definition');
				
				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
			}
			$id = $this->Getdat_model->cekcrossreff($crossreff);
// cek Event ID

			if ($id !== NULL) {
				$save_analisis = "Analysis Follow Up : ".$analysis;
				if (empty($solusi_temp)) {
					$save_follow = $save_analisis;
				} else {
					$solusi = implode(',',$solusi_temp);
					$solusi_save = "Solution Follow Up : ".$solusi;
					$save_follow = $save_analisis.$solusi_save;
				}
				$this->Getdat_model->updatefollow($id['p_id'],$save_follow);
			}
			$this->Getdat_model->saveaction($action);
			$this->_send_email($number);
		}
	}

	public function problemdelete()
	{
		$number_problem= htmlspecialchars($this->input->post('numberproblem'));
		$solusi = htmlspecialchars($this->input->post('solusi'));
		$this->db->set($solusi,'');
		$this->db->where('number',$number_problem);
		$this->db->update('problem_definition');
		echo json_encode('sukses');	
	}

	private function _send_email($number)
	{
		date_default_timezone_set("Asia/Jakarta");
		$mail = $this->phpmailer_lib->load();
		$number = htmlspecialchars($this->input->post('number'));	
		$row = $this->Getdat_model->getnumber($number);
		$b = time();
		$hour = date("G",$b);
		$user_email = strtolower($this->session->userdata('email'));
		$pengirim = $this->session->userdata('nama');
		$pic = $row['PIC'];
		$userpengirim = $row['user'];
		$assign = getmail($userpengirim); 
		$cc  = $row['cc'];
		$ac = cekactype($row['actype']);
		$op = cekoperator($row['operator']);
		$ata = $row['ata'];
		$evaluation = $row['evaluationtype'];
		$problem_title = $row['problemtitle'];
		$target = date('d F Y', strtotime($row['TargetDate']));
		$new_c = rtrim($cc,',');
		$new_p = rtrim($pic,',');
		$s = explode(',',$new_c);
		$p = explode(',',$new_p);
		$email_penerima = [];
		for ($y=0; $y < count($s) ; $y++) 
		{ 
			$hasil_cc[] = $s[$y];    
		}	
		for ($y=0; $y < count($p) ; $y++) 
		{ 
			$hasil_pic[] = $p[$y];    
		}	
		if(count($hasil_cc) < 0){
			$email_penerima =  $hasil_pic;
			array_push($email_penerima,$assign);
		} else {
			$email_penerima =  array_merge($hasil_pic,@$hasil_cc);
			array_push($email_penerima,$assign);
		}
		for ($s=0; $s < count($email_penerima) ; $s++) { 
			$mail->isSMTP();	
			$mail->Host = 'mail.gmf-aeroasia.co.id';
			$mail->CharSet = 'UTF-8';
			$mail->Port = 25;
			$mail->SMTPAuth = true;
			$mail->Username = "reliability.management@gmf-aeroasia.co.id";
			$mail->Password = "Aeroas1@";
			$mail->setFrom('reliability.management@gmf-aeroasia.co.id', 'APPS IDEE');
			$mail->Subject = 'Evaluation Submit:'.$evaluation.':'.$problem_title.'';
			$mail->isHTML(true);  
			$mail->addAddress($email_penerima[$s]);
			$mail->Body = '
			<!doctype html>
			<html>
			<body>
			<h4><b>Greetings!</b> </h4> </br>
			<p>Dear Fellow engineer, evaluation has been submitted for :</p></br>
			<body>
			<div>
			<div>
			<div>
			<ul>
			<li>PROBLEM TITLE :  '.$problem_title.'</li>
			<li>A/C TYPE  : '.$ac.'</li>
			<li>OPERATOR : '.$op.'</li>
			<li>ATA : '.$ata.'</li>
			<li>PIC : '.strtolower($new_p).' </li>
			<li>Target Date : '.$target.' </li>
			<li>PROBLEM NO : <b>'.$number.'</b></li>
			<li>EVALUATION TYPE :  '.$evaluation.'</li>
			</ul>
			</div> 
			</div>
			</div>
			<p>Thank you for your time and assistance</p>
			<p><i>This message was automatically generate by  IDEE apps</i></p><br>
			<p><a target="_blank"  href="'.base_url().'">IDEE Web Link</a></p><br>
			<p>Regards,</p><br>
			<p>Reliability Management (TER-1)</p>
			</br>
			</body>
			</html>
			';	
		}
		if(!$mail->send()) {
			echo 'Pesan email tidak terkirim.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			
			$this->db->set('status', 'Sent Success');
			$this->db->where('numberproblem', $number);
			$this->db->update('assigntemp');
		}
		$this->session->set_flashdata("Pesan", "PROBLEM EVALUATION IS SEND");
		redirect('pe','refresh');
		
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
?>

