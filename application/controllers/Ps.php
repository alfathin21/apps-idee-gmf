<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Ps extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Getdat_model');
		is_log_in();
		error_reporting(0);
	}

	public function index()
	{
		$data['title'] = 'Problem Search';
		$data['te'] = $this->Getdat_model->gette();
		$role=  $this->session->userdata('role_id');
		$hitung = $this->db->query("SELECT * FROM user_access_menu where role_id = '$role' and menu_id = 2 ")->num_rows();
		$data['act'] = $hitung;
		$data['uic'] = $this->db->query("SELECT uic from uic_master WHERE status = 'OPEN' ")->result_array();
 		$data['alert'] = $this->db->query("SELECT alert from alert_master WHERE status = 'OPEN' ")->result_array();
 		$data['evaltype'] = $this->db->query("SELECT evaltype from evaltype_master WHERE status = 'OPEN' ")->result_array();
 		$data['operator'] = $this->db->query("SELECT operator, value from operator_master WHERE status = 'OPEN' ")->result_array();
 		$data['actype'] = $this->db->query("SELECT actype, value from actype_master WHERE status = 'OPEN' ")->result_array();
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/ps',$data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}

	public function deleteattach()
	{
		$number = $this->input->post('number');
		$file 	= $this->input->post('file');
		$id_file = $this->input->post('ui');
		$out = $this->db->query("SELECT file from problem_definition where number = '$number' ")->row_array();
    	$op = $out['file'];
    	$array_awal = explode(',',$file);
    	$array_db   = explode(',', $op);
    	$result=array_diff($array_db,$array_awal);
    	$update_file = implode('', $result);
    	$save = $update_file.',';
    	$this->db->set('file', $save);
    	$this->db->where('number',$number);
    	$this->db->update('problem_definition');
    	$this->db->delete('attach', ['token' => $file]);
    		if(file_exists($file=FCPATH.'/files/'.$id_file)){
				unlink($file);
			}
    	echo json_encode($id_file);
	}

	public function deleteattachsolution()
	{
		$number = $this->input->post('number');
		$file 	= $this->input->post('file');
		$id_file = $this->input->post('ui');
		$out = $this->db->query("SELECT file from solutions where numbersolution = '$number' ")->row_array();
    	$op = $out['file'];
    	$array_awal = explode(',',$file);
    	$array_db   = explode(',', $op);
    	$result=array_diff($array_db,$array_awal);
    	$update_file = implode('', $result);
    	$save = $update_file.',';
    	$this->db->set('file', $save);
    	$this->db->where('numbersolution',$number);
    	$this->db->update('solutions');
    	$this->db->delete('attach', ['token' => $file]);
    		if(file_exists($file=FCPATH.'/files/'.$id_file)){
				unlink($file);
			}
    	echo json_encode($id_file);
	}

	public function search()
	{
		$this->load->library('encryption');
		$q_param['number']				= htmlspecialchars($this->input->post('number'));
		$q_param['evaluation']			= htmlspecialchars($this->input->post('evaluation'));
		$q_param['operator'] 			= htmlspecialchars($this->input->post('operator'));
		$q_param['ata'] 				= htmlspecialchars($this->input->post('ata'));
		$q_param['uic'] 				= htmlspecialchars($this->input->post("uic"));
		$q_param['keyword'] 			= htmlspecialchars($this->input->post("keyword"));
		$q_param['actype'] 				= htmlspecialchars($this->input->post("actype"));
		$q_param['acreg'] 				= htmlspecialchars($this->input->post("acreg"));
		$q_param['alerttype'] 			= htmlspecialchars($this->input->post("alerttype"));
		$q_param['status'] 				= htmlspecialchars($this->input->post("status"));
		$q_param['problem_title'] 		= htmlspecialchars($this->input->post("problem_title"));
		$q_param['pic'] 				= $this->input->post("pic[]");
		$q_param['crossreff'] 			= htmlspecialchars($this->input->post("crossreff"));
		$q_param['date_to'] 			= htmlspecialchars($this->input->post("date_to"));
		$q_param['date_from'] 			= htmlspecialchars($this->input->post("date_from"));
		$result 						= $this->Getdat_model->problem_search($q_param);
		$response = [];
		$no = 1;
		foreach ($result as $key) {
			$number = $key["number"];
			$enkrip = $this->encryption->encrypt($number);
			$crossreff = $key["crossreff"];
			$operator =cekoperator($key["operator"]);
			$actype = cekactype($key["actype"]);
			$ata = $key["ata"];
			$systems = $key["systems"];
			$evaluationtype = $key["evaluationtype"];
			$alert = $key["alert"];
			$acreg = $key["acreg"];
			$problemtitle = htmlspecialchars($key["problemtitle"]);
			$sa = htmlspecialchars($key["detailproblem"]);
			$ba = htmlspecialchars($key["problemanalisis"]);
			$detailproblem = str_replace("'", '', $sa);
			$problemanalisis = str_replace("'", '', $ba);


			$c   =  rtrim($key["PIC"], ",");
			$b = backtoname($c);
			$PIC = strtoupper($b);
			$cc = $key["cc"];
			$TargetDate = $key["TargetDate"];
			$UIC = $key["UIC"];
			$solution0 = $key["solution0"];
			$solution1 = $key["solution1"];
			$solution2 = $key["solution2"];
			$solution3 = $key["solution3"];
			$solution4 = $key["solution4"];
			$solution5 = $key["solution5"];
			$solution6 = $key["solution6"];
			$solution7 = $key["solution7"];
			$solution8 = $key["solution8"];
			$solution9 = $key["solution9"];
			$status = $key["status"];
			$onwatch = $key["onwatch"];
			$remarks = $key["remarks"];
			$releated = $key["releated"];
			$comments = $key["comments"];
			$file = $key["file"];
			$sri = $key["srieffectivenees"];
			$h['no'] = $no;
			$h['number'] = $number;
			$h['crossreff'] = $crossreff;
			$h['operator'] = $operator;
			$h['actype'] 	= $actype;
			$h['alert'] = $alert;
			$h['acreg'] = $acreg;
			$h['ata'] = $ata;
			$h['systems'] = $systems;
			$h['problemtitle'] = $problemtitle;
			$h['evaluationtype'] = $evaluationtype;
			$h['detailproblem'] = $detailproblem;
			$h['PIC'] =$PIC;
			$h['problemanalisis'] = $key["problemanalisis"];
			$h['action1'] = "
			<button 
			id ='view_p'
			data-comments = '$comments'
			data-enkrip = '$enkrip'
			data-problemanalisis = '$problemanalisis'
			data-number = '$number'
			data-eval   =  '$evaluationtype'
			data-actype = '$actype'
			data-operator = '$operator'
			data-acreg    = '$acreg'
			data-releated = '$releated'
			data-solution0 = '$solution0'  
			data-solution1 =  '$solution1'
			data-solution2 =  '$solution2'  
			data-solution3 =   '$solution3'  
			data-solution4 =  '$solution4'  
			data-solution5 =  '$solution5'  
			data-solution6 =  '$solution6'  
			data-solution7 =  '$solution7'  
			data-solution8 =  '$solution8'  
			data-solution9 =  '$solution9'  
			data-alert=  '$alert'
			data-ata   = '$ata'
			data-systems = '$systems'
			data-problemtitle = '$problemtitle'
			data-detailproblem = '$detailproblem'
			data-PIC = '$PIC'
			data-UIC = '$UIC'
			data-file = '$file'
			data-status = '$status'
			data-cc = '$cc'
			data-remarks = '$remarks'
			data-cross = '$crossreff'
			data-sri = '$sri'
			data-target_date = '$TargetDate'
				data-onwatch = '$onwatch'
			type='button' data-toggle='modal' data-target='#detail' class='btn btn-primary btn-sm'>
			<i class='fa fa-search'></i>&nbsp; View Detail
			</button>
			<button id='udate' 
			data-comments_d = '$comments'
			data-problemanalisis_d = '$problemanalisis'
			data-number_d = '$number'
			data-eval_d   =  '$evaluationtype'
			data-actype_d = '$actype'
			data-operator_d = '$operator'
			data-acreg_d    = '$acreg'
			data-releated_d = '$releated'
			data-solution0_d = '$solution0'  
			data-solution1_d =  '$solution1'
			data-solution2_d =  '$solution2'  
			data-solution3_d =   '$solution3'  
			data-solution4_d =  '$solution4'  
			data-solution5_d =  '$solution5'  
			data-solution6_d =  '$solution6'  
			data-solution7_d =  '$solution7'  
			data-solution8_d =  '$solution8'  
			data-solution9_d =  '$solution9'  
			data-alert_d=  '$alert'
			data-ata_d   = '$ata'
			data-systems_d = '$systems'
			data-problemtitle_d = '$problemtitle'
			data-detailproblem_d = '$detailproblem'
			data-PIC_d = '$c'
			data-UIC_d = '$UIC'
			data-file_d = '$file'
			data-status_d = '$status'
			data-cc_d = '$cc'
			data-remarks_d = '$remarks'
			data-cross_d = '$crossreff'
			data-sri_d = '$sri'
			data-target_date_d = '$TargetDate'
			data-onwatch = '$onwatch'
			type='button' data-toggle='modal' data-target='#update' class='btn btn-danger btn-sm'>
			<i class='fa fa-clipboard'></i>&nbsp; Update Status
			</button>
			";
			$h['action2'] = "
			<button 
			id ='view_p'
			data-enkrip = '$enkrip'
			data-comments = '$comments'
			data-problemanalisis = '$problemanalisis'
			data-number = '$number'
			data-eval   =  '$evaluationtype'
			data-actype = '$actype'
			data-operator = '$operator'
			data-acreg    = '$acreg'
			data-releated = '$releated'
			data-solution0 = '$solution0'  
			data-solution1 =  '$solution1'
			data-solution2 =  '$solution2'  
			data-solution3 =   '$solution3'  
			data-solution4 =  '$solution4'  
			data-solution5 =  '$solution5'  
			data-solution6 =  '$solution6'  
			data-solution7 =  '$solution7'  
			data-solution8 =  '$solution8'  
			data-solution9 =  '$solution9'  
			data-alert=  '$alert'
			data-ata   = '$ata'
			data-systems = '$systems'
			data-problemtitle = '$problemtitle'
			data-detailproblem = '$detailproblem'
			data-PIC = '$PIC'
			data-UIC = '$UIC'
			data-file = '$file'
			data-status = '$status'
			data-cc = '$cc'
			data-remarks = '$remarks'
			data-cross = '$crossreff'
			data-sri = '$sri'
			data-target_date = '$TargetDate'
			data-onwatch = '$onwatch'
			type='button' data-toggle='modal' data-target='#detail' class='btn btn-primary btn-sm'>
			<i class='fa fa-search'></i>&nbsp; View Detail
			</button>
			";
			if ($key["status"] == "OPEN") {
				$h['status'] = '<label class="badge badge-success">OPEN</label>';
			} else if ($key['status'] == "REVIEW") {
				$h['status'] = '<label class="badge badge-danger">REVIEW</label>';

			} else if ($key['status'] == "PROGRESS") {
				$h['status'] = '<label class="badge badge-warning">PROGRESS</label>';

			}else if ($key['status'] == "CLOSE") {
				$h['status'] = '<label class="badge badge-primary">CLOSE</label>';

			}else if ($key['status'] == "HIDDEN") {
				$h['status'] = '<label class="badge badge-default">HIDDEN</label>';
			}
			$h['remarks'] = $key["remarks"];
			if ($key["file"] == '') {
				$h['file'] = 'NO';
			} else {
				$h['file'] = 'YES';
			}

			$no++;
			array_push($response, $h);


		}

		$newresponse = array(
			'data' => $response
		);
		echo json_encode($newresponse);
	}


}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */

 ?>

