<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rd extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->library('phpmailer_lib');	
		$this->load->model('Getdat_model');
		
	}
	public function index()
	{
		$Evid =$this->input->get('evid');	
		$parse   = $this->Getdat_model->detailtdam($Evid);
		$outy     = $this->Getdat_model->getRetri($Evid);
		$getRetri = $outy['Rectification'];
		$data = 
		[
			'title' 		=> 'New Issue TDAM',
			'EventId' 		=> $Evid,
			'ACTypeOrig' 	=> cekactypeorig($parse['ACTypeOrig']),
			'ACRegOrig'	 	=> $parse['ACRegOrig'],
			'atachap'	 	=> atachek($parse['atachap']),
			'Note' 		 	=> $parse['Note'],
			'TargetDate' 	=> $parse['p_target_date'],
			'Description'	=> $parse['p_description'],
			'Preanalis'		=> $parse['Preanalis'],
			'FltNo'			=> $parse['FltNo'],
			'DateDeptEvent'	=> $parse['DateDeptEvent'],
			'Preanalis'		=> $parse['Preanalis'],
			'uic'			=> $this->db->query("SELECT uic from uic_master WHERE status = 'OPEN' ")->result_array(),
			'operator'		=> $this->db->query("SELECT operator, value from operator_master WHERE status = 'OPEN' ")->result_array(),
			'actype'		=> $this->db->query("SELECT actype, value from actype_master WHERE status = 'OPEN' ")->result_array()
		];
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/fr',$data);
		$this->load->view('template/fo2');
		$this->load->view('template/foot');	
	}

	public function save_tdam()
	{
		$number = $this->input->post('number');
		$urutan = htmlspecialchars($this->input->post('urutan'));
		$operator = htmlspecialchars($this->input->post('operator'));
		$actype = htmlspecialchars($this->input->post('actype'));
		$crossreff = htmlspecialchars($this->input->post('crossreff'));
		$evaluation = htmlspecialchars($this->input->post('evaluation'));
		$proposeaction = $this->input->post('propose_action');
		$discussion = html_entity_decode($this->input->post('tdam_discussion'));
		$rectification = html_entity_decode($this->input->post('rectification'));
		$detailproblem = html_entity_decode($this->input->post('detailproblem'));
		$alert = htmlspecialchars($this->input->post('alert'));
		$acreg = htmlspecialchars($this->input->post('acreg'));
		$ata = htmlspecialchars($this->input->post('ata'));
		$system = htmlspecialchars($this->input->post('system'));
		$problem_title = htmlspecialchars($this->input->post('problem_title'));
		$detail = html_entity_decode($this->input->post('detail'));
		$uic = htmlspecialchars($this->input->post('uic'));
		$target_date = htmlspecialchars($this->input->post('target_date'));
		$releated = htmlspecialchars( $this->input->post('releated'));
		$cc = $this->input->post('cc');
		$pc = $this->input->post('pc');
		@$h_cc = count($cc);
		@$h_pc = count($pc);
		$user = $this->session->userdata('nopeg');
		$token = $this->Getdat_model->gettoken($user,$number);
		$h_token = count($token);
 		// awal
		if ($h_token == 0) {
			$ts= '';
		} else {
			foreach ($token as $key) 
			{
				$out[] = $key['token'].',';
			}
			$ts = implode("",$out);
		}
		if ($h_pc > 0 ) {
				// Jika PIC lebih dari satu
			for ($i=0; $i < $h_pc ; $i++) { 
				$pic[] = $pc[$i].',';
				$data3 = [
					'numberproblem' => $number,
					'user' => $pc[$i],
					'send' => date('Y-m-d'),
					'status' => 'PIC TDAM'
				];
				$this->Getdat_model->save_pic($data3);
			}
			$p_i  = implode(',',$pic);
			$pic_gabung = str_replace(',,',',', $p_i);

			$pic_save = strtolower($pic_gabung);
		} else if ($h_pc == 1) {	
						// jika PIC hanya satu
			$data3 = [
				'numberproblem' => $number,
				'user' => $pc,
				'send' => date('Y-m-d'),
				'status' => 'PIC TDAM'
			];
			$this->Getdat_model->save_pic($data3);
			$pic_save = strtolower($pc);
		}

		if ($h_cc > 0 ) { 
						// Jika CC lebih dari satu
			for ($i=0; $i < $h_cc ; $i++) { 
				$c_c[] = $cc[$i].',';
				$data3 = [
					'numberproblem' => $number,
					'user' => $cc[$i],
					'send' => date('Y-m-d'),
					'status' => 'CC'
				];
				$this->Getdat_model->save_pic($data3);					
			}
			$c_i  = implode(',',$c_c);
			$cc_gabung = str_replace(',,',',', $c_i);
			$cc_save = strtolower($cc_gabung);
		} else if ($h_cc == 1) {	
						// jika CC hanya satu
			$data3 = [
				'numberproblem' => $number,
				'user' => $cc,
				'send' => date('Y-m-d'),
				'status' => 'CC'
			];
			$this->Getdat_model->save_pic($data3);
			$cc_save = strtolower($cc);
		} else {
			$cc_save = '';
		}
		$data4 = 
		[
			'number' 	=> $number,
			'actype'	=> $actype,
			'operator'	=> $operator,
			'urutan'	=> $urutan
		];
		$this->Getdat_model->save_number($data4);
 		// akhir
 		// awal
		if (isset($_POST['save_tdam'])) {
			$data = 
			[
				'number' => $number,
				'crossreff' => $crossreff,
				'evaluationtype' => $evaluation,
				'operator' => $operator,
				'actype' => $actype,
				'alert' => $alert,
				'acreg' => $acreg,
				'ata' => $ata,
				'systems' => $system,
				'ProblemTitle' => $problem_title,
				'detailproblem' => $detailproblem,
				'PIC' => strtolower($pic_save),
				'cc' => strtolower($cc_save),
				'UIC' => $uic,
				'TargetDate' => $target_date,
				'status'	=> 'OPEN',
				'file'		=> $ts,
				'remarks'	=> '-',
				'releated'	=> $releated,
				'user'		=> $this->session->userdata('nopeg'),
				'create_at'	=> date('Y-m-d')
			];
			$this->Getdat_model->save_draft($data);
			$this->db->delete('problem_definition_tdam', ['EventID' => $crossreff]);
			$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
			$this->session->set_flashdata("Pesan", "Problem Definition TDAM is save to draft !");
			redirect('tdm','refresh');
		} else if(isset($_POST['assign_tdam'])){
			$data = 
			[
				'number' => $number,
				'crossreff' => $crossreff,
				'evaluationtype' => $evaluation,
				'operator' => $operator,
				'actype' => $actype,
				'alert' => $alert,
				'acreg' => $acreg,
				'ata' => $ata,
				'systems' => $system,
				'ProblemTitle' => $problem_title,
				'DetailProblem' => $detailproblem,
				'PIC' => strtolower($pic_save),
				'cc' => strtolower($cc_save),
				'UIC' => $uic,
				'TargetDate' => $target_date,
				'status'	=> 'OPEN',
				'file'		=> $ts,
				'remarks'	=> '-',
				'releated'	=> $releated,
				'user'		=> $this->session->userdata('nopeg'),
				'create_at'	=> date('Y-m-d')
			];
			$email_penerima = [];
			$user_email = strtolower($this->session->userdata('email'));

			if ($cc == '') {
				$email_penerima =  $pc;
				array_push($email_penerima,$user_email);

			} else {
				$email_penerima =  array_merge($pc,@$cc);
				array_push($email_penerima,$user_email);
			}
			$mail = $this->phpmailer_lib->load();
			$date = date('Y-m-d');
			$ac = cekactype($actype);
			$op = cekoperator($operator);
			$pengirim = $this->session->userdata('nama');
			$target = date('d F Y', strtotime($target_date));
			for ($s=0; $s < count($email_penerima) ; $s++) { 
				$mail->isSMTP();	
				$mail->Host = 'mail.gmf-aeroasia.co.id';
				$mail->Port = 25;
				$mail->SMTPAuth = true;
				$mail->Username = "reliability.management@gmf-aeroasia.co.id";
				$mail->Password = "Aeroas1@";
				$mail->setFrom('reliability.management@gmf-aeroasia.co.id', 'APPS IDEE');
				$mail->Subject = 'Task Assignment:'.$evaluation.':'.$problem_title.'';
				$mail->isHTML(true);  
				$mail->addAddress(strtolower($email_penerima[$s]));
				$mail->Body = '
				<!doctype html>
				<html>
				<body>
				<h4><b>Greetings!</b> </h4> </br>
				<p>Dear Fellow engineer, you have been assigned a task:</p></br>
				<body>
				<div>
				<div>
				<div>
				<ul>
				<li>PROBLEM TITLE :  '.$problem_title.'</li>
				<li>A/C TYPE  : '.$ac.'</li>
				<li>OPERATOR : '.$op.'</li>
				<li>ATA : '.$ata.'</li>
				<li>PIC : '.$pic_save.' </li>
				<li>Target Date : '.$target.' </li>
				<li>PROBLEM NO : <b>'.$number.'</b></li>
				<li>EVALUATION TYPE :  '.$evaluation.'</li>
				<li>Assigned By : <b><i>'.$pengirim.'</i></b></li>
				</ul>
				</div> 
				</div>
				</div>
				<p>Thank you for your time and assistance</p>
				<p><i>This message was automatically generate by  IDEE apps</i></p><br>
				<p><a target="_blank"  href="'.base_url().'">IDEE Web Link</a></p><br>
				<p>Regards,</p><br>
				<p>Reliability Management (TER-1)</p>
				</br>
				</body>
				</html>
				';	

			}
			if(!$mail->send()) {
				echo 'Pesan email tidak terkirim.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				$this->db->set('status', 'Sent Success');
				$this->db->where('numberproblem', $number);
				$this->db->update('assigntemp');
				$this->Getdat_model->save_assign($data);
				$this->db->set('status', 'ASSIGN');
				$this->db->where('EventID', $crossreff);
				$this->db->update('problem_definition_tdam');
				$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
				$this->session->set_flashdata("Pesan", "ASSIGN IS SEND");
				redirect('tdm','refresh');
			}

		}
 		// akhir




	}

}

/* End of file Rd.php */
/* Location: ./application/controllers/Rd.php */