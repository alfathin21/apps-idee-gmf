<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		
	}



	public function index()
	{

		$this->load->helper('file');
		$this->load->library('phpmailer_lib');
		$mail = $this->phpmailer_lib->load();
		$file_path = APPPATH . "/assets/reminder.txt";
		$pb_tdm = $this->db->query("SELECT TargetDate,ata,create_at,number,crossreff,evaluationtype,actype,problemtitle,PIC,status FROM problem_definition WHERE evaluationtype = 'TDAM' and (status = 'OPEN' or status = 'PROGRESS') ORDER BY create_at ASC ")->result_array();
		$pb_erc = $this->db->query("SELECT TargetDate,ata,create_at,number,crossreff,evaluationtype,actype,problemtitle,PIC,status FROM problem_definition WHERE evaluationtype = 'ERC - SRI' and (status = 'OPEN' or status = 'PROGRESS') ORDER BY create_at ASC ")->result_array();
		$pb_oth = $this->db->query("SELECT TargetDate,ata,create_at,number,crossreff,evaluationtype,actype,problemtitle,PIC,status FROM problem_definition WHERE evaluationtype <> 'ERC - SRI' and evaluationtype <> 'TDAM' and (status = 'OPEN' or status = 'PROGRESS')  ORDER BY evaluationtype DESC, create_at ASC ")->result_array();

		$no = 1;
		$no2 = 1;
		$no3 = 1;

		$a = '<h2><center>IDEE EVALUATION TDAM</center></h2>
		<table style="border: 1px solid black;">
		<tr>
		<th style="border: 1px solid black;">No</th>
		<th style="border: 1px solid black;">Number</th>
		<th  style="border: 1px solid black;">Cross Reff</th>
		<th  style="border: 1px solid black;">A/C TYPE</th>
		<th  style="border: 1px solid black;">ATA</th>
		<th  style="border: 1px solid black;">Evaluation Type</th>
		<th  style="border: 1px solid black;">Assigment Month</th>
		<th  style="border: 1px solid black;">Problem Title</th>
		<th  style="border: 1px solid black;">PIC</th>
		<th  style="border: 1px solid black;">Target Date</th>
		<th  style="border: 1px solid black;">Status</th>
		</tr>
		<tbody>
		';
		$b = '<h2><center>IDEE EVALUATION ERC - SRI</center></h2><table style="border: 1px solid black;">
		<tr>
		<th  style="border: 1px solid black;">No</th>
		<th  style="border: 1px solid black;">Number</th>
		<th  style="border: 1px solid black;">Cross Reff</th>
		<th  style="border: 1px solid black;">A/C TYPE</th>
		<th  style="border: 1px solid black;">ATA</th>
		<th  style="border: 1px solid black;">Evaluation Type</th>
		<th  style="border: 1px solid black;">Assigment Month</th>
		<th  style="border: 1px solid black;">Problem Title</th>
		<th  style="border: 1px solid black;">PIC</th>
		<th  style="border: 1px solid black;">Target Date</th>
		<th  style="border: 1px solid black;">Status</th>
		</tr>

		<tbody>
		';

		$c = '<h2><center>IDEE EVALUATION OTHERS</center></h2><table style="border: 1px solid black;">
		
		<tr>
		<th style="border: 1px solid black;">No</th>
		<th style="border: 1px solid black;">Number</th>
		<th style="border: 1px solid black;">Cross Reff</th>
		<th style="border: 1px solid black;">A/C TYPE</th>
		<th style="border: 1px solid black;">ATA</th>
		<th style="border: 1px solid black;">Evaluation Type</th>
		<th style="border: 1px solid black;">Assigment Month</th>
		<th style="border: 1px solid black;">Problem Title</th>
		<th style="border: 1px solid black;">PIC</th>
		<th style="border: 1px solid black;">Target Date</th>
		<th style="border: 1px solid black;">Status</th>
		</tr>

		<tbody>
		';
		foreach ($pb_tdm as $key ) {
			$a .=  "
			<tr>
			<td style='border: 1px solid black;'>".$no."</td>
			<td style='border: 1px solid black;'>".$key['number']."</td>
			<td style='border: 1px solid black;'>".$key['crossreff']."</td>
			<td style='border: 1px solid black;'>".cekactype($key['actype'])."</td>
			<td style='border: 1px solid black;'>".$key['ata']."</td>
			<td style='border: 1px solid black;'>".$key['evaluationtype']."</td>
			<td style='border: 1px solid black;'>". date('Y - m', strtotime($key["create_at"]))."</td>
			<td style='border: 1px solid black;'>".$key['problemtitle']."</td>
			<td style='border: 1px solid black;'>".strtoupper(backtoname($key['PIC']))."</td>
			<td style='border: 1px solid black;'>".date('d F Y', strtotime($key["TargetDate"]))."</td>
			<td style='border: 1px solid black;'>".$key['status']."</td>
			</tr>
			";
			$no++;
		}

		foreach ($pb_oth as $key ) {
			$c .=  "
			<tr  style='border: 1px solid black;'>
			<td style='border: 1px solid black;'>".$no3."</td>
			<td style='border: 1px solid black;'>".$key['number']."</td>
			<td style='border: 1px solid black;'>".$key['crossreff']."</td>
			<td style='border: 1px solid black;'>".cekactype($key['actype'])."</td>
			<td style='border: 1px solid black;'>".$key['ata']."</td>
			<td style='border: 1px solid black;'>".$key['evaluationtype']."</td>
			<td style='border: 1px solid black;'>". date('Y - m', strtotime($key["create_at"]))."</td>
			<td style='border: 1px solid black;'>".$key['problemtitle']."</td>
			<td style='border: 1px solid black;'>".strtoupper(backtoname($key['PIC']))."</td>
			<td style='border: 1px solid black;'>".date('d F Y', strtotime($key["TargetDate"]))."</td>
			<td style='border: 1px solid black;'>".$key['status']."</td>
			</tr>
			";
			$no3++;
		}

		foreach ($pb_erc as $key ) {
			$b .=  "
			<tr  style='border: 1px solid black;'>
			<td style='border: 1px solid black;'>".$no2."</td>
			<td style='border: 1px solid black;'>".$key['number']."</td>
			<td style='border: 1px solid black;'>".$key['crossreff']."</td>
			<td style='border: 1px solid black;'>".cekactype($key['actype'])."</td>
			<td style='border: 1px solid black;'>".$key['ata']."</td>
			<td style='border: 1px solid black;'>".$key['evaluationtype']."</td>
			<td style='border: 1px solid black;'>". date('Y - m', strtotime($key["create_at"]))."</td>
			<td style='border: 1px solid black;'>".$key['problemtitle']."</td>
			<td style='border: 1px solid black;'>".strtoupper(backtoname($key['PIC']))."</td>
			<td style='border: 1px solid black;'>".date('d F Y', strtotime($key["TargetDate"]))."</td>
			<td style='border: 1px solid black;'>".$key['status']."</td>
			</tr>
			";
			$no2++;
		}

		$a .= '</tbody></table>';
		$b .= '</tbody></table>';
		$c .= '</tbody></table>';
		$arr =  array('list-TE@gmf-aeroasia.co.id');
		$mail->isSMTP();	
		$mail->CharSet = 'UTF-8';
		$mail->Host = 'mail.gmf-aeroasia.co.id';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "reliability.management@gmf-aeroasia.co.id";
		$mail->Password = "Aeroas1@";
		$mail->setFrom('reliability.management@gmf-aeroasia.co.id', 'Reminder Apps IDEE');
		$mail->Subject = 'IDEE Evaluation status';
		$mail->isHTML(true);  
		foreach ($arr as $key ) {
			$mail->addAddress($key);
			
		}
		$mail->Body = '
		<!doctype html>
		<html>
		<body>
		<h4>Dear Fellow Engineer ,</h4> </br>
		<p>Following are status of TDAM, ERC-SRI, Type Meeting and Other evaluations which still OPEN and PROGRESS refers to IDEE Apps.</p></br>
		<p>Please follow up these evaluations on IDEE Apps before the target date.</p>

		'.$a.'<br>
		'.$b.'<br>
		'.$c.'
		<br>
		<p><a target="_blank"  href="'.base_url().'">IDEE Web Link</a></p><br>
		<p>Regards,</p><br>
		<p>Reliability Management (TER-1)</p>
		</br>
		</body>
		</html>
		';	

		
		if(!$mail->send()) {
			$data = "Pesan email tidak terkirim. ".date('l, d-m-Y  h:i:s a'); 
			
		} else {
			$data = "Pesan email terkirim. ".date('l, d-m-Y  h:i:s a');
		} 
		if (write_file(FCPATH . '/assets/reminder.txt', $data) == FALSE)
		{
			echo 'Unable to write the file';

		} else {
			echo 'File written!';                           
		}
	}
	public function send_mail() { 
        //Load email library
        

         //SMTP & mail configuration
         $config = array(
             'protocol'  => 'smtp',
             'smtp_host' => 'mail.gmf-aeroasia.co.id',
             'smtp_port' => 8008,
             'smtp_user' => 'app.notif',
             'smtp_pass' => 'app.notif',
             'mailtype'  => 'html',
             'charset'   => 'utf-8'
         );
          $this->load->library('email',$config);
         $this->email->initialize($config);
         $this->email->set_mailtype("html");
         $this->email->set_newline("\r\n");

         //Email content
         $htmlContent = '<h1>Sending email via SMTP server</h1>';
         $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

         $this->email->to('fathinhidayat14@gmail.com');
         $this->email->from('app.notif@gmf-aeroasia.co.id','MyWebsite');
         $this->email->subject('How to send email via SMTP server in CodeIgniter');
         $this->email->message($htmlContent);

         //Send email
         $this->email->send();
      }

}

/* End of file Reminder.php */
/* Location: .//C/Users/Alfathin Hidayatullo/AppData/Local/Temp/fz3temp-2/Reminder.php */
//pak scheduler yang buat narik data TDAM  sama running script Reminder email ini, aku simpen di server 172.16.40.163