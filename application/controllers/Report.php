<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function index()
	{
		is_log_in();
		$this->load->model('Getdat_model');
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
		    'format' => 'A4',
		    'orientation' => 'L'
		]);
		$data = $this->load->view('page/report', [], TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output();
	}


}

/* End of file Pdf.php */
/* Location: ./application/controllers/Pdf.php */


?>