<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_log_in();
		cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
 		$this->load->model('User_model');
 		$this->db_users = $this->load->database('db_users', TRUE);
 		error_reporting(0);
	}
	public function index()
	{
		$data['title'] = 'Autorization Control';
		$data['role']  =  $this->User_model->role_menu();
		$this->load->view('template/head', $data);
		$this->load->view('template/side');
		$this->load->view('page/rg');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}

	public function role_akses($role_id)
	{
		$data['title'] = "Role Akses";
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/role_akses');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}
	public function akses()
	{
		$data['title'] = 'Autorization Control';
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/role_akses');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}

	public function list_role()
	{
		$list_role =   $this->User_model->role_menu();
		echo json_encode($list_role);
	}
	public function list_uic()
	{
			$query = $this->db->get("uic_master")->result_array();
		echo json_encode($query);
	}

	public function save_role()
	{
		$role_name = $this->input->post('role');
		$data = 
		[
			'id'	=> '',
			'role'	=> $role_name
		];
	    $this->db_users->insert('user_role', $data);
	    echo json_encode('Success');
	}
	public function list_user()
	{
		$sv = $this->User_model->list_user();
		echo json_encode($sv);
	}
	public function list_alert()
	{
		$sv = $this->db->get('alert_master')->result_array();
		echo json_encode($sv);
	}
	public function list_operator()
	{
		$sv = $this->db->get('operator_master')->result_array();
		echo json_encode($sv);
	}
	public function list_actype()
	{
		$sv = $this->db->get('actype_master')->result_array();
		echo json_encode($sv);
	}
	public function list_evaluation()
	{
		$sv = $this->db->get('evaltype_master')->result_array();
		echo json_encode($sv);
	}
	public function reg()
	{
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$role = $this->input->post('role');
		$customer = $this->input->post('customer');
		$jabatan = $this->input->post('jabatan');
		$email = $this->input->post('email');
		$data2 = 
		[
			"username" => $username,
			"password" => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
		];
		$data = 
		[
			"nama"     => $name,
			"nopeg"	   => $username,
			"email"	   => $email,	
			"unit"	   => $customer,
			"jabatan"  => $jabatan,
			"is_active" => '1',
			"role_id"	   => $role
		];
		$this->User_model->saveuser($data2);
		$sv = $this->User_model->saveuserdetail($data);
		echo json_encode($sv);
	}
	public function update_user()
	{
		$nopeg = $this->input->post('nopeg');
		$role_id = $this->input->post('role_id');
		$this->db_users->query("UPDATE users_detail SET role_id = '$role_id' WHERE nopeg = '$nopeg' ");
		$this->session->set_flashdata("Pesan", "Role user change");
		echo json_encode('Success');
	}
	public function get_data_user()
	{
		$list = $this->User_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['nopeg'];
			$row[] = $field['nama'];
			$row[] = $field['role'];
			$row[] = '<button data-toggle="modal" data-target="#myModal" data-id='.$field['id'].' data-nopeg='.$field['nopeg'].' data-nama='.$field['nama'].' data-email='.$field['email'].' data-jabatan='.$field['jabatan'].' id="edit" data-role_id='.$field['role_id'].'  class="btn btn-success btn-sm"  ><i class="fa fa-edit"></i></button>';
			
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->User_model->count_all(),
			"recordsFiltered" => $this->User_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}


	function cekusername()
	{
		$username = $this->input->post('username');
		$cek = $this->User_model->cek($username);
		echo json_encode($cek);
	}

	public function update()
	{
		$tab = $this->input->get('tab');
		$i = $this->input->get('i');
		$p = $this->input->get('p');
		$this->db->set('status', $p);
		$this->db->where('id',$i);
		$this->db->update($tab, $object);
		if ($p == 'HIDDEN') {
			$this->session->set_flashdata("Pesan", "Update Success !");
		} else {
			$this->session->set_flashdata("Pesan", "Update Success Open !");
		}
		redirect('master','refresh');
	}

	function save()
	{
		$uic = $this->input->post('uic_name');

		$data = 
		[
			'id' => '',
			'uic' => strtoupper($uic),
			'status' => 'OPEN'
		];
		$this->db->insert('uic_master', $data);
		$this->session->set_flashdata("Pesan", "Save Success !");
		redirect('master','refresh');
	}
	function saveevaluation()
	{
		$uic = $this->input->post('uic_name');

		$data = 
		[
			'id' => '',
			'evaltype' => strtoupper($uic),
			'status' => 'OPEN'
		];
		$this->db->insert('evaltype_master', $data);
		$this->session->set_flashdata("Pesan", "Save Success !");
		redirect('master','refresh');
	}
	function savealert()
	{
		$uic = $this->input->post('alert_name');

		$data = 
		[
			'id' => '',
			'alert' => strtoupper($uic),
			'status' => 'OPEN'
		];
		$this->db->insert('alert_master', $data);
		$this->session->set_flashdata("Pesan", "Save Success !");
		redirect('master','refresh');
	}	
	function saveoperator()
	{
		$uic = $this->input->post('operator_name');
		$value = $this->input->post('value_name');

		$data = 
		[
			'id' => '',
			'operator' => strtoupper($uic),
			'value' => strtoupper($value),
			'status' => 'OPEN'
		];
		$this->db->insert('operator_master', $data);
		$this->session->set_flashdata("Pesan", "Save Success !");
		redirect('master','refresh');
	}
	function saveactype()
	{
		$uic = $this->input->post('operator_name');
		$value = $this->input->post('value_name');

		$data = 
		[
			'id' => '',
			'actype' => strtoupper($uic),
			'value' => strtoupper($value),
			'status' => 'OPEN'
		];
		$this->db->insert('actype_master', $data);
		$this->session->set_flashdata("Pesan", "Save Success !");
		redirect('master','refresh');
	}

	

}

/* End of file Role.php */
/* Location: ./application/controllers/Role.php */