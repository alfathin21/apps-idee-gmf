<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Sd extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Getdat_model');
		is_log_in();
		error_reporting(0);
	}
	public function index()
	{
		$data['title'] = 'Display Status';
		$data['te'] = $this->Getdat_model->gette();
		$data['uic'] = $this->db->query("SELECT uic from uic_master WHERE status = 'OPEN' ")->result_array();
		$data['evaltype'] = $this->db->query("SELECT evaltype from evaltype_master WHERE status = 'OPEN' ")->result_array();
		$data['operator'] = $this->db->query("SELECT operator, value from operator_master WHERE status = 'OPEN' ")->result_array();
		$data['actype'] = $this->db->query("SELECT actype, value from actype_master WHERE status = 'OPEN' ")->result_array();
		$this->load->view('template/head', $data);
		$this->load->view('template/side');
		$this->load->view('page/sd');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');
	}
	public function search_evaluation()
	{
		//use array to save post data
		$q_param['evaluation']	= htmlspecialchars($this->input->post('evaluation'));
		$q_param['operator'] 	= htmlspecialchars($this->input->post('operator'));
		$q_param['output_component'] = htmlspecialchars($this->input->post("output_component"));
		$q_param['role_id'] 	= htmlspecialchars($this->input->post("role_id"));
		$q_param['uic'] 		= htmlspecialchars($this->input->post("uic"));
		$q_param['keyword'] 	= htmlspecialchars($this->input->post("keyword"));
		$q_param['actype'] 		= htmlspecialchars($this->input->post("actype"));
		$q_param['alerttype'] 	= htmlspecialchars($this->input->post("alerttype"));
		$q_param['pic'] 		= $this->input->post("pic[]");
		$q_param['crossreff'] 	= htmlspecialchars($this->input->post("crossreff"));
		$q_param['date_to'] 	= htmlspecialchars($this->input->post("date_to"));
		$q_param['date_from'] 	= htmlspecialchars($this->input->post("date_from"));
		$q_param['onwatch'] 	= htmlspecialchars($this->input->post("onwatch"));
		$result = $this->Getdat_model->problem_display_evaluation($q_param);
		$response = [];
		$no = 1;
		foreach ($result as $key) {
			$h['no'] = $no;
			$h['number'] = $key["number"];
			$h['crossreff'] = $key["crossreff"];
			$h['operator'] = cekoperator($key["operator"]);
			$h['actype'] 	= cekactype($key["actype"]);
			$h['asign'] = date('d F Y', strtotime($key["create_at"]));
			$h['alert'] = $key["alert"];
			$h['acreg'] = $key["acreg"];
			$h['ata'] = $key["ata"];
			$h['systems'] = $key["systems"];
			$h['problemtitle'] = $key["problemtitle"];
			$h['evaluationtype'] = $key["evaluationtype"];
			$h['detailproblem'] = $key["detailproblem"];
			$c =  backtoname($key["PIC"]);
			$h['PIC'] = strtoupper($c);
			$h['cc'] = $key["cc"];
			$h['UIC'] = $key["UIC"];
			$h['TargetDate'] = date('d F Y', strtotime($key["TargetDate"]));
			$h['EvalDate'] = date('Y - m', strtotime($key["create_at"]));
			$h['srieffectivenees'] = $key["srieffectivenees"];
			$h['problemanalisis'] = $key["problemanalisis"];
			if ($key["status"] == "OPEN") {
				$h['status'] = '<label class="badge badge-success">OPEN</label>';
			} else if ($key['status'] == "REVIEW") {
				$h['status'] = '<label class="badge badge-danger">REVIEW</label>';

			} else if ($key['status'] == "PROGRESS") {
				$h['status'] = '<label class="badge badge-warning">PROGRESS</label>';

			}else if ($key['status'] == "CLOSE") {
				$h['status'] = '<label class="badge badge-primary">CLOSE</label>';

			}else if ($key['status'] == "HIDDEN") {
				$h['status'] = '<label class="badge badge-default">HIDDEN</label>';
			}
			if ($key["remarks"] == null or $key["remarks"] == 'null' or $key["remarks"] == NULL) {  	
				$h['remarks'] = '-';
			} else {
				$h['remarks'] = $key["remarks"];
			}
			if ($key["file"] == '') {
				$h['file'] = '0';
			} else {
				$cek = $key['file'];
				$array_jadi = explode(',', $cek);
				$hitung = count($array_jadi);
				$h['file'] = $hitung - 1;
			}
			$no++;
			array_push($response, $h);
		}
		$newresponse = array(
			'data' => $response
		);
		echo json_encode($newresponse);
	}

	public function search_solution()
	{
		//use array to save post data
		$q_param['evaluation']	= htmlspecialchars($this->input->post('evaluation'));
		$q_param['operator'] 	= htmlspecialchars($this->input->post('operator'));
		$q_param['output_component'] = htmlspecialchars($this->input->post("output_component"));
		$q_param['role_id'] 	= htmlspecialchars($this->input->post("role_id"));
		$q_param['keyword'] 	= htmlspecialchars($this->input->post("keyword"));
		$q_param['uic'] 	= htmlspecialchars($this->input->post("uic"));
		$q_param['actype'] 		= htmlspecialchars($this->input->post("actype"));
		$q_param['alerttype'] 	= htmlspecialchars($this->input->post("alerttype"));
		$q_param['pic'] 		= $this->input->post("pic[]");
		$q_param['crossreff'] 	= htmlspecialchars($this->input->post("crossreff"));
		$q_param['date_to'] 	= htmlspecialchars($this->input->post("date_to"));
		$q_param['date_from'] 	= htmlspecialchars($this->input->post("date_from"));
			$q_param['onwatch'] 	= htmlspecialchars($this->input->post("onwatch"));
		$no = 1;
		$result = $this->Getdat_model->problem_display_solutions($q_param);
		$response = [];
		$solusi = [];
		foreach ($result as $key) {
			$h['no'] = $no;
			$h['number'] = $key["number"];
			$h['crossreff'] = $key["crossreff"];
			$h['evaluationtype'] = $key["evaluationtype"];
			$h['operator'] = cekoperator($key["operator"]);
			$h['actype'] 	= cekactype($key["actype"]);
			$h['asign'] = $key["create_at"];
			$h['alert'] = $key["alert"];
			$h['acreg'] = $key["acreg"];
			$h['ata'] = $key["ata"];
			$h['systems'] = $key["systems"];
			$h['problemtitle'] = $key["problemtitle"];
			$h['evaluationtype'] = $key["evaluationtype"];
			$h['detailproblem'] = $key["detailproblem"];
			$c =  backtoname($key["PIC"]);
			$h['PIC'] = strtoupper($c);
			$h['cc'] = $key["cc"];
			$h['UIC'] = $key["UIC"];
			$h['TargetDate'] = $key["TargetDate"];
			$h['EvalDate'] =$key["create_at"];
			$h['problemanalisis'] = $key["problemanalisis"];
			$h['srieffectivenees'] = $key["srieffectivenees"];
			$h['status'] = $key["status"];
			for ($i = 0; $i < 10; $i++) {
				if (!empty($key['solution' . $i])) {
					$solusiKeberapa = "solution" . $i;
					$row = $this->Getdat_model->join($solusiKeberapa, $key['solution' . $i]);
					$h['namasolusi'] = $row["solution"];
					$h['uicsolusi'] = $row["uic"];

					if ($row['last_update'] == '' or $row['last_update'] == null) {
						$h['last_update'] = '-';
					} else {
						$h['last_update'] = date('Y-m-d', strtotime($row["last_update"]));
					}
					if ($row['status'] < 30) {
						if ($row['status'] == '' or empty($row['status']) or $row['status'] == null) {
						$h['status_solusi'] = "<span class='text-center badge badge-danger'>0 %</span>";
						} else {
							$h['status_solusi'] = "<span class='text-center badge badge-danger'>" . $row['status'] . '%' . "</span>";
						}
					} else if ($row['status'] > 30 and $row['status'] < 70) {
						$h['status_solusi'] = "<span class='text-center badge badge-success'>" . $row['status'] . '%' . "</span>";
					} else {
						$h['status_solusi'] = "<span class='text-center badge badge-primary'>" . $row['status'] . '%' . "</span>";
					}
					 if (empty($row['file']) or $row['file'] == ',') {
				    	$h['fil_sol'] = '0';
				    } else{
				    	$cek = $row['file'];
						$array_jadi = explode(',', $cek);
						$hitung = count($array_jadi);
						$h['fil_sol'] = $hitung - 1;
				    }
				    if ($row["remarks"] == null or $row["remarks"] == 'null' or $row["remarks"] == NULL) {  	
						$h['remarks_status'] = '-';
				    } else {
				    	$h['remarks_status'] = $row["remarks"];
				    }

					array_push($response, $h);
				}
			}
			$no++;
		}
		$newresponse = array(
			'data' => $response
		);
		echo json_encode($newresponse);
	}







}











/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */

?>

