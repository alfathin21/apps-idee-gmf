<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends CI_Controller {
	public function index()
	{
		is_log_in();
		cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
		$data['title'] = 'Synchronization DB TDAM';
 		$this->load->view('template/head', $data);
 		$this->load->view('template/side', $data);
 		$this->load->view('page/sync');
 		$this->load->view('template/fo2');
 		$this->load->view('template/foot');
	}



}

/* End of file Sync.php */
/* Location: ./application/controllers/Sync.php */