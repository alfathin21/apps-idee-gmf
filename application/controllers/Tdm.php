<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tdm extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
		error_reporting(0);
	}
	public function index()
	{
		$data['title'] = 'New Issue TDAM';	
		$this->load->view('template/head', $data);
		$this->load->view('template/side', $data);
		$this->load->view('page/tdam_new');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');	
	}
	public function get_data_user()
	{
		$list = $this->Getdat_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
	
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['EventID'];
			$row[] = $field['ACTypeOrig'];
			$row[] = $field['ACRegOrig'];
			if (is_null($field['atachap']) or $field['atachap'] == '') {
				$row[] = '00';
			} else {
				$row[] = $field['atachap'];
			}
			$row[] = $field['FltNo'];
			$row[] = $field['Note'];
			$row[] = date('d F Y',strtotime($field['p_target_date']));
			$row[] = '<a href="rd?evid='.$field['EventID'].'" class="btn btn-success btn-sm"><i class="fa fa-external-link-square"></i></a>';
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Getdat_model->count_all(),
			"recordsFiltered" => $this->Getdat_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}





	public function save_tdam()
	{
		$number = $this->input->post('number');
		$urutan = htmlspecialchars($this->input->post('urutan'));
		$operator = htmlspecialchars($this->input->post('operator'));
		$actype = htmlspecialchars($this->input->post('acTYpe'));
		$crossreff = htmlspecialchars($this->input->post('crossreff'));
		$evaluation = htmlspecialchars($this->input->post('evaluation'));
		$proposeaction = htmlspecialchars($this->input->post('propose_action'));
		$discussion = htmlspecialchars($this->input->post('tdam_discussion'));
		$rectification = htmlspecialchars($this->input->post('rectification'));
		$detailproblem = htmlspecialchars($this->input->post('editor2'));
		$alert = htmlspecialchars($this->input->post('alert'));
		$acreg = htmlspecialchars($this->input->post('acreg'));
		$ata = htmlspecialchars($this->input->post('atachap'));
		$system = htmlspecialchars($this->input->post('system'));
		$problem_title = htmlspecialchars($this->input->post('problem_title'));
		$detail = $this->input->post('detail');
		$uic = htmlspecialchars($this->input->post('uic'));
		$target_date = htmlspecialchars($this->input->post('target_date'));
		$revisi_target = htmlspecialchars( $this->input->post('revisi_target'));
		$releated = htmlspecialchars( $this->input->post('releated'));
		$cc = $this->input->post('cc');
		$pc = $this->input->post('pc');
		@$h_cc = count($cc);
		@$h_pc = count($pc);
		$user = $this->session->userdata('nopeg');
		$token = $this->Getdat_model->gettoken($user,$number);
 		$h_token = count($token);
 		if ($h_token == 0) {
				$ts= '';
			} else {
				foreach ($token as $key) 
				{
					$out[] = $key['token'].',';
				}
				$ts = implode("",$out);
			}
			if ($h_pc > 0 ) {
				// Jika PIC lebih dari satu
				for ($i=0; $i < $h_pc ; $i++) { 
					$pic[] = $pc[$i].',';
					$data3 = [
						'numberproblem' => $number,
						'user' => $pc[$i],
						'send' => date('Y-m-d'),
						'status' => 'PIC TDAM'
					];
					$this->Getdat_model->save_pic($data3);
				}
				$p_i  = implode(',',$pic);
				$pic_gabung = str_replace(',,',',', $p_i);

				$pic_save = strtolower($pic_gabung);
			} else if ($h_pc == 1) {	
						// jika PIC hanya satu
				$data3 = [
					'numberproblem' => $number,
					'user' => $pc,
					'send' => date('Y-m-d'),
					'status' => 'PIC TDAM'
				];
				$this->Getdat_model->save_pic($data3);
				$pic_save = strtolower($pc);
			}

			if ($h_cc > 0 ) { 
						// Jika CC lebih dari satu
				for ($i=0; $i < $h_cc ; $i++) { 
					$c_c[] = $cc[$i].',';
					$data3 = [
						'numberproblem' => $number,
						'user' => $cc[$i],
						'send' => date('Y-m-d'),
						'status' => 'CC'
					];
					$this->Getdat_model->save_pic($data3);					
				}
				$c_i  = implode(',',$c_c);
				$cc_gabung = str_replace(',,',',', $c_i);
				$cc_save = strtolower($cc_gabung);
			} else if ($h_cc == 1) {	
						// jika CC hanya satu
				$data3 = [
					'numberproblem' => $number,
					'user' => $cc,
					'send' => date('Y-m-d'),
					'status' => 'CC'
				];
				$this->Getdat_model->save_pic($data3);
				$cc_save = strtolower($cc);
			} else {
				$cc_save = '';
			}
			$data4 = 
		 			[
		 				'number' 	=> $number,
		 				'actype'	=> $actype,
		 				'operator'	=> $operator,
		 				'urutan'	=> $urutan
		 			];
			$this->Getdat_model->save_number($data4);
		if (isset($_POST['save_tdam'])) {
			$data = 
					[
						'number' => $number,
						'crossreff' => $crossreff,
						'evaltype' => $evaluation,
						'operator' => $operator,
						'actype' => $actype,
						'acreg' => $acreg,
						'alert' => $alert,
						'ata' => $ata,
						'systems' => $system,
						'problem_title' => $problem_title,
						'proposeaction' => $proposeaction,
						'discussion' => $discussion,
						'rectification' => $rectification,
						'detailproblem' => $detailproblem,
						'file' => $ts,
						'pic' => strtolower($pic_save),
						'cc' => strtolower($cc_save),
						'target_date' => $target_date,
						'revisi_target' => $revisi_target,
						'uic' => $uic,
						'user'		=> $this->session->userdata('nopeg'),
						'create_at'	=> date('Y-m-d'),
						'releated'	=> $releated
					];
			$this->Getdat_model->save_tdam($data);
			$this->db->delete('temp_token',['user' => $user, 'number' => $number]);
			$this->session->set_flashdata("Pesan", "Problem Definition TDAM is save to draft !");
			redirect('tdm','refresh');
		} else if (isset($_POST['assign_tdam'])) {
			$data = 
	 				[
	 					'number' => $number,
	 					'crossreff' => $crossreff,
	 					'evaluationtype' => $evaluation,
	 					'operator' => $operator,
	 					'actype' => $actype,
	 					'alert' => $alert,
	 					'acreg' => $acreg,
	 					'ata' => $ata,
	 					'systems' => $system,
	 					'ProblemTitle' => $problem_title,
	 					'DetailProblem' => $detail,
	 					'PIC' => strtolower($pic_save),
	 					'cc' => strtolower($cc_save),
	 					'UIC' => $uic,
	 					'TargetDate' => $target_date,
	 					'RevisiTarget' => $revisi_target,
	 					'status'	=> 'OPEN',
	 					'file'		=> $ts,
	 					'remarks'	=> '-',
	 					'releated'	=> $releated,
	 					'user'		=> $this->session->userdata('nopeg'),
	 					'create_at'	=> date('Y-m-d')
	 				];
	 				if ($cc == '') {
	 					$email_penerima =  $pc;
	 				} else {
	 					$email_penerima =  array_merge($pc,@$cc);
	 				}
				
		}

	}
}

/* End of file Tdm.php */
/* Location: ./application/controllers/Tdm.php */