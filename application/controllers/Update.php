<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update extends CI_Controller {
	public function index()
	{
		is_log_in();
		cekRole($this->session->userdata('role_id'));
 		$this->load->model('Getdat_model');
 		$nopeg = $this->session->userdata('nopeg');
		$number = $this->input->get('number');
		$cek = $this->Getdat_model->draft($number,$nopeg);
		if ($cek == NULL or empty($cek)) {
			$this->session->set_flashdata("Pesan", "Number draft not found !");	
			redirect('draft','refresh');
		} 
		$data = 
		[
			'title' => 'Update Draft',
			're'	=>  $cek
		];
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/draft');
		$this->load->view('template/fo2');	
		$this->load->view('template/foot');	
	} 
}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */