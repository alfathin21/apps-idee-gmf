<?php 
class Upload extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		is_log_in();
		$this->load->helper(array('url','file'));
		$this->load->model('Getdat_model');
	}
	public function index(){
		$data['title'] = 'Upload';
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/upload');
		$this->load->view('template/fo2');
		$this->load->view('template/foot');	
	}
	Public function proses_upload()
	{
		$nama_file = $_FILES['userfile']['name'];
			$save = str_replace(' ', ',', $nama_file);
		$save1 = str_replace('#', ',', $save);
		$save2 = str_replace('_', ',', $save1);
		$save3 = str_replace('&', '', $save2);
		$_FILES['userfile']['name'] =  $save3;
		$config['upload_path']   = FCPATH.'/files/';
		$config['allowed_types'] = '|jpg|png|pdf|docx|xlsx|doc|txt|csv';
		$this->load->library('upload',$config);
		if($this->upload->do_upload('userfile')){
			$token=$this->input->post('token_file');
			$nama=$this->upload->data('file_name');
			$data = 
			[
				"token" 	=> $token,
				"user"		=> $this->session->userdata('nopeg'),
				"number" 	=> 'TDAM'
			];
			$this->db->insert('temp_token',$data);
			$this->db->insert('attach',array('nama_file'=>$save3,'token'=>$token));
		}
	}
	Public function remove_file()
	{
		$token=$this->input->post('token');
		$foto=$this->db->get_where('attach',array('token'=>$token));
		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_file=$hasil->nama_file;
			if(file_exists($file=FCPATH.'/files/'.$nama_file)){
				unlink($file);
			}
			$this->db->delete('temp_token',array('token'=>$token));
			$this->db->delete('attach',array('token'=>$token));
		}
	}

	public function finish()
	{
		$this->db->empty_table('temp_token');
		$this->session->set_flashdata("Pesan", "Upload file success");	


	}

}
?>