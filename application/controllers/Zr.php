<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Zr extends CI_Controller {
	public function index()
	{
		is_log_in();
		cekRole($this->session->userdata('role_id'));
		$this->load->model('Getdat_model');
		$number = htmlspecialchars($this->input->post('number'));
		$cek = $this->db->query("SELECT * FROM problem_definition JOIN historirevision on problem_definition.number = historirevision.rnumber where problem_definition.number='$number' and historirevision.rnumber = '$number' ORDER BY dateinsert DESC LIMIT 1 ")->row_array();
		if ($cek) {
			$data['title'] = 'Rev Issue';
			$data['revisi'] = $cek;
			$data['te'] = $this->Getdat_model->gette();
			$revisi = $cek['revisionnumber'];
			$str2 = explode("-", $revisi);
			$a = end($str2);
			$a += 1;
			if ($a < 10) {
				$a = str_pad($a, 2, "0", STR_PAD_LEFT);
			}
			$last = key($str2);
			$str2[$last] = $a;
			$akhir =  implode($str2,"-");
			$data['number'] = $akhir;
			$this->load->view('template/head', $data);
			$this->load->view('template/side', $data);
			$this->load->view('page/zr', $data);
			$this->load->view('template/fo2');
			$this->load->view('template/foot',$data);	
		} else {
			$this->session->set_flashdata("Pesan", "Number problem is not found !");
			redirect('pd','refresh');
		}
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */

?>

