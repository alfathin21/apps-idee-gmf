<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Getdat_model extends CI_Model {
	var $table = 'problem_definition_tdam'; 
    var $column_order = array('EventID','ACTypeOrig','ACRegOrig','atachap','FltNo','Note','p_target_date');
    var $column_search = array('EventID','ACTypeOrig','ACRegOrig','atachap','FltNo','Note','p_target_date'); 
    var $order = array('EventID' => 'DESC');
	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('db_employe', TRUE);
		$this->db3 = $this->load->database('db_tdam', TRUE);
		$this->db4 = $this->load->database('db_mcdr', TRUE);
		$this->db_users = $this->load->database('db_users', TRUE);
	}

	public function cekcrossreff($crossreff)
	{
		
		return $this->db3->query("SELECT p_id FROM tbl_preventive_action where p_eventid = '$crossreff' AND p_uic LIKE '%TE%' ")->row_array();
	}
	public function saveaction($data)
	{
		$this->db->insert('action_log', $data);
	}

	public function updatefollow($id,$analysis)
	{
		$date = date('Y-m-d');
		$this->db3->set('p_follow_up',$analysis);
		$this->db3->set('p_status',3);
		$this->db3->set('InsertDate',$date);
		$this->db3->where('p_id', $id);
	    $this->db3->update('tbl_preventive_action');
	}

	public function getevaluation($number,$pic)
	{
		return $this->db->query("SELECT * FROM problem_definition where number = '$number' AND PIC LIKE '%$pic%' ")->row_array();
	}

	public function getnumber($number)
	{
		return $this->db->get_where('problem_definition', ['number' => $number])->row_array();
	}
	public function histori()
	{
		return $this->db->query("SELECT number FROM problem_definition where status = 'HIDDEN'")->result_array();
	}
	public function solusi($actype, $ata)
	{
		return $this->db->get_where('solutions', ['actype' => $actype, 'ata' => $ata])->result_array();
	}
	public function assign()
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d');
		return $this->db->query("SELECT * FROM problem_definition JOIN assigntemp on problem_definition.number = assigntemp.numberproblem where assigntemp.send = '$date' AND assigntemp.status = 'pending' AND assigntemp.user = 'Admin' ")->result_array();
	}
	Public function getproblem($pic)
	{
	    
		return $this->db->query("SELECT * FROM problem_definition where PIC LIKE '%$pic%' AND (status = 'OPEN' OR status = 'PROGRESS') ")->result_array();
	}

	public function searchdata($user)
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d');
		$this->db->select('numberproblem');
		$this->db->from('assigntemp');
		$this->db->where('send',$date);
		$this->db->where('user',$user);
		$query = $this->db->get()->result_array();
		return $query;
	}
	public function searchpic($number)
	{
		$this->db->select('*');
		$this->db->from('problem_definition');
		$this->db->where('number',$number);
		$query = $this->db->get()->result_array();
		return $query;
	}
	public function searchemail($pic)
	{
		$this->db2->select('EMAIL');
		$this->db2->from('tabmail');
		$this->db2->where('NOPEG',$pic);
		$query = $this->db2->get()->result_array();
		return $query;
	}

	public function gettoken($user,$number)
	{
		
		return $this->db->query("SELECT * FROM temp_token where user = '$user' and number = '$number' ")->result_array();
	}



	public function getempoleye($no, $column)
	{
		$this->db2->select('nama');
		$this->db2->limit(10);
		$this->db2->from('v_tabpersonel_active');
		$this->db2->like('nopeg', $no);
		return $this->db2->get()->result_array();
	}

	public function gette()
	{
		return $this->db2->query("SELECT nopeg,nama,EMAIL,unit,jabatan FROM v_tabpersonel_active WHERE unit LIKE '%JKTTE%'")->result_array();
	}

	public function save_pic($data3)
	{
		return $this->db->insert('assigntemp', $data3);
	}

	public function save_draft($data)
	{
		return $this->db->insert('draft', $data);
	}

	public function save_revisi($data)
	{
		return $this->db->insert('historirevision', $data);
	}

	public function save_assign($data)
	{
		return $this->db->insert('problem_definition', $data);
	}
	public function update_draft($data)
	{

		$this->db->set('crossreff',$data['crossreff']);
		$this->db->set('evaluationtype',$data['evaluationtype']);
		$this->db->set('operator',$data['operator']);
		$this->db->set('actype',$data['actype']);
		$this->db->set('alert',$data['alert']);
		$this->db->set('acreg',$data['acreg']);
		$this->db->set('ata',$data['ata']);
		$this->db->set('systems',$data['systems']);
		$this->db->set('ProblemTitle',$data['ProblemTitle']);
		$this->db->set('DetailProblem',$data['DetailProblem']);
		$this->db->set('PIC',$data['PIC']);
		$this->db->set('cc',$data['cc']);
		$this->db->set('UIC',$data['UIC']);
		$this->db->set('TargetDate',$data['TargetDate']);
		$this->db->set('status',$data['status']);
		$this->db->set('file',$data['file']);
		$this->db->set('remarks',$data['remarks']);
		$this->db->set('releated',$data['releated']);
		$this->db->set('user',$data['user']);
		$this->db->set('create_at',date('Y-m-d'));
		$this->db->where('number',$data['number']);
		return $this->db->update('draft');
	}
	public function draft($number, $user)
	{
		$query = $this->db->query("SELECT * FROM draft where number = '$number' AND user = '$user' ")->row_array();
		return $query;
	}
	public function savelog($data)
	{
		return $this->db_users->insert('logreliability', $data);
	}
	public function ceknumberdraft($number)
	{
		$cek = $this->db->query("SELECT number from draft where number = '$number' ");
		$hasil = $cek->num_rows($cek);
		return $hasil;
	}
	public function ceknumberproblem($number)
	{
		$cek = $this->db->query("SELECT problem_definition.number from problem_definition join draft on problem_definition.number = draft.number where problem_definition.number = '$number' or draft.number = '$number' ");
		$hasil = $cek->num_rows($cek);
		return $hasil;
	}

	public function save_number($data)
	{
		return $this->db->insert('generatenumber', $data);
	}
	public function delete_draft($number)
	{
		$this->db->delete('draft', ['number' => $number]);
	}
	public function join($solusiKeberapa,$numberSolusi)
	{
		return $this->db->query("SELECT * FROM problem_definition JOIN solutions on problem_definition.$solusiKeberapa = solutions.numbersolution WHERE
			$solusiKeberapa = '$numberSolusi' ORDER BY create_at ASC ")->row_array();
	}
	public function getfile($token)
	{
		return $this->db->query("SELECT * from attach where token = '$token' ")->row_array();
	}
	public function getdraft($user)
	{

		$this->db->where('user', $user);
		return $this->db->get('draft')->result_array();
	}

	public function gettdam($user)
	{

		$this->db->where('user', $user);
		return $this->db->get('problem_definition_tdam')->result_array();
	}

	public function delete_problem($number)
	{
	
		$this->db->SET('status','HIDDEN');
		$this->db->where('number', $number);
		$this->db->update('problem_definition');
	}	
	public function save_user($tampung)
	{
		$nopeg = $tampung['nopeg'];
		$cek = $this->db_users->query("SELECT * from users_detail where nopeg = '$nopeg'")->num_rows();
		if ($cek > 0) {
		} else {
			return $this->db_users->insert('users_detail', $tampung);
		}
	}
	public function getDataSession($username)
	{
		return $this->db_users->query("SELECT * FROM users_detail WHERE nopeg = '$username' ")->row_array();
	}


	public function cekrole($role_id,$akses_menu)
	{

		return $this->db->query("SELECT * FROM user_access_menu  JOIN user_menu ON user_access_menu.menu_id = user_menu.id  WHERE role_id = '$role_id' AND menu = '$akses_menu'  ")->num_rows();
	}


public function problem_display_solutions(array $param = null)
	{
		$evaluation 	= $param['evaluation'];
		$operator 		= $param['operator'];
		$output_component = $param['output_component'];
		$role_id 	 	= $param['role_id'];
		$keyword 		= $param['keyword'];
		$uic 		    = $param['uic'];
		$actype 		= $param['actype'];
		$alerttype 		= $param['alerttype'];
		$pic	 		= $param['pic'];
		$crossreff 		= $param['crossreff'];
		$date_to 		= $param['date_to'];
		$date_from 		= $param['date_from'];
		$onwatch 		= $param['onwatch'];

		
		if (!empty($date_from) and !empty($date_to)) {
			$this->db->where("(create_at BETWEEN '$date_from' AND '$date_to') ");
		}

		if (!empty($operator)) {
			//if user choose operator
			$this->db->where('operator', $operator);
		}

		if (!empty($evaluation)) {
			//if user choose evaluation
			$this->db->where('evaluationtype', $evaluation);
		}
		if (!empty($alerttype)) {
			//if user choose alert type
			$this->db->where('alert', $alerttype);
		}

		if (!empty($pic)) {
			//if user search based on  PIC name
			foreach ($pic as $p) {
				$this->db->like('PIC', strtolower($p));
			}
		}

		if (!empty($crossreff)) {
			//if user searching Crossreff number
			$this->db->where('crossreff', $crossreff);
		}
		if (!empty($uic)) {
			//if user searching uic number
			$this->db->where('UIC', $uic);
		}

		if (!empty($keyword)) {
			//if user searching keyword number
			$keyword = strtolower($keyword);
			$this->db->where("(lower(problemtitle) LIKE '%$keyword%' OR lower(problemanalisis) LIKE '%$keyword%' OR lower(detailproblem) LIKE '%$keyword%')");
		}

		if (!empty($actype)) {
			//if user searching A/C Type
			$this->db->where('actype', $actype);
		}

if (!empty($onwatch)) {
			//if user searching A/C Type
			$this->db->where('onwatch', $onwatch);
		}


		$this->db->where("(solution0 != '' OR solution1 != '' OR solution2 != '' OR solution3 != '' OR solution4 != '' OR solution5 != ''  OR solution6 != '' OR solution7 != '' OR solution8 != ''  OR solution9 != '' AND status != 'HIDDEN')", NULL, FALSE);
		$this->db->order_by('create_at', 'ASC');
		$query = $this->db->get("problem_definition");

		return $query->result_array();
	}



public function problem_display_evaluation(array $param = null)
	{
		$evaluation 	= $param['evaluation'];
		$operator 		= $param['operator'];
		$output_component = $param['output_component'];
		$role_id 	 	= $param['role_id'];
		$keyword 		= $param['keyword'];
		$uic 		    = $param['uic'];
		$actype 		= $param['actype'];
		$alerttype 		= $param['alerttype'];
		$pic	 		= $param['pic'];
		$crossreff 		= $param['crossreff'];
		$date_to 		= $param['date_to'];
		$date_from 		= $param['date_from'];
		$onwatch 		= $param['onwatch'];

		

	
		if (!empty($date_from) and !empty($date_to)) {
			$this->db->where("(create_at BETWEEN '$date_from' AND '$date_to') ");
		}


		if (!empty($operator)) {
			//if user choose operator
			$this->db->where('operator', $operator);
		}

		if (!empty($evaluation)) {
			//if user choose evaluation
			$this->db->where('evaluationtype', $evaluation);
		}
		if (!empty($alerttype)) {
			//if user choose alert type
			$this->db->where('alert', $alerttype);
		}

		if (!empty($pic)) {
			//if user search based on  PIC name
			foreach ($pic as $p) {
				$this->db->like('PIC', strtolower($p));
			}
		}

		if (!empty($crossreff)) {
			//if user searching Crossreff number
			$this->db->where('crossreff', $crossreff);
		}
		if (!empty($uic)) {
			//if user searching uic number
			$this->db->where('UIC', $uic);
		}

		if (!empty($keyword)) {
			//if user searching keyword number
			$keyword = strtolower($keyword);
			$this->db->where("(lower(problemtitle) LIKE '%$keyword%' OR lower(problemanalisis) LIKE '%$keyword%' OR lower(detailproblem) LIKE '%$keyword%')");
		}

		if (!empty($actype)) {
			//if user searching A/C Type
			$this->db->where('actype', $actype);
		}
		if (!empty($onwatch)) {
			//if user searching A/C Type
			$this->db->where('onwatch', $onwatch);
		}



		$this->db->where("(status != 'HIDDEN')");
		$query = $this->db->get("problem_definition");

		return $query->result_array();
	}




public function problem_search(array $param = null)
	{
		$evaluation 		= $param['evaluation'];
		$operator 			= $param['operator'];
		$number 			= $param['number'];
		$keyword 			= $param['keyword'];
		$ata 				= $param['ata'];
		$uic 		    	= $param['uic'];
		$actype 			= $param['actype'];
		$alerttype 			= $param['alerttype'];
		$pic	 			= $param['pic'];
		$crossreff 			= $param['crossreff'];
		$date_to 			= $param['date_to'];
		$date_from 			= $param['date_from'];
		$problem_title 		= $param['problem_title'];
		$status 			= $param['status'];
		$acreg 				= strtoupper($param['acreg']);

		if (!empty($date_from) and !empty($date_to)) {
			$this->db->where("(create_at BETWEEN '$date_from' AND '$date_to') ");
		}
		if (!empty($operator)) {
			//if user choose operator
			$this->db->where('operator', $operator);
		}
		if (!empty($number)) {
			//if user choose number
			$this->db->like('number', $number);
		}
		if (!empty($problem_title)) {
			//if user choose problem_title
			$this->db->where("(lower(problemtitle) LIKE '%$keyword%')");
		}
		if (!empty($evaluation)) {
			//if user choose evaluation
			$this->db->where('evaluationtype', $evaluation);
		}
		if (!empty($status)) {
			//if user choose evaluation
			$this->db->where('status', $status);
		}
		if (!empty($ata)) {
			$this->db->where('ata', $ata);
		}
		if (!empty($acreg)) {
			//if user choose evaluation
			$this->db->where('acreg', $acreg);
		}
		if (!empty($alerttype)) {
			//if user choose alert type
			$this->db->where('alert', $alerttype);
		}
		if (!empty($pic)) {
			//if user search based on  PIC name
			foreach ($pic as $p) {
				$this->db->like('PIC', strtolower($p));
			}
		}
		if (!empty($crossreff)) {
			//if user searching Crossreff number
			$this->db->like('crossreff', $crossreff);
		}
		if (!empty($uic)) {
			//if user searching uic number
			$this->db->where('UIC', $uic);
		}
		if (!empty($keyword)) {
			//if user searching keyword number
			$keyword = strtolower($keyword);
$this->db->where("(lower(problemtitle) LIKE '%$keyword%' OR lower(problemanalisis) LIKE '%$keyword%' OR lower(detailproblem) LIKE '%$keyword%' OR lower(remarks) LIKE '%$keyword%' OR lower(systems) LIKE '%$keyword%' OR lower(ata) LIKE '%$keyword%' OR lower(alert) LIKE '%$keyword%' OR lower(evaluationtype) LIKE '%$keyword%' OR lower(crossreff) LIKE '%$keyword%' OR lower(UIC) LIKE '%$keyword%' OR lower(srieffectivenees) LIKE '%$keyword%' OR lower(number) LIKE '%$keyword%' OR lower(operator) LIKE '%$keyword%' OR lower(actype) LIKE '%$keyword%' OR lower(acreg) LIKE '%$keyword%' OR lower(onwatch) LIKE '%$keyword%' ) ");
		}
		if (!empty($actype)) {
			//if user searching A/C Type
			$this->db->where('actype', $actype);
		}
		$this->db->where("(status != 'HIDDEN')");
		$query = $this->db->get("problem_definition");

		return $query->result_array();
	}


	// DB_TDAM
	public function getCrossreff()
	{
			return $this->db->query("SELECT EventID,ACTypeOrig,ACRegOrig,atachap,FltNo,Note,p_target_date FROM problem_definition_tdam ORDER BY EventID DESC ")->result_array();
	}	
	
	public function detailtdam($event)
	{
			return $this->db->get_where('problem_definition_tdam', ['EventID' => $event])->row_array();
	}	
	
	public function getEvent()
	{
		return $this->db3->query("SELECT p_id,p_eventid,EventID,FltNo,DateDeptEvent,ACTypeOrig,ACRegOrig,atachap,Note,p_target_date,Preanalis,p_description FROM tblevent JOIN tbl_preventive_action on tblevent.EventID = tbl_preventive_action.p_eventid JOIN tdamx on tblevent.EventID = tdamx.ID where p_uic LIKE '%TE%' ")->result_array();
	}
	public function getRetri($event)
	{
		return $this->db4->query("SELECT Rectification FROM mcdrnew WHERE EventID = '$event'")->row_array();
	}
	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$this->db->where('status', 'OPEN');
		$i = 0;
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                	$this->db->group_start(); 
                	$this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
            	 $this->db->group_end(); 
            }
            $i++;
        }
        if(isset($_POST['order'])) 
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function cektdam($p_id)
    {
    	return $this->db->get_where('problem_definition_tdam',['p_id' => $p_id])->num_rows();
    	
    }

    public function getlog()
    {
    	return $this->db->get('logreliability')->result_array();
    }



    public function getaction()
    {

    	return $this->db->get('action_log')->result_array();
    }

    public function getsolusi($tahun)
    {
    	$solusi = $this->db->query("SELECT * FROM solutions WHERE YEAR(last_update) = '$tahun'")->num_rows();
    	return $solusi;
    }
    public function getusers($tahun)
    {
    	$users = $this->db_users->query("SELECT * FROM logreliability WHERE YEAR(datetime) = '$tahun' AND apps = 'IDEE' ")->num_rows();
    	return $users;
    }
    public function eval($tahun)
    {

		$eval = $this->db->query("SELECT * FROM problem_definition where YEAR(create_at) = '$tahun' AND problemanalisis != '' ")->num_rows();
		return $eval;
    }
    public function problem($tahun)
    {

		$problem = $this->db->query("SELECT * FROM problem_definition WHERE status != 'HIDDEN' AND YEAR(create_at) = '$tahun' ")->num_rows();
		return $problem;
    }

     public function angkat($tahun)
    {
		$eval = $this->db->query("SELECT * FROM problem_definition where YEAR(create_at) = '$tahun' AND (solution0 != '' OR solution1 != '' OR solution2 != '' OR solution3 != '' OR solution4 != '' OR solution5 != ''  OR solution6 != '' OR solution7 != '' OR solution8 != ''  OR solution9 != '' AND status != 'HIDDEN')")->num_rows();
		$users = $this->db_users->query("SELECT * FROM logreliability WHERE YEAR(datetime) = '$tahun'")->num_rows();
		$problem = $this->db->query("SELECT * FROM problem_definition WHERE status != 'HIDDEN' AND YEAR(create_at) = '$tahun' ")->num_rows();
		$hasil = 
		[
			"problem"	 => $problem,
			"users"		 => $users,
			"eval"		 => $eval
		];
		return $hasil;

    }



}

/* End of file Getdat_model.php */
/* Location: ./application/models/Getdat_model.php */
?>