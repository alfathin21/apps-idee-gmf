<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	var $table = 'users_detail'; 
    var $column_order = array('id','nama','nopeg','email','unit','jabatan','role');
    var $column_search = array('id','nama','nopeg','email','unit','jabatan','role'); 
    var $order = array('nopeg' => 'DESC');

	public function __construct()
	{
		parent::__construct();
        $this->db_users = $this->load->database('db_users', TRUE);
	}	 
	private function _get_datatables_query()
	{
		$this->db_users->from($this->table);
        $this->db_users->join('user_role', 'users_detail.role_id = user_role.id', 'left');
		$i = 0;
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                	$this->db_users->group_start(); 
                	$this->db_users->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db_users->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
            	 $this->db_users->group_end(); 
            }
            $i++;
        }
        if(isset($_POST['order'])) 
        {
        	$this->db_users->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db_users->order_by(key($order), $order[key($order)]);
        }
    }
	public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db_users->limit($_POST['length'], $_POST['start']);
        $query = $this->db_users->get();
        return $query->result_array();
    }
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db_users->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db_users->from($this->table);
        return $this->db_users->count_all_results();
    }
      public function cek($username)
    {
        
    return $this->db_users->get_where('users',['username' => $username])->num_rows();

    }
    public function role_menu()
    {
        return $this->db_users->get('user_role')->result_array();
    }
  
        public function saveuser($data)
    {
        return $this->db_users->insert('users', $data);
    }

    public function saveuserdetail($data)
    {
        return $this->db_users->insert('users_detail', $data);
    }

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */