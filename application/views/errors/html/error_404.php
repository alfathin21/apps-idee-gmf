<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
	 <div class="middle-box text-center animated fadeInDown">
        <h2><?php echo $heading; ?></h2>
        <h3 class="font-bold"><?php echo $message; ?></h3>
    </div>
</body>
</html>