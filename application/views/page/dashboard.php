<script src="<?= base_url('assets/js/chart.js') ?>"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-md-12">
  <div class="container">
      <h2><?= $ucapan ?>, <?= $this->session->userdata('nama') ?></h2>
  </div>
</div>
</div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2">
      <select name="tahun" id="tahun" onchange="loop();" class="form-control">
        <option selected="">Select year :</option>
        <?php
        for ($x = $tahun; $x >= 2014; $x--) {
          ?>
          <option value="<?= $x ?>"><?= $x ?></option>
          <?php
        }
        ?>
      </select>
    </div>
  </div>
  <br>
<!--  -->
<style>
  .box{
    box-shadow: 1px 1px 4px rgba(0,0,0,0.4);
  }
</style>
<div class="row">
  <div class="col-md-4 col-sm-4">
    <div class="ibox float-e-margins box">
      <div class="ibox-title">
        <span id="atas1" class="label label-success pull-right"><?= $tahun ?></span>
        <h5><i class="fa fa-bookmark-o"></i>&nbsp;Total Problem</h5>
      </div>
      <div class="ibox-content">
        <h1 id="text_problem" class="no-margins"><?= $problem ?></h1>
        <div id="text_kecil1" class="stat-percent font-bold text-success"><?= $problem/100 ?>% <i class="fa fa-level-up"></i></div>
        <small>Problem</small>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="ibox float-e-margins box">
      <div class="ibox-title">
        <span id="atas3"  class="label label-primary pull-right"><?= $tahun ?></span>
        <h5><i class="fa fa-tasks"></i>&nbsp;Total Evaluation</h5>
      </div>
      <div class="ibox-content">
        <h1 id="text_evaluasi" class="no-margins"><?= $eval ?></h1>
        <div id="text_kecil3" class="stat-percent font-bold text-navy"><?= $eval/100 ?>% <i class="fa fa-level-up"></i></div>
        <small>Evaluation</small>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="ibox float-e-margins box">
      <div class="ibox-title">
         <span id="atas4"  class="label label-danger pull-right"><?= $tahun ?></span>
        <h5><i class="fa fa-users"></i>&nbsp;User Login</h5>
      </div>
      <div class="ibox-content">
        <h1 id="text_user" class="no-margins"><?= $user ?></h1>
        <div id="text_kecil4" class="stat-percent font-bold text-danger"><?= $user/100 ?>% <i class="fa fa-level-up"></i></div>
        <small>Users Login</small>
      </div>
    </div>
  </div>
</div>
  <!--  -->
<!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins box">
        <div class="ibox-title">
          <h5><i class="fa fa-chart"></i></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-lg-12">
           <div id="chart_tea"></div>
         </div>
       </div>
     </div>
   </div>
 </div>
  <div class="col-md-12">
      <div class="ibox float-e-margins box">
        <div class="ibox-title">
          <h5><i class="fa fa-chart"></i></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-lg-12">
           <div id="chart_ter"></div>
         </div>
       </div>
     </div>
   </div>
 </div>

  <div class="col-md-12">
      <div class="ibox float-e-margins box">
        <div class="ibox-title">
          <h5><i class="fa fa-chart"></i></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-lg-12">
           <div id="chart_ted"></div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- row -->
</div>
</div>
</div>
<script>
    $(document).ready(function() {
      // ajax buat ted
      var tahun = $('#tahun').val();
        $.ajax({
            url:"<?= base_url('Dashboard/chartted') ;?>",
            type:'post',
            data: {tahun : tahun},
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_ted', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TED EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TED-1',
                'TYPE MEETING TED-1',
                'ERC SRI TED-1',
                'OTHER TED-1',
                'TDAM TED-2',
                'TYPE MEETING TED-2',
                'ERC SRI TED-2',
                'OTHER TED-2',
                'TDAM TED-3',
                'TYPE MEETING TED-3',
                'ERC SRI TED-3',
                'OTHER TED-3',
                'TDAM TED-4',
                'TYPE MEETING TED-4',
                'ERC SRI TED-4',
                'OTHER TED-4'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: '<?= $tahun; ?>'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open]
              }, {
                name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress]
              }]
            });
             
           }
         });

      // ajax buat ted
      // ajax untuk TEA
  $.ajax({
            url:"<?= base_url('Dashboard/charttea') ;?>",
            type:'post',
            data: {tahun : tahun},
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_tea', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TEA EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TEA-1',
                'TYPE MEETING TEA-1',
                'ERC SRI TEA-1',
                'OTHER TEA-1',
                'TDAM TEA-2',
                'TYPE MEETING TEA-2',
                'ERC SRI TEA-2',
                'OTHER TEA-2',
                'TDAM TEA-3',
                'TYPE MEETING TEA-3',
                'ERC SRI TEA-3',
                'OTHER TEA-3',
                'TDAM TEA-4',
                'TYPE MEETING TEA-4',
                'ERC SRI TEA-4',
                'OTHER TEA-4'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: '<?= $tahun; ?>'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open]
              }, {
                name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress]
              }]
            });
             
           }
         });

      // akhir ajax untuk tea
      // ajax untuk ter
            $.ajax({
            url:"<?= base_url('Dashboard/chartter') ;?>",
            type:'post',
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_ter', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TER EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TER-1',
                'TYPE MEETING TER-1',
                'ERC SRI TER-1',
                'OTHER TER-1',
                'TDAM TER-2',
                'TYPE MEETING TER-2',
                'ERC SRI TER-2',
                'OTHER TER-2',
                'TDAM TER-3',
                'TYPE MEETING TER-3',
                'ERC SRI TER-3',
                'OTHER TER-3',
                'TDAM TER-4',
                'TYPE MEETING TER-4',
                'ERC SRI TER-4',
                'OTHER TER-4',
                'TDAM TER-5',
                'TYPE MEETING TER-5',
                'ERC SRI TER-5',
                'OTHER TER-5'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: '<?= $tahun; ?>'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others,res.ter5_tdam,res.ter5_type,res.ter5_erc,res.ter5_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open,res.ter5_tdam_open,res.ter5_type_open,res.ter5_erc_open,res.ter5_others_open]
              }, {
                 name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress,res.ter5_tdam_progress,res.ter5_type_progress,res.ter5_erc_progress,res.ter5_others_progress]
              }]
            });
             
           }
         });
        // ajax untuk TER
    });
</script>
<script>
  $(document).ready(function() {
    CKEDITOR.replace( 'editor' );
    $('#pc').select2();
    $('#cc').select2();
    $('#tahun').select2();
    setTimeout(function() {
      toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 3000
      };
      toastr.success('Welcome to IDEE');
    }, 1300);
  });
function loop()
{
 var tahun = $('#tahun').val();
 $.ajax({
      url: "<?= base_url('dashboard/tahun'); ?>",
      type: "POST",
      dataType: "JSON",
      data: {tahun: tahun},
      success: function (response) {
        var out_problem = response.problem/100
        var out_eval = response.eval/100;
        var out_users = response.users/100;
        $('#atas1').text('');
        $('#atas3').text('');
        $('#atas4').text('');
        $('#text_problem').text('');
        $('#text_evaluasi').text('');
        $('#text_user').text('');
        $('#text_kecil1').text('');
        $('#text_kecil2').text('');
        $('#text_kecil3').text('');
        $('#text_kecil4').text('');
        $('#atas1').prepend(tahun);
        $('#atas3').prepend(tahun);
        $('#atas4').prepend(tahun);
        $('#text_problem').prepend(response.problem);
        $('#text_evaluasi').prepend(response.eval);
        $('#text_user').prepend(response.users);
        $('#text_kecil1').prepend(out_problem+' %'+' <i class="fa fa-level-up"></i>');
        $('#text_kecil3').prepend(out_eval+' %'+' <i class="fa fa-level-up"></i>');
        $('#text_kecil4').prepend(out_users+' %'+' <i class="fa fa-level-up"></i>');
      }
    });

 $.ajax({
            url:"<?= base_url('Dashboard/charttea2') ;?>",
            type:'post',
            data: {tahun : tahun},
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_tea', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TEA EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TEA-1',
                'TYPE MEETING TEA-1',
                'ERC SRI TEA-1',
                'OTHER TEA-1',
                'TDAM TEA-2',
                'TYPE MEETING TEA-2',
                'ERC SRI TEA-2',
                'OTHER TEA-2',
                'TDAM TEA-3',
                'TYPE MEETING TEA-3',
                'ERC SRI TEA-3',
                'OTHER TEA-3',
                'TDAM TEA-4',
                'TYPE MEETING TEA-4',
                'ERC SRI TEA-4',
                'OTHER TEA-4'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: tahun
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open]
              }, {
                 name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress]
              }]
            });
             
           }
         });


    $.ajax({
            url:"<?= base_url('Dashboard/chartter2') ;?>",
            type:'post',
            data: {tahun : tahun},
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_ter', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TER EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TER-1',
                'TYPE MEETING TER-1',
                'ERC SRI TER-1',
                'OTHER TER-1',
                'TDAM TER-2',
                'TYPE MEETING TER-2',
                'ERC SRI TER-2',
                'OTHER TER-2',
                'TDAM TER-3',
                'TYPE MEETING TER-3',
                'ERC SRI TER-3',
                'OTHER TER-3',
                'TDAM TER-4',
                'TYPE MEETING TER-4',
                'ERC SRI TER-4',
                'OTHER TER-4',
                'TDAM TER-5',
                'TYPE MEETING TER-5',
                'ERC SRI TER-5',
                'OTHER TER-5'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: tahun
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others,res.ter5_tdam,res.ter5_type,res.ter5_erc,res.ter5_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open,res.ter5_tdam_open,res.ter5_type_open,res.ter5_erc_open,res.ter5_others_open]
              }, {
                 name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress,res.ter5_tdam_progress,res.ter5_type_progress,res.ter5_erc_progress,res.ter5_others_progress]
              }]
            });
             
           }
         });


$.ajax({
            url:"<?= base_url('Dashboard/chartted2') ;?>",
            type:'post',
            data: {tahun : tahun},
            success: function(data){
              res = JSON.parse(data);
               Highcharts.chart('chart_ted', {
              chart: {
                type: 'column'
              },
              title: {
                text: 'TED EVALUATION STATUS'
              },
              xAxis: {
                categories: [
                'TDAM TED-1',
                'TYPE MEETING TED-1',
                'ERC SRI TED-1',
                'OTHER TED-1',
                'TDAM TED-2',
                'TYPE MEETING TED-2',
                'ERC SRI TED-2',
                'OTHER TED-2',
                'TDAM TED-3',
                'TYPE MEETING TED-3',
                'ERC SRI TED-3',
                'OTHER TED-3',
                'TDAM TED-4',
                'TYPE MEETING TED-4',
                'ERC SRI TED-4',
                'OTHER TED-4'
                ],
                crosshair: true
              },
              yAxis: {
                min: 0,
                title: {
                  text: tahun
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> EVALUATION : {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'TOTAL',
                data: [res.ter1_tdam,res.ter1_type,res.ter1_erc,res.ter1_others,res.ter2_tdam,res.ter2_type,res.ter2_erc,res.ter2_others,res.ter3_tdam,res.ter3_type,res.ter3_erc,res.ter3_others,res.ter4_tdam,res.ter4_type,res.ter4_erc,res.ter4_others]
              }, {
                name: 'OPEN & PROGRESS',
                data: [res.ter1_tdam_open,res.ter1_type_open,res.ter1_erc_open,res.ter1_others_open,res.ter2_tdam_open,res.ter2_type_open,res.ter2_erc_open,res.ter2_others_open,res.ter3_tdam_open,res.ter3_type_open,res.ter3_erc_open,res.ter3_others_open,res.ter4_tdam_open,res.ter4_type_open,res.ter4_erc_open,res.ter4_others_open]
              }, {
                 name: 'CLOSED & REVIEW',
                data: [res.ter1_tdam_progress,res.ter1_type_progress,res.ter1_erc_progress,res.ter1_others_progress,res.ter2_tdam_progress,res.ter2_type_progress,res.ter2_erc_progress,res.ter2_others_progress,res.ter3_tdam_progress,res.ter3_type_progress,res.ter3_erc_progress,res.ter3_others_progress,res.ter4_tdam_progress,res.ter4_type_progress,res.ter4_erc_progress,res.ter4_others_progress]
              }]
            });
             
           }
         });
}
</script>