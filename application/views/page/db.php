<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-pencil-square"></i>&nbsp; PROBLEM EVALUATION / LIST PROBLEM</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
         <div class="table-responsive">
             <table class="table table-bordered table-striped table-hovered" id="table">
                <thead>
                  <tr>
                   <th class="text-center" width="4%">NO</th>
                   <th class="text-center">NUMBER</th>
                   <th class="text-center" width="16%">EVALUATION TYPE</th>
                   <th class="text-center" width="10%">A/C TYPE</th>
                   <th class="text-center">PROBLEM TITLE</th>
                   <th class="text-center">TARGET DATE</th>
                   <th class="text-center">PIC</th>
                   <th class="text-center">STATUS</th>
                   <th class="text-center" width="10%">ACTION</th>
                 </tr>
               </thead>
               <tbody>
                <?php 
                $no = 1;
                foreach ($problem as $key )  : ?>
                 <tr>
                   <td class="text-center"><?= $no; ?></td>
                   <td class="text-center"><?= $key['number']; ?></td>
                   <td class="text-center"><?= $key['evaluationtype']; ?></td>
                   <td class="text-center"><?= cekactype($key['actype']); ?></td>
                   <td class="text-center"><?= $key['problemtitle']; ?></td>
                   <td class="text-center"><?=  date('d F Y', strtotime($key['TargetDate']));?></td>
                   <td class="text-center">
                    <?php 
                    $new_s = rtrim($key['PIC'],',');
                    $s = explode(',',$new_s);
                    for ($i=0; $i < count($s) ; $i++) { 
                      $hasil = cek_email($s[$i]);
                      echo $hasil.',';    
                    }
                    ?>
                    </td>
                    <td class="text-center">
                      <?php if ($key['status'] == 'OPEN'): ?>
                        <span class="badge badge-success"><?= $key['status'] ?></span>
                        <?php elseif ($key['status'] == 'PROGRESS') : ?>
                          <span class="badge badge-warning"><?= $key['status'] ?></span>
                        <?php endif ?>
                        <td>
                         <a href="<?= base_url() ;?>follow?number=<?= $key['number']; ?>" class="btn btn-danger btn-sm">
                           <i class="fa fa-plus"></i> FOLLOW
                         </a>
                       </td>
                     </tr>
                     <?php
                     $no++; 
                   endforeach; ?>
                 </tbody>
               </table>
         </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>


<script>
     
    </script>