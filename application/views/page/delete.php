<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-6">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-folder"></i>&nbsp; PROBLEM DEFINITION DELETE</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-md-12">
            <table id="del" class="table table-striped table-responsive table-hovered">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>NUMBER PROBLEM</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div class="col-md-6">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-folder"></i>&nbsp; PROBLEM DEFINITION HISTORI DELETE</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
       <div class="row">
        <div class="col-md-12">
          <table id="table" class="table table-striped table-responsive table-hovered">
            <thead>
              <tr>
                <th>NO</th>
                <th>NUMBER PROBLEM</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1; 
              foreach ($histori as $key) :?>
                <tr>
                  <td><?= $no ?></td>
                  <td><?= $key['number'] ?></td>
                  <td>
                    <a data-id ="<?= $key['number'] ?>" data-toggle="modal" data-target ="#update_proble" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Update</a>
                  </td>
                </tr>
                <?php
                $no++;
              endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>



</div>
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-folder"></i>&nbsp; SOLUTIONS DELETE</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
       <div class="row">
        <div class="col-md-12 table-responsive">
          <table id="solutions_data" class="table table-striped table-hovered">
            <thead>
              <tr>
                <th>NO</th>
                <th>NUMBER SOLUTIONS</th>
                <th>SOLUTIONS</th>
                <th>STATUS</th>
                <th>LAST UPDATE</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
</div>
<!--  -->
<!-- akhir modal -->
<div class="modal fade" id="update_proble" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-users"></i> &nbsp; REGISTRASI USER</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">NUMBER PROBLEM : </label>
          <input type="text" class="form-control" id="numberproblem" autocomplete="off" >
        </div> 
        <div class="form-group">
          <label for="">STATUS : </label>
          <select name="status" id="status" class="form-control">
            <option value="DELETED">DELETED</option>
            <option value="OPEN">OPEN</option>
            <option value="PROGRESS">PROGRESS</option>
            <option value="REVIEW">REVIEW</option>
            <option value="CLOSE">CLOSE</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" id="save_user"><i class="fa fa-arrow-right"></i> UPDATE NUMBER PROBLEM</button>
      </div>
    </form>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!--  -->
<script>

  var table;
  var table_solusi;
  $(document).ready(function() {
    table = $('#del').DataTable({ 
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
        "url": "<?= ('delete/get_data_problem')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ 0 ], 
        "orderable": false, 
      },
      ],
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
    });
    table_solusi = $('#solutions_data').DataTable({ 
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
        "url": "<?= ('delete/get_data_solution')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ 0 ], 
        "orderable": false, 
      },
      ],
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
    });
  });


  $(function(){
    $("#update_proble").on("show.bs.modal", function (event) {
      var button = $(event.relatedTarget);
      var id = button.data("id");
      var modal = $(this);
      modal.find("#numberproblem").val(id);
    });
  });

  $('#delete').on('click', function(){
   confirm("Delete problem ?");
 })
  $('#save_user').on('click', function(){
    var number =  $('#numberproblem').val();
    var status =  $('#status').val();
    $.ajax({
      url: "<?= site_url('delete/dl_histori'); ?>",
      type: "POST",
      data: {status: status,number:number},
      success: function (data) {
        document.location.href = '<?= base_url('delete')?>'; 
      },
    });
  })
</script>





<!--  -->
<!-- /page content -->





