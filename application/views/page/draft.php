<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-archive"></i> &nbsp; PROBLEM DEFINITION / UPDATE DRAFT</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <form action="<?= base_url('pd/savedraft') ?>" method="post" name="pd" id="pd">
            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">NUMBER  : </label>
                <input value="<?= $re['number'] ?>" required="" type="text" readonly="" class="form-control has-feedback-left" id="number" name="number" >
                <input type="hidden" name="file_gabung" value="<?= $re['file'] ?>">
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">CROSS REF :</label>
                <input value="<?= $re['crossreff'] ?>" placeholder="CROSS REF" autocomplete="off" type="text" class="form-control has-feedback-left" id="crossreff" name="crossreff" >
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">EVALUATION TYPE * :</label>
                <select required class="form-control select4" name="evaluation" id="evaluation">
                  <option value="<?= $re['evaluationtype'] ?>"><?= $re['evaluationtype'] ?></option>
                  <option value="TDAM">TDAM</option>
                  <option value="TYPE MEETING">TYPE MEETING</option>
                  <option value="ERC - SRI">ERC - SRI</option>
                  <option value="PERFORMANCE">PERFORMANCE</option>
                  <option value="ETOPS">ETOPS</option>
                  <option value="OTHERS">OTHERS</option>
                  <option value="MRC">MRC</option>
                  <option value="DE-DT">DE-DT</option>
                  <option value="Safety Meeting">Safety Meeting</option>
                </select>
              </div>
            </div>
            <br>
            <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">OPERATOR * : </label>
              <select required="" class="form-control Operator" id="operator" name="operator" >
                <option selected readonly value="<?= $re['operator'] ?>"><?= cekoperator($re['operator']) ?></option>
                <option disabled value="GA">GARUDA INDONESIA</option>
                <option disabled value="QG">CITILINK</option>
                <option disabled value="SJ">SRIWIJAYA</option>
                <option disabled value="IN">NAM AIR</option>
              </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">A/C Type *: </label> 
              <select required="" id="actype" class="form-control selek2" name="actype"  >
                <option selected readonly value="<?= $re['actype'] ?>"><?= cekactype($re['actype']) ?></option>
                <option disabled value="ATR">ATR72-600</option>
                <option disabled value="330">A330</option>
                <option disabled value="320">A320-200</option>
                <option disabled value="733">B737-300</option>
                <option disabled value="735">B737-500</option>
                <option disabled value="738">B737-800</option>
                <option disabled value="739">B737-900</option>
                <option disabled value="73M">B737-MAX</option>
                <option disabled value="777">B777-300</option>
                <option disabled value="CRJ">CRJ1000</option>
              </select>    
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">ALERT TYPE SRI : </label>
              <select class="form-control" name="alert" id="alert" >
                <option value="<?= $re['alert'] ?>"><?= $re['alert'] ?></option>
                <option value="ALD">ALD</option>
                <option value="ALP">ALP</option>
                <option value="ALM">ALM</option>
                <option value="ALC">ALC</option>
                <option value="SDR">SDR</option>
                <option value="ACCDNT">ACCDNT</option>
                <option value="ENV">ENV</option>
                <option value="FTG">FTG</option>
                <option value="EXT REP">EXT REP</option>
                <option value="OPS">OPS</option>
              </select>
            </div>
          </div>
          <br>

          <div class="row">
           <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
            <label for="">A/C REG : </label>
            <input maxlength="6"  value="<?= $re['acreg'] ?>" autocomplete="off" type="text" class="form-control has-feedback-left" id="acreg" name="acreg" placeholder="A/C REG">
          </div>
          <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
           <label for="">ATA * : </label>
           <input required="" value="<?= $re['ata'] ?>" maxlength="2" autocomplete="off" type="number" class="form-control has-feedback-left"  id="ata" name="ata" placeholder="Code ATA">
         </div>
         <div class="col-md-8 col-sm-6 col-xs-12 form-group has-feedback">
           <label for="">SYSTEMS : </label>
           <input value="<?= $re['systems'] ?>" autocomplete="off" type="text" class="form-control has-feedback-left" id="system" name="system" >
         </div>
       </div>
       <br>
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
         <label for="">PROBLEM TITLE * : </label>
         <input value="<?= $re['problemtitle'] ?>" required="" autocomplete="off" type="text" class="form-control has-feedback-left" name="problem_title" id="problem_title" >
       </div>
     </div>
     <br>
     <div class="row" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <label for="">DETAIL OF PROBLEM :</label>
        <textarea  name="editor" id="editor" class="editor"><?= $re['detailproblem'] ?></textarea>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">PIC ASSIGN * :</label>
          <br>
          <div class="form-group">
            <select required="" name="pc[]" id="pc" class="form-control" multiple="">
              <?php
              $new_s = rtrim($re['PIC'],',');
              $e  = explode(',',$new_s);
              ?>
              <?php foreach ($e as $key )  :?>
                <option value="<?= $key ?>" <?= 'selected' ?>> <?= $key?> </option>
              <?php endforeach; ?> 
              <?php foreach ($te as $key) : ?>
                <option value="<?= $key['EMAIL'] ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
              <?php endforeach; ?> 
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="">CC :</label>
          <br>
          <div class="form-group">
            <select  required="" name="cc[]" id="cc" class="form-control" multiple="">
              <?php
              $new_s = rtrim($re['cc'],',');
              $e  = explode(',',$new_s);
              ?>
              <?php foreach ($e as $key )  :?>
                <option value="<?= $key ?>" <?= 'selected' ?>> <?= $key?> </option>
              <?php endforeach; ?> 
              <?php foreach ($te as $key) : ?>
                <option value="<?= $key['EMAIL'] ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
              <?php endforeach; ?> 
            </select>
          </div>
        </div>
      </div>
    </div>  
    <br>
    <div class="row" >
      <div class="col-md-7 col-sm-6 col-xs-12 form-group has-feedback">
       <label for="">RELATED EVALUATION :</label>
       <input value="<?= $re['releated'] ?>" type="text" placeholder="RELATED EVALUATION" autocomplete="off" class="form-control" name="releated" id="releated" >
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
      <label for="">TARGET DATE * : </label>
      <input required="" value="<?= $re['TargetDate'] ?>" type="date" class="form-control has-feedback-left"  id="target_date" name="target_date" >

    </div>
    <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
      <label for="">UIC *: </label>
      <select required="" class="form-control Operator" id="uic"  name="uic">
        <option value="<?= $re['UIC'] ?>"><?= $re['UIC'] ?></option>
        <option value="TEA-1">TEA-1</option>
        <option value="TEA-2">TEA-2</option>
        <option value="TEA-3">TEA-3</option>
        <option value="TEA-4">TEA-4</option>
        <option value="TER-1">TER-1</option>
        <option value="TER-2">TER-2</option>
        <option value="TER-3">TER-3</option>
        <option value="TER-4">TER-4</option>
        <option value="TER-5">TER-5</option>
        <option value="TED-1">TED-1</option>
        <option value="TED-2">TED-2</option>
        <option value="TED-3">TED-3</option>
        <option value="TED-4">TED-4</option>
        <option value="TED-5">TED-5</option>
      </select>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for=""> ATTACHMENTS :</label>
        <br>
        <button data-toggle="modal" type="button" data-target="#upload" style="background: #337AB7; color: white" class="form-control" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; <b>UPLOAD</b></button>
      </div>
    </div>
    <div class="col-md-6">
      <label for=""> ATTACHMENTS FILE  :</label>
      <table class="table table-striped" id="skuy">
        <?php if (!empty($re['file'] )) : ?>
          <?php 
          $new_s = rtrim($re['file'],',');
          $exp = explode(',',$new_s);
          ?>
          <?php  for($i = 0; $i < count($exp); $i++){
            $row= $this->Getdat_model->getfile($exp[$i]); 
            ?>
            <tbody>
              <?php if ($row != NULL or !empty($row)): ?>
               <?php if ($row['nama_baru'] == null) {  
                $link = $row['nama_file']; 
              } else {
                $link = $row['nama_baru'];
              } ?>
              <tr id="<?= $row['nama_file']; ?>">
                <td><button class="badge badge-success" type="button" id="file_open" data-toggle="modal" data-target="#modal2" data-target="#modal" data-draft="<?= $link; ?>"><?= $row['nama_file']; ?></button></td>
                <td>
                  <a id="opopop" type="button" data-unik1="<?= $re['number'] ?>" data-delete="<?= $row['nama_file'] ?>" data-unik2="<?= $row['token'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
            <?php endif ?>
          </tbody>
        <?php } ?>     
      <?php endif ?>
    </table>
  </div>
</div>
<br>
<div class="row" >
  <div class="col-md-5"></div>
  <div class="col-md-6">
    <button style="margin-right: 15px;" type="submit" name="update_draft" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button>
    <button type="submit" name="assign" class="btn btn-success" ><i class="fa fa-send-o"></i>&nbsp;&nbsp; ASSIGN </button>
  </div>
</div>
</form>


</div>
</div>
</div>
</div>
</div>
</div>

<script>
  CKEDITOR.replace( 'editor' );

  $(document).on("click", "#opopop", function () {
    var number = $(this).data('unik1');
    var file = $(this).data('unik2');
    var ui = $(this).data('delete');
    $.ajax({
     type:"POST",
     data:{number:number,file:file,ui:ui},
     url:"<?= base_url('Draft/deleteattach') ?>",
     dataType: 'json',
     success: function(data){
      var sao = document.getElementById(data);
      $(sao).remove();
    }
  });
  });
</script>