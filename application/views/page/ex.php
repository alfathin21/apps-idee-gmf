<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-filter"></i>&nbsp; PROBLEM DEFINITION / UPDATE ISSUE TDAM</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <form action="<?= base_url('tdm/save_tdam') ?>" method="POST">
            <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">NUMBER <span style="color: red">*</span> : </label>
              <input required="" type="text" readonly="" class="form-control has-feedback-left" id="number" name="number" >
              <input type="hidden" name="urutan" id="urutan" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">CROSS REF :</label>
              <select required="" name="crossreff" id="crossreff" class="form-control cross" >
                <option value=""></option>
                <?php foreach ($crossreff as $key) : ?>
                  <option value="<?= $key['p_eventid'] ?>"><?= $key['p_eventid'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">OPERATOR <span style="color: red">*</span> : </label>
              <select required="" class="form-control Operator" id="operator" name="operator" >
                <option value=""></option>
                <option value="GA">GARUDA INDONESIA</option>
                <option value="QG">CITILINK</option>
                <option value="SJ">SRIWIJAYA</option>
                <option value="IN">NAM AIR</option>
              </select>
            </div>

          </div>
          <br>
          <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">EVALUATION TYPE <span style="color: red">*</span> :</label>
              <input type="text" name="evaluation" class="form-control" value="TDAM" readonly="">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">A/C Type <span style="color: red">*</span> : </label> 
              <input type="text" id="acTYpe" class="form-control selek2" name="acTYpe" readonly=""> 
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">ALERT TYPE SRI : </label>
              <select class="form-control" name="alert" id="alert" >
                <option value=""></option>
                <option value="ALD">ALD</option>
                <option value="ALP">ALP</option>
                <option value="ALM">ALM</option>
                <option value="ALC">ALC</option>
                <option value="SDR">SDR</option>
                <option value="ACCDNT">ACCDNT</option>
                <option value="ENV">ENV</option>
                <option value="FTG">FTG</option>
                <option value="EXT REP">EXT REP</option>
                <option value="OPS">OPS</option>
              </select>
            </div>
          </div>
          <br>
          <div class="row"> 
            <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">A/C REG : </label>
              <input maxlength="6" readonly autocomplete="off" type="text" class="form-control has-feedback-left" id="acreg" name="acreg" placeholder="A/C REG">
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
             <label for="">ATA <span style="color: red">*</span> : </label>
             <input required="" maxlength="2" autocomplete="off"readonly type="number" class="form-control has-feedback-left"  id="atachap" name="atachap" placeholder="Code ATA">
           </div>
           <div class="col-md-8 col-sm-6 col-xs-12 form-group has-feedback">
             <label for="">SYSTEMS : </label>
             <input  autocomplete="off"  type="text" class="form-control has-feedback-left" id="system" name="system" placeholder="Systems">
           </div>
         </div>
         <br>
         <div class="row" >  
           <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
             <label for="">PROBLEM TITLE <span style="color: red">*</span> : </label>
             <input required="" autocomplete="off" type="text" class="form-control has-feedback-left" name="problem_title" id="problem_title" placeholder="Problem Title" >
           </div>
         </div>
         <br>
         <div class="row">
           <div class="col-md-6 form-group">
            <label for="">TDAM PROPOSE ACTION :</label>
            <textarea name="propose_action" id="propose_action" ></textarea>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="col-md-6 form-group">
          <label for="">TDAM DISCUSSION :</label>
          <textarea name="tdam_discussion" id="tdam_discussion" ></textarea>
        </div>   
      </div>
      <br>
      <div class="row">
        <div class="col-md-12 form-group">
          <label for="">RECTIFICATION :</label>
          <textarea name="rectification" id="rectification"></textarea>
        </div> 
      </div>
      <br>
      <div class="row" >
        <div class="col-md-12 col-sm-12 col-xs-12">
          <label for="">DETAIL OF PROBLEM :</label>
          <textarea  name="editor2" id="editor2" class="editor2"></textarea>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">PIC ASSIGN <span style="color: red">*</span> :</label>
            <br>
            <div class="form-group">
              <select required="" name="pc[]" id="tdam_pc" class="form-control" multiple="">
                <option value=""></option>
                <?php foreach ($te as $key) : ?>
                  <option value="<?= strtolower($key['EMAIL']) ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">CC :</label>
            <br>
            <div class="form-group">
              <select name="cc[]" id="tdam_cc" class="form-control" multiple="">
                <option value=""></option>
                <?php foreach ($te as $key) : ?>
                  <option value="<?= $key['EMAIL'] ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
      </div>  
      <br>
      <div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
         <label for="">RELATED EVALUATION :</label>
         <input type="text" placeholder="RELATED EVALUATION" autocomplete="off" class="form-control" name="releated" id="releated" >
       </div>
       <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
        <label for="">TARGET DATE <span style="color: red">*</span> : </label>
        <input required="" type="date" class="form-control has-feedback-left" readonly id="target_date" name="target_date" >
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
        <label for="">REVISI TARGET :</label>                
        <input type="date" class="form-control has-feedback-left"  id="revisi_target" name="revisi_target" placeholder="Revise Targe">
      </div>
      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
        <label for="">UIC <span style="color: red">*</span> : </label>
        <select required="" class="form-control Operator" id="uic"  name="uic">
          <option></option>
          <option value="TEA-1">TEA-1</option>
          <option value="TEA-2">TEA-2</option>
          <option value="TEA-3">TEA-3</option>
          <option value="TEA-4">TEA-4</option>
          <option value="TER-1">TER-1</option>
          <option value="TER-2">TER-2</option>
          <option value="TER-3">TER-3</option>
          <option value="TER-4">TER-4</option>
          <option value="TER-5">TER-5</option>
          <option value="TED-1">TED-1</option>
          <option value="TED-2">TED-2</option>
          <option value="TED-3">TED-3</option>
          <option value="TED-4">TED-4</option>
          <option value="TED-5">TED-5</option>
        </select>
      </div>
    </div>
    <br>
     <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for=""> ATTACHMENTS :</label>
            <br>
            <button data-toggle="modal" type="button" data-target="#upload" style="background: #337AB7; color: white" class="form-control" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; <b>UPLOAD</b></button>
          </div>
        </div>
        <div class="col-md-6">
          <label for=""> ATTACHMENTS FILE  :</label>
          <table class="table table-striped" id="skuy">
            <span id="ee"></span>
          </table>
        </div>
      </div>

      <hr>
    <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-4">
        <button style="margin-right: 15px;" type="submit" name="save_tdam" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button>
        <button type="submit" name="assign_tdam" class="btn btn-success" ><i class="fa fa-send-o"></i>&nbsp;&nbsp; ASSIGN </button>
      </div>
    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
  CKEDITOR.replace( 'editor2' );
  CKEDITOR.replace( 'propose_action' );
  CKEDITOR.replace( 'tdam_discussion' );
  CKEDITOR.replace( 'rectification' );
</script>
