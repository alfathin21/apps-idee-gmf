<?php $unit = $this->session->userdata('unit'); ?>
<form class=" form-horizontal form-label-left input_mask" method="post" action="<?= base_url('pe/save') ?>" >
  <div class="wrapper wrapper-content">
    <div class="row">
      <div class="col-md-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5><i class="fa fa-edit"></i>&nbsp; PROBLEM DEFINITION DETAIL</h5>
            <div class="ibox-tools">
              <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="ibox-content inspinia-timeline">

            <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">NUMBER  : </label>
              <input  value="<?= $detail['number'] ?>" readonly="" type="text" required class="form-control" id="number" name="number" >
              <input type="hidden" name="file_gabung" value="<?= $detail['file'] ?>">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">CROSS REF :</label>
              <input readonly="" value="<?= $detail['crossreff'] ?>" type="text" class="form-control" id="crossreff" name="crossreff" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">EVALUATION TYPE  :</label>
              <input readonly="" class="form-control" type="text" readonly="" value="<?= $detail['evaluationtype'] ?>">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">OPERATOR  : </label>
              <input type="text" class="form-control" readonly=""  name="operator2"  value="<?= cekoperator($detail['operator']) ?>" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">A/C Type : </label> 
              <input type="text" class="form-control" readonly="" name="actype2" id="actype2" value="<?= cekactype($detail['actype']) ?>">  
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">ALERT TYPE SRI : </label>
              <input readonly="" class="form-control" type="text" readonly="" value="<?= $detail['alert'] ?>">
            </div>
          </div>
          <br>
          <div class="row">
           <div class="col-md-2 col-sm-6 col-xs-12">
            <label for="">A/C REG : </label>
            <input readonly="" value="<?= $detail['acreg'] ?>"  type="text" class="form-control has-feedback-left" id="acreg" name="acreg" >
          </div>
          <div class="col-md-2 col-sm-6 col-xs-12">
           <label for="">ATA  : </label>
           <input readonly="" value="<?= $detail['ata'] ?>" type="number" class="form-control has-feedback-left"  id="ata" name="ata">
         </div>
         <div class="col-md-8 col-sm-6 col-xs-12">
           <label for="">SYSTEMS : </label>
           <input readonly="" value="<?= $detail['systems'] ?>" type="text" class="form-control has-feedback-left" id="system" name="system">
         </div>
       </div>
       <br>
       <div class="row" >  
         <div class="col-md-12 col-sm-12 col-xs-12">
           <label for="">PROBLEM TITLE  : </label>
           <input readonly="" value="<?= $detail['problemtitle'] ?>" autocomplete="off" type="text" class="form-control" name="problem_title" id="problem_title" placeholder="Problem Title" >
         </div>
       </div>
       <br>
       <div class="row" >
        <div class="col-md-12 col-sm-12 col-xs-12">
          <label for="">DETAIL OF PROBLEM :</label>
          <textarea readonly="" name="tor" id="tor" class="editor"><?= $detail['detailproblem'] ?></textarea>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">PIC ASSIGN :</label>
          <select disabled="" required="" name="pc[]" id="pc" class="form-control" multiple="">
            <?php
            $new_s = rtrim($detail['PIC'],',');
            $e  = explode(',', $new_s);
            ?>
            <?php for ($i=0; $i < count($e) ; $i++) {  ?>
              <option  value="<?= $e[$i] ?>" <?= 'selected' ?>> <?= $e[$i] ?> </option>
            <?php }; ?>   
          </select>
        </div>
        <div class="col-md-6">
          <label for="">CC :</label>
          <select disabled="" required="" name="cc[]" id="cc" class="form-control" multiple="">
            <?php
            $new_s = rtrim($detail['cc'],',');
            $e  = explode(',',$new_s);
            ?>
            <?php foreach ($e as $key )  :?>
              <option value="<?= $key ?>" <?= 'selected' ?>> <?= $key?> </option>
            <?php endforeach; ?>   
          </select>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-12 ">
         <label for="">RELATED EVALUATION :</label>
         <input type="text" value="<?= $detail['releated'] ?>" readonly="" class="form-control" name="releated" id="releated" >
       </div>

       <div class="col-md-3 col-sm-6 col-xs-12 ">
        <label for="">TARGET DATE * : </label>
        <input  type="date"readonly value="<?= strftime('%Y-%m-%d', strtotime($detail['TargetDate'])); ?>" class="form-control -left"  id="target_date" name="target_date" >

      </div>

      <div class="col-md-2 col-sm-6 col-xs-12 ">
        <label for="">UIC *: </label>
        <input type="text" class="form-control" value="<?= $detail['UIC']?>" readonly>
      </div>

    </div>
    <div class="row">
     <br>
     <div class="col-md-12"> 
      <label for="">COMMENTS REVIEW :</label>
      <input type="text" class="form-control" readonly="" value="<?= $detail['comments'] ?>">

    </div>
  </div>

</div>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-check-square-o"></i>&nbsp; PROBLEM EVALUATION / ADD SOLUTIONS</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
        <div class="row" >
          <div class="col-md-12 col-sm-12 col-xs-12">
            <label for="">PROBLEM ANALYSIS :</label>
            <textarea required="" name="analysis" id="analysis" class="analysis"><?= $detail['problemanalisis'] ?></textarea>
            <input name="number" id="number" type="hidden" value="<?= $detail['number'] ?>">
            <input  name="actype3" id="actype3" type="hidden" value="<?= $detail['actype'] ?>">
            <input  name="ata" id="ata" type="hidden" value="<?= $detail['ata'] ?>">
          </div>
        </div>
        <br>
        <div class="row">
          <div  class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
            <button  data-toggle="modal" type="button" data-target="#upload" class="btn btn-sm btn-success"><i class="fa fa-download"></i>&nbsp;&nbsp;UPLOAD FILES  </button>
          </div>
          <div class="col-lg-10 col-md-9 col-sm-2 col-xs-12">
            <table class="table table-striped">
              <p id="c">ATTACHMENTS FILE : </p>
            </table>
            <table class="table table-striped" id="skuy">
              <?php if (!empty($detail['file'] )) : ?>
                <?php 
                $new_string = rtrim($detail['file'],',');
                $exp = explode(',',$new_string);
                ?>
                <?php  for($i = 0; $i < count($exp); $i++){
                  $row= $this->Getdat_model->getfile($exp[$i]); 
                  ?>
                  <tbody>
                   <?php if ($row != NULL or !empty($row)): ?>
                         <?php if ($row['nama_baru'] == null) {  
                    $link = $row['nama_file']; 
                  } else {
                    $link = $row['nama_baru'];
                  } ?>
                    <tr id="<?= $row['nama_file']; ?>">
                      <td>
                       <button class="badge badge-primary" type="button" id="open3" data-toggle="modal" data-target="#modal3" data-pre3="<?= $link; ?>"><?= $row['nama_file']; ?></button>
                     </td>
                     <td>
                      <a id="opopop" type="button" data-unik1="<?= $detail['number'] ?>" data-delete="<?= $row['nama_file'] ?>" data-unik2="<?= $row['token'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endif ?>
              </tbody>
            <?php } ?>     
          <?php endif ?>
        </table>
        <div id="tambah">
          <input id="idf" value="0" type="hidden" name="idf" >  
        </div>
        <br>
      </div>
    </div>
    <br>
    <div class="row">
     <div class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
      <button type="button" onclick="tambah(); return false;" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> &nbsp; &nbsp; Add Solutions</button>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-3">
      <button data-toggle="modal" data-target="#exits" name="save" type="button" class="btn btn-sm btn-danger"><i class="fa fa-sign-out"></i> &nbsp;  Add Existing Solutions</button>
    </div>
  </div>
  <br>
  <div id="sasa">
    <table></table>
  </div>
  <div id="xcn"></div>
  <div>
   <?php for ($i=0; $i < 10 ; $i++) {  ?>
    <?php if (empty($detail['solution'.$i])) : ?>
      <?php else  :?>
        <?php 
        $solusiKeberapa = "solution".$i;
        $row = $this->Getdat_model->join($solusiKeberapa,$detail['solution'.$i]);
        $data[] = $detail['solution'.$i];
        ?>
        <div class='row' id="srow<?= $i ;?>">
          <input id="numbersolusi<?= $i ;?>" name='p[]' type="hidden" value="<?= $row['numbersolution'] ?>">
          <div class='col-md-12 col-sm-6 col-xs-12 '>
            <label>SOLUTION :</label>
            <input readonly type='text' id="solutionn<?= $i;?>" name="solution[]" class='form-control ' value="<?= $row['solution'] ?>">
            <br>
          </div>
          <div class='col-md-6 col-sm-6 col-xs-12 '>
            <label>UIC : </label>
            <input readonly id="uicc<?= $i?>" type='text' class='form-control ' value="<?= $row['uic'] ?>">
          </div>
          <div class='col-md-3 col-sm-6 col-xs-12 '>
            <label>TARGET DATE : </label>
            <input readonly id="targett_date<?= $i?>" type='date' class='form-control ' value="<?= $row['targetdate'] ?>" >
          </div>
          <div class='col-md-3 col-sm-6 col-xs-12 '>
            <label>REVISI TARGET :</label>
            <input readonly type='date' id="revisii_target<?= $i ?>" class='form-control ' value="<?= $row['revisidate'] ?>">
            <br>
          </div>
          <div class='col-md-6 col-sm-6 col-xs-12'>
           <a onclick='hapusElemen3("#sroww<?= $i?>",<?= $i?>,"<?= $solusiKeberapa  ?>"); return false;' class='btn btn-danger btn-sm' ><i class='fa fa-close'></i>&nbsp; Delete Solution</a>
         </div>
       </div>
       <br>
     <?php endif; ?>
   <?php } ?>
   <?php
   if (isset($data)) {
    $cek = count($data);
    echo "<script>$('#idf').val(".$cek.");</script>";
  }
  ?>
</div>
<div id="divHobi"><hr></div><br>
<br>
<div class="row" >
  <div class="col-md-4"></div>
  <div class="col-md-6" >
    <button style="margin-right:15px;" name="progress" type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button>
    <button name="review" type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp; SUBMIT EVALUATION</button>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
<script>
  CKEDITOR.replace( 'analysis' );
  CKEDITOR.replace( 'tor' );
  CKEDITOR.config.removePlugins = 'elementspath';
</script>
<script>
  function tambah() {
    var idf = document.getElementById("idf").value;
    if (idf < 10) {
      var stre;
      stre="<input type='hidden' class='form-control ' id='ssa"+idf+"' name='p[]' ><div class='row' id='srow" + idf + "'><div class='col-md-12 col-sm-6 col-xs-12'><label>SOLUTION :</label><input type='text'  class='form-control ' id='solution"+idf+"' name='solution[]' ><br></div><div class='col-md-6 col-sm-6 col-xs-12'><label>UIC : </label><input type='text' class='form-control ' id='uic"+idf+"' name='uic"+idf+"'></div><div class='col-md-3 col-sm-6 col-xs-12'><label>TARGET DATE : </label><input id='target_date"+idf+"' type='date' class='form-control ' name='target_date"+idf+"'></div><div class='col-md-3 col-sm-6 col-xs-12 '><label>REVISI TARGET :</label><input type='date' class='form-control ' id='revisi_target"+idf+"' name='revisi_target"+idf+"'><br></div></br><div class='col-md-6 col-sm-6 col-xs-12'><a style='margin-right: 15px;' id='save_solusi"+idf+"' class='btn btn-primary btn-sm' onclick='cek("+idf+")'><i id='iconsave"+idf+"' class='fa fa-save'></i>&nbsp; Save Solution</a><a  class='btn btn-danger btn-sm' onclick='hapusElemen2(\"#srow" + idf + "\","+idf+"); return false;'><i class='fa fa-close'></i>&nbsp; Delete Solution</a><hr></div>";
      $("#divHobi").append(stre);
      idf = (idf-1) + 2;
      document.getElementById("idf").value = idf;
    } else {
      return false;
    }
  }
  function tambah2(solusinumber,solusi,uic,targetdate,revisidate) {
   var idf = document.getElementById("idf").value;
   if (idf < 10) {
     var stre;
     stre="<input type='hidden' class='form-control' id='numbersolusi"+idf+"' value='"+solusinumber+"'  name='p[]' ><div id='srow" + idf + "' class='row'><div class='col-md-12 col-sm-6 col-xs-12'><label>SOLUTION :</label><input type='text'  class='form-control ' readonly='' id='solution"+idf+"' value='"+solusi+"'  name='solution[]' ></br></div><div class='col-md-6 col-sm-6 col-xs-12'><label>UIC : </label><input type='text' class='form-control ' readonly='' value='"+uic+"' name='uic"+idf+"'></div><div class='col-md-3 col-sm-6 col-xs-12'><label>TARGET DATE : </label><input type='text' class='form-control ' readonly='' name='target_date"+idf+"' value='"+targetdate+"'></div><div class='col-md-3 col-sm-6 col-xs-12'><label>REVISI TARGET :</label><input type='text' class='form-control ' readonly='' name='revisi_target"+idf+"' value='"+revisidate+"' ></br></div></br><div class='col-md-6 col-sm-6 col-xs-12 '><a  class='btn btn-danger btn-sm' onclick='hapusElemen(\"#srow" + idf + "\"); return false;'><i class='fa fa-close'></i>&nbsp; Delete Solution</a><hr></div></div>";
     $("#divHobi").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   } else {
    return false;
  }
}
function hapusElemen2(idf,idf2){
  var ambil = $('#idf').val();
  var number = '#numbersolusi'+idf2;
  var js = $(number).val();
  var hasil = ambil - 1;
  document.getElementById("idf").value = hasil;
  $.ajax({
    url: "<?= base_url('pe/numbersolutiondelete'); ?>",
    type: "POST",
    dataType: "JSON",
    data: {js: js},
    success: function (response) {
     $(idf).remove();
   },
   error: function (jqXHR, textStatus, errorThrown) {

   }
 });
}
function hapusElemen3(idf,idf2,solu){
  var ambil = $('#idf').val();
  var numberproblem = $('#number').val();
  var number = '#numbersolusi'+idf2;
  var js = $(number).val();
  var hasil = ambil-1;
  var a = '#solusike'+idf2;
  var c = '#srow'+idf2;
  var solusi = solu;
  $.ajax({
    url: "<?= base_url('pe/problemdelete'); ?>",
    type: "POST",
    dataType: "JSON",
    data: {solusi: solusi,numberproblem:numberproblem},
    success: function (response) {
      $(c).remove();
      document.getElementById("idf").value = hasil;
    },
    error: function (jqXHR, textStatus, errorThrown) {

    }
  });
}
function hapusElemen(idf) {
  var ambil = $('#idf').val();
  var hasil = ambil - 1;
  document.getElementById("idf").value = hasil;
  $(idf).remove();
}
function cek(idf){
  var actype = $('#actype3').val();
  var ata = $('#ata').val();
  var solution_name = $('#solution'+idf).val();
  var uic = $('#uic'+idf).val();
  var target_date = $('#target_date'+idf).val();
  var revisi_target = $('#revisi_target'+idf).val();
  $.ajax({
    url: "<?= base_url('pe/numbersolutionsave'); ?>",
    type: "POST",
    dataType: "JSON",
    data: {actype: actype,ata: ata,solution_name: solution_name, uic: uic, target_date: target_date, revisi_target:revisi_target},
    success: function (response) {
      var cokot = '#save_solusi'+idf;
      var cokot1 = '#solution'+idf;
      var cokot2 = '#uic'+idf;
      var cokot3 = '#target_date'+idf;
      var cokot4 = '#revisi_target'+idf;
      var cokot5 = '#iconsave'+idf;
      var number = '#ssa'+idf;
      // $(cokot).removeClass('btn btn-primary btn-sm');
      // $(cokot).addClass('btn btn-success btn-sm');
      // $(cokot).text('');
      // $(cokot).prepend('<i class="fa fa-edit"></i>&nbsp; Update Solution');
      // $(cokot).attr('onclick', 'updatesolusi('+idf+')');
      $(cokot).remove();
      $(cokot1).attr('readonly', true);
      $(cokot2).attr('readonly', true);
      $(cokot3).attr('readonly', true);
      $(cokot4).attr('readonly', true);
      $(number).val(response);
      
      
       // document.getElementById(number).value = response;
       
     },
     error: function (jqXHR, textStatus, errorThrown) {

     }
   });
}
function updatesolusi(idf){
 var solution_name = $('#solution'+idf).val();
 var uic = $('#uic'+idf).val();
 var target_date = $('#target_date'+idf).val();
 var revisi_target = $('#revisi_target'+idf).val();
 var numbersolusi = $('#numbersolusi'+idf).val();
 var cokot1 = '#solution'+idf;
 var cokot2 = '#uic'+idf;
 var cokot3 = '#target_date'+idf;
 var cokot4 = '#revisi_target'+idf;
 var cokot = '#save_solusii'+idf;
 $.ajax({
  url: "<?= base_url('pe/solutionupdate'); ?>",
  type: "POST",
  dataType: "JSON",
  data: {numbersolusi: numbersolusi,solution_name: solution_name,uic: uic,target_date: target_date, uic: uic,revisi_target:revisi_target},
  success: function (response){
    $(cokot).remove();
    $(cokot1).attr('readonly', true);
    $(cokot2).attr('readonly', true);
    $(cokot3).attr('readonly', true);
    $(cokot4).attr('readonly', true);

  },
  error: function (jqXHR, textStatus, errorThrown) {

  }
});

}
function updatesolusi2(idf){
 var solution_name = $('#solutionn'+idf).val();
 var uic = $('#uicc'+idf).val();
 var target_date = $('#targett_date'+idf).val();
 var revisi_target = $('#revisii_target'+idf).val();
 var numbersolusi = $('#numbersolusi'+idf).val();
 var cokot1 = '#solutionn'+idf;
 var cokot2 = '#uicc'+idf;
 var cokot3 = '#targett_date'+idf;
 var cokot4 = '#revisii_target'+idf;
 var cokot = '#save_solusii'+idf;
 $.ajax({
  url: "<?= base_url('pe/solutionupdate'); ?>",
  type: "POST",
  dataType: "JSON",
  data: {numbersolusi: numbersolusi,solution_name: solution_name,uic: uic,target_date: target_date, uic: uic,revisi_target:revisi_target},
  success: function (response){
    $(cokot).remove();
    $(cokot1).attr('readonly', true);
    $(cokot2).attr('readonly', true);
    $(cokot3).attr('readonly', true);
    $(cokot4).attr('readonly', true);

  },
  error: function (jqXHR, textStatus, errorThrown) {

  }
});

}

$(document).on("click", "#opopop", function () {
  var number = $(this).data('unik1');
  var file = $(this).data('unik2');
  var ui = $(this).data('delete');
  $.ajax({
   type:"POST",
   data:{number:number,file:file,ui:ui},
   url:"<?= base_url('Ps/deleteattach') ?>",
   dataType: 'json',
   success: function(data){
    var sao = document.getElementById(data);
    $(sao).remove();
  }
});
});
</script>



