<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-filter"></i>&nbsp; PROBLEM DEFINITION / NEW ISSUE TDAM </h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content inspinia-timeline">
					<form action="<?= base_url('Rd/save_tdam') ?>" method="POST">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<label for="">NUMBER <span style="color: red">*</span> :</label>
								<input type="text" class="form-control" id="number" required="" name="number" readonly="">
								<input type="hidden" class="form-control" id="urutan" required="" name="urutan" readonly="">
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<label for="">CROSS REF :</label>
								<input type="text" readonly="" class="form-control " id="crossreff" value="<?= $EventId; ?>" name="crossreff" >
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<label for="">EVALUATION TYPE <span style="color: red">*</span> :</label>
								<input type="text" value="TDAM" name="evaluation" class="form-control" readonly="">
							</div>
						</div>
						<br>
						<div class="row"> 
							<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
								<label for="">A/C REG : </label>
								<input readonly maxlength="6" value="<?= $ACRegOrig ?>" autocomplete="off" type="text" class="form-control has-feedback-left" id="acreg" name="acreg" placeholder="A/C REG">
							</div>
							<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
								<label for="">ATA <span style="color: red">*</span> : </label>
								<input required="" type="number" class="form-control has-feedback-left"  id="atachap" name="ata" value="<?= $atachap ?>">
							</div>
							<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
								<label for="">FLT NO : </label>
								<input readonly type="text" class="form-control has-feedback-left" value="<?= $FltNo ?>" id="fltno" name="fltno">
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
								<label for="">DATE DEP EVENT : </label>
								<input readonly type="date" value="<?= $DateDeptEvent ?>" class="form-control has-feedback-left" id="system" name="system">
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label for="">ALERT TYPE SRI : </label>
								<select disabled="" class="form-control" name="alert" id="lert" >
									<option value=""></option>
									<option value="ALD">ALD</option>
									<option value="ALP">ALP</option>
									<option value="ALM">ALM</option>
									<option value="ALC">ALC</option>
									<option value="SDR">SDR</option>
									<option value="ACCDNT">ACCDNT</option>
									<option value="ENV">ENV</option>
									<option value="FTG">FTG</option>
									<option value="EXT REP">EXT REP</option>
									<option value="OPS">OPS</option>
								</select>
							</div>

						</div>
						<br>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
								<label for="">SYSTEMS : </label>
								<input  autocomplete="off"  type="text" class="form-control has-feedback-left" id="system" name="system" placeholder="Systems">
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<label for="">A/C TYPE <span style="color: red">*</span> : </label>
								<select required="" id="actype" class="form-control selek2" name="actype">
									<?php if ($ACTypeOrig == 'ATR'): ?>
										<option selected="" value="ATR">ATR72-600</option>
										<?php elseif ($ACTypeOrig == '330') : ?>
											<option value="330">A330</option>
											<?php elseif ($ACTypeOrig == '320') :?>
												<option value="320">A320-200</option>
												<?php elseif ($ACTypeOrig == '733') : ?>
													<option value="733">B737-300</option>
													<?php elseif ($ACTypeOrig == '735'): ?>
														<option value="735">B737-500</option>
														<?php elseif ($ACTypeOrig == '738') : ?>
															<option value="738">B737-800</option>
															<?php elseif ($ACTypeOrig == '739') : ?>
																<option value="739">B737-900</option>
																<?php elseif ($ACTypeOrig == '73M') :?>
																	<option value="73M">B737-MAX</option>
																	<?php elseif ($ACTypeOrig == '777') :?>
																		<option value="777">B777-300</option>
																		<?php elseif ($ACTypeOrig == 'CRJ') :?>	
																			<option value="CRJ">CRJ1000</option>

																			<?php else : ?>
																				<option value=""></option>
																				<?php foreach ($actype as $key ) :?>
																					<option value="<?= $key['value']?>"><?= $key['actype']?></option>
																				<?php endforeach; ?>
																			<?php endif ?>
																		</select>
																	</div>
																	<div class="col-md-4 col-sm-6 col-xs-12">
																		<label for="">OPERATOR  <span style="color: red">*</span> : </label>
																		<select onchange="automaticnumber();" class="form-control operator2" required="" id="operator" name="operator">
																			<option value=""></option>
																			<?php foreach ($operator as $key ) :?>
																				<option value="<?= $key['value']?>"><?= $key['operator']?></option>
																			<?php endforeach; ?>
																		</select>
																	</div>
																	
																</div>
																<br>
																<div class="row" >  
																	<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
																		<label for="">PROBLEM TITLE <span style="color: red">*</span> : </label>
																		<input required=""  autocomplete="off" type="text" class="form-control has-feedback-left" name="problem_title" id="problem_title" value="<?= $Note ?>">
																	</div>
																</div>
																<br>
																<div class="row">
																	<div class="col-md-12 form-group">
																		<label for="">RECTIFICATION :</label>
																		<textarea name="rectification" id="rectification" readonly="" >
																			<?php
																			$outy     = $this->Getdat_model->getRetri($EventId);
																			$getRetri = $outy['Rectification'];
																			?>
																			<?= $getRetri; ?>
																		</textarea>
																	</div>	
																</div>
																<br>
																<div class="row">
																	<div class="col-md-6 form-group">
																		<label for="">TDAM DISCUSSION :</label>
																		<textarea style="max-height: 30px;" name="tdam_discussion" id="tdam_discussion" readonly="" ><?= $Preanalis ?></textarea>
																	</div>
																	<div class="col-md-6 form-group">
																		<label for="">TDAM PROPOSE ACTION :</label>
																		<textarea name="propose_action" id="propose_action" readonly="" ><?= $Description ?></textarea>
																	</div>	
																</div>
																<br>
																<div class="row">
																	<div class="col-md-12 form-group">
																		<label for="">DETAIL PROBLEM :</label>
																		<textarea name="detailproblem" id="detailproblem"></textarea>
																	</div>	
																</div>
																<br>
																<div class="row">
																	<div class="col-md-6">
																		<div class="form-group">
																			<label for="">PIC ASSIGN <span style="color: red">*</span> :</label>
																			<br>
																			<div class="form-group">
																				<select required="" name="pc[]" id="pic3" class="form-control">
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6">
																		<div class="form-group">
																			<label for="">CC : :</label>
																			<br>
																			<div class="form-group">
																				<select name="cc[]" id="cc3" class="form-control">

																				</select>
																			</div>
																		</div>
																	</div>
																</div>  
																<br>
																<div class="row">
																	<div class="col-md-7 col-sm-6 col-xs-12 form-group has-feedback">
																		<label for="">RELATED EVALUATION :</label>
																		<input type="text" placeholder="RELATED EVALUATION" autocomplete="off" class="form-control" name="releated" id="releated" >
																	</div>
																	<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
																		<label for="">TARGET DATE <span style="color: red">*</span> : </label>
																		<input readonly type="date" value="<?= $TargetDate ?>" class="form-control has-feedback-left"  id="target_date" name="target_date" >
																	</div>
																	<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
																		<label for="">UIC <span style="color: red">*</span> : </label>
																		<select required="" class="form-control operator2" id="uic3"  name="uic">
																			<option></option>
																			<?php foreach ($uic as $key) :?>
																				<option value="<?= $key['uic'] ?>"><?= $key['uic'] ?></option>
																			<?php endforeach ;?>
																		</select>
																	</div>
																</div>
																<br>
																<div class="row">
																	<div class="col-md-6">
																		<div class="form-group">
																			<label for=""> ATTACHMENTS :</label>
																			<br>
																			<button data-toggle="modal" type="button" data-target="#upload" style="background: #337AB7; color: white" class="form-control" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; <b>UPLOAD</b></button>
																		</div>
																	</div>
																	<div class="col-md-6">
																		<label for=""> ATTACHMENTS FILE  :</label>
																		<table class="table table-striped" id="skuy">
																			<span id="ee"></span>
																		</table>
																	</div>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-5"></div>
																	<div class="col-md-4">
																		<!-- <button style="margin-right: 15px;" type="submit" name="save_tdam" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button> -->
																		<button type="submit" name="assign_tdam" class="btn btn-success" ><i class="fa fa-send-o"></i>&nbsp;&nbsp; ASSIGN </button>
																	</div>
																</div>
																<br>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<script>
										CKEDITOR.replace( 'propose_action' );
										CKEDITOR.replace( 'tdam_discussion');
										CKEDITOR.replace( 'rectification' );
										CKEDITOR.replace( 'detailproblem' );
										CKEDITOR.config.removePlugins = 'elementspath';
										$('#lert').select2({
											placeholder: "Select Alert",
											allowClear: true
										});
										function automaticnumber(){
											var operator= $('#operator').val();
											var actype= $('#actype').val();
											var ata = $('#atachap').val();
											$.ajax({
												url: "<?= base_url('pd/generate'); ?>",
												type: "POST",
												dataType: "JSON",
												data: {operator: operator,actype: actype},
												success: function (response) {
													document.getElementById("number").value = operator+"-"+actype+"-"+ata+"-"+response+"-"+"00";
													document.getElementById("urutan").value = response;
												},
												error: function (jqXHR, textStatus, errorThrown) {

												}
											});
										}
									</script>


