<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-database"></i>&nbsp; PROBLEM DEFINITION / LIST DRAFT</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped table-hovered" id="table">
                <thead>
                  <tr>
                    <th class="text-center">NO</th>
                    <th class="text-center">NUMBER</th>
                    <th class="text-center">EVALUATION TYPE</th>
                    <th class="text-center">AC/TYPE</th>
                    <th class="text-center">PROBLEM TITLE</th>
                    <th class="text-center">PIC</th>
                    <th class="text-center" width="5%">ACTION</th>         
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($ld as $key ) : ?>
                    <tr>
                      <td><?= $no;?></td>
                      <td><?= $key['number']?></td>
                      <td><?= $key['evaluationtype']?></td>
                      <td><?= cekactype($key['actype'])?></td>
                      <td><?= $key['problemtitle']?></td>
                      <td>
                       <?php 
                       $new_s = rtrim($key['PIC'],',');
                       $s = explode(',',$new_s);
                       for ($i=0; $i < count($s) ; $i++) { 
                        $hasil = cek_email($s[$i]);
                        echo $hasil.',';    
                      }
                      ?>
                    </td>   
                    <td class="text-center">
                      <a href="<?= base_url('update')?>?number=<?=$key['number']?>" class="btn btn-success"><i class="fa fa-external-link-square"></i></a>
                    </td>
                  </tr>
                  <?php
                  $no++;
                endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
