<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-6">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-users"></i>&nbsp; LOG ACTIVITY USER</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
             <div class="table-responsive">
                <table class="table table-bordered table-striped" id="log_reli">
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Name</th>
                      <th>Date & Time Login</th>
                      <th>Login Description</th>
                  </tr>
                </thead>
               
              </table>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <div class="col-md-6">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-users"></i>&nbsp; LOG ACTION RECORD USER</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
             <div class="table-responsive">
                <table class="table table-bordered table-striped" id="sa1">
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Number Problem</th>
                      <th>User</th>
                      <th>Action</th>
                      <th>Date</th>
                      <th>Description</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
         //datatables

         
        table = $('#log_reli').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?= ('log/get_data_user')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
            "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
        });
    });

     $(document).ready(function() {
        var tab2;
         //datatables
        tab2 = $('#sa1').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?= ('log/get_data_action')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
            "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
        });
    });

</script>