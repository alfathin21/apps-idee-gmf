<!Doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/login/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/login/GMF.min.css">
  <link href="<?= base_url()?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link rel="icon" type="image/png" href="<?= base_url()?>assets/images/icon.ico">
  <link href="<?= base_url()?>assets/sweetalert2.min.css" rel="stylesheet">
  <title>IDEE LOGIN </title>
</head>
<body>
 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
 <?php if ($this->session->flashdata('Pesan')) : ?>
 <?php endif; ?>
 <section>
  <div class="container container-gmf">
    <div class="row row-gmf">
      <div class="col-lg-9 col-md-8 col-sm-9 col-xs-8 col-xs-8-top" >
        <a href="#" class="brand-logo"><img class="img img-responsive img-logo" style="margin-top: 40px; position: absolute;z-index:3;margin-left:45px;" src="<?= base_url()?>assets/img/login/logo_fix2.png" width="240px"></a>
        
        <div class="frontText" style="   z-index: 2; position: absolute;color:white;padding-top: 200px;padding-left:60px;">
          <h3 style="color:white; text-shadow: 1px 1px 10px rgba(0,0,0,0.9);">Hi, welcome to IDEE</h3>
          <h3 style="color:white; text-shadow: 1px 1px 10px rgba(0,0,0,0.9);">INTEGRATED&nbsp;DATABASE ENGINEERING EVALUATION </h3>
          <hr style="border:4px solid white;width:45px;margin-left:0px;margin-top:30px !important">
          <span  style="text-shadow: 1px 1px 10px rgba(0,0,0,0.9);" class="textUnder">By : Reliability Management</span>   <br>
          <span style="text-shadow: 1px 1px 10px rgba(0,0,0,0.9);"><i>if you have any trouble, please contact to spoc-ict@gmf-aeroasia.co.id</i></span>   
        </div>
        <img src="<?= base_url()?>assets/img/login/bg.jpg" class="img img-responsive img-lf" >
      </div>
      <div class="col-lg-3 col-md-4 col-sm-3 col-xs-4 col-xs-4-form">
       <form  class="custom-form" method="post">
            <h4 class="text-center">LOGIN TO YOUR ACCOUNT</h4>
            <div class="right-border">
            </div>
            <div class="left-border">
            </div>

            <div class="form-group" style="margin-top: 50px">
              <input type="text" class="form-control" name="uname"  id="user" autofocus required="" />
              <label for="user" class="animated-label">Employee Number</label>
            </div>
            <div class="form-group" style="margin-top: 40px">
              <input type="password" class="form-control" autocomplete="off" name="pass"  id="pass"  required=""/>
              <label for="pass" class="animated-label">Password</label>
            </div>
           
            
            <div class="submit" style="margin-top:20px;">
              <button class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;LOGIN</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</section>      
<script src="<?= base_url()?>assets/js/jquery-1.12.4.min.js"></script>
<script src="<?= base_url()?>assets/js/login/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/sweet.js"></script>
<script>
  const flashData = $('.flash-data').data('flashdata');
  if (flashData == 'Unregistered username') {
   Swal({
    title: 'ERROR !',
    text:   flashData,
    type: 'error'
  });
 }
 else if (flashData == 'Password Wrong !') { 
   Swal({
    title: 'ERROR !',
    text:   flashData,
    type: 'error'
  });
 } 
  else if (flashData == 'Username & Password is wrong') { 
   Swal({
    title: 'ERROR !',
    text:   flashData,
    type: 'error'
  });
 } 
 else if (flashData == 'Logout success') { 
   Swal({
    title: 'Success',
    text:   'Congratulations, your account has successfully logged out ',
    type: 'success'
  });
 } 
 else if (flashData == 'Password is wrong !') {
   Swal({
    title: 'ERROR',
    text:   'Sorry, password is wrong ',
    type: 'error'
  });
 } 
 else if (flashData == 'username not found !') {
   Swal({
    title: 'ERROR',
    text:   'username not found !',
    type: 'error'
  });

 }
 </script>

</body>
</html>




