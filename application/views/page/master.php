<!-- cirian -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-gears"></i>&nbsp; ADMINISTRATOR / MASTER DATA : | UIC | ACTYPE | OPERATOR | EVALUATION TYPE |</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
       <div class="row">
         <div class="col-md-6">
           <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_uic"><i class="fa fa-plus"></i> &nbsp;ADD UIC</button>
           <br>
           <table class="table  table-striped" id="uic_master">
            <thead>
              <th>NO</th>
              <th>UIC</th>
              <th>ACTION</th>
            </thead>
            <tbody id="show_uic"></tbody>
          </table>
        </div>
        <div class="col-md-6">
         <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_alert"><i class="fa fa-plus"></i> &nbsp;ADD ALERT</button>
         <br>
         <table class="table  table-striped" id="alert_master">
          <thead>
            <th>NO</th>
            <th>ALERT</th>
            <th>ACTION</th>
          </thead>
          <tbody id="show_alert"></tbody>
        </table>
      </div>

      <div class="col-md-6">
       <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_actype"><i class="fa fa-plus"></i> &nbsp;ADD ACTYPE</button>
       <br>
       <table class="table  table-striped" id="actype_master">
        <thead>
          <th>NO</th>
          <th>ACTYPE</th>
          <th>ACTION</th>
        </thead>
        <tbody id="show_actype"></tbody>
      </table>
    </div>
    <div class="col-md-6">
     <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_evaluation"><i class="fa fa-plus"></i> &nbsp;ADD EVALUATION TYPE</button>
     <br>
     <table class="table  table-striped" id="evaluation_master">
      <thead>
        <th>NO</th>
        <th>EVALUATION TYPE</th>
        <th>ACTION</th>
      </thead>
      <tbody id="show_evaluation"></tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
   <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_operator"><i class="fa fa-plus"></i> &nbsp;ADD OPERATOR</button>
   <br>
   <table class="table  table-striped" id="operator_master">
    <thead>
      <th>NO</th>
      <th>OPERATOR</th>
      <th>ACTION</th>
    </thead>
    <tbody id="show_operator"></tbody>
  </table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>





<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="modal_add_role" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-users"></i> &nbsp; ADD NEW ROLE</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">ROLE NAME : </label>
          <input type="text" class="form-control" id="add_role">
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" id="save_role"><i class="fa fa-arrow-right"></i> SAVE ROLE</button>
      </div>
    </form>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="modal_add_uic" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-database"></i> &nbsp; ADD NEW UIC</h4>
      </div>
      <form action="<?= base_url('role/save') ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="">UIC NAME : </label>
            <input type="text" class="form-control" required="" name="uic_name">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div class="modal fade" id="modal_add_evaluation" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-database"></i> &nbsp; ADD NEW EVALUATION TYPE</h4>
      </div>
      <form action="<?= base_url('role/saveevaluation') ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="">EVALUATION TYPE NAME : </label>
            <input type="text" class="form-control" required="" name="uic_name">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="modal_add_alert" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-database"></i> &nbsp; ADD NEW ALERT</h4>
      </div>
      <form action="<?= base_url('role/savealert') ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="">ALERT NAME : </label>
            <input type="text" class="form-control" required="" name="alert_name">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="modal_add_operator" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-database"></i> &nbsp; ADD NEW OPERATOR</h4>
      </div>
      <form action="<?= base_url('role/saveoperator') ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="">OPERATOR NAME : </label>
            <input type="text" class="form-control" required="" name="operator_name">
          </div>
          <div class="form-group">
            <label for="">VALUE TO NUMBER IDEE  : </label>
            <input type="text" class="form-control" maxlength="2" required="" name="value_name">
          </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="modal_add_actype" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-database"></i> &nbsp; ADD NEW ACTYPE</h4>
      </div>
      <form action="<?= base_url('role/saveactype') ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="">ACTYPE NAME : </label>
            <input type="text" class="form-control" required="" name="operator_name">
          </div>
          <div class="form-group">
            <label for="">VALUE TO NUMBER IDEE  : </label>
            <input type="text" class="form-control" maxlength="3" required="" name="value_name">
          </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->


<script>
  $(document).ready(function() {
    tampil_data_uic();
    tampil_data_alert();
    tampil_data_operator();
    tampil_data_actype();
    tampil_data_evaluation();
    $('#uic_master').DataTable({
     "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
   });


    $('#alert_master').DataTable({
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
    });

    $('#actype_master').DataTable({
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
    });
    $('#evaluation_master').DataTable({
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
    });
  })
  
  function tampil_data_uic(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_uic',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          if (data[i].status == 'OPEN') {
            var a = '<a href="<?= base_url('role/update')?>?tab=uic_master&i='+data[i].id+'&p=HIDDEN" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>';
          } else {
           var a = '<a href="<?= base_url('role/update')?>?tab=uic_master&i='+data[i].id+'&p=OPEN" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
         }
         html += '<tr>'+
         '<td>'+no+'</td>'+
         '<td>'+data[i].uic+'</td>'+
         '<td>'+a+'</td>'+
         '</tr>';
         no++;
       }
       $('#show_uic').html(html);
     }
   });
  }
  function tampil_data_alert(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_alert',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          if (data[i].status == 'OPEN') {
            var a = '<a href="<?= base_url('role/update')?>?tab=alert_master&i='+data[i].id+'&p=HIDDEN" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>';
          } else {
           var a = '<a href="<?= base_url('role/update')?>?tab=alert_master&i='+data[i].id+'&p=OPEN" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
         }
         html += '<tr>'+
         '<td>'+no+'</td>'+
         '<td>'+data[i].alert+'</td>'+
         '<td>'+a+'</td>'+
         '</tr>';
         no++;
       }
       $('#show_alert').html(html);
     }
   });
  }  
  function tampil_data_operator(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_operator',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          if (data[i].status == 'OPEN') {
            var a = '<a href="<?= base_url('role/update')?>?tab=operator_master&i='+data[i].id+'&p=HIDDEN" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>';
          } else {
           var a = '<a href="<?= base_url('role/update')?>?tab=operator_master&i='+data[i].id+'&p=OPEN" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
         }
         html += '<tr>'+
         '<td>'+no+'</td>'+
         '<td>'+data[i].operator+'</td>'+
         '<td>'+a+'</td>'+
         '</tr>';
         no++;
       }
       $('#show_operator').html(html);
     }
   });
  }

  function tampil_data_actype(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_actype',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          if (data[i].status == 'OPEN') {
            var a = '<a href="<?= base_url('role/update')?>?tab=actype_master&i='+data[i].id+'&p=HIDDEN" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>';
          } else {
           var a = '<a href="<?= base_url('role/update')?>?tab=actype_master&i='+data[i].id+'&p=OPEN" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
         }
         html += '<tr>'+
         '<td>'+no+'</td>'+
         '<td>'+data[i].actype+'</td>'+
         '<td>'+a+'</td>'+
         '</tr>';
         no++;
       }
       $('#show_actype').html(html);
     }
   });
  }
  function tampil_data_evaluation(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_evaluation',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          if (data[i].status == 'OPEN') {
            var a = '<a href="<?= base_url('role/update')?>?tab=evaltype_master&i='+data[i].id+'&p=HIDDEN" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>';
          } else {
           var a = '<a href="<?= base_url('role/update')?>?tab=evaltype_master&i='+data[i].id+'&p=OPEN" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
         }
         html += '<tr>'+
         '<td>'+no+'</td>'+
         '<td>'+data[i].evaltype+'</td>'+
         '<td>'+a+'</td>'+
         '</tr>';
         no++;
       }
       $('#show_evaluation').html(html);
     }
   });
  }
</script>
<!-- cirian -->