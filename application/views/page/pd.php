<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-edit"></i>&nbsp; PROBLEM DEFINITION / NEW ISSUE</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <form action="<?= base_url('pd/savedraft') ?>" method="post" name="pd" id="pd">
           <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">NUMBER <span style="color: red">*</span> : </label>
              <input required="" type="text" readonly="" class="form-control has-feedback-left" id="number" name="number" >
              <input type="hidden" name="urutan" id="urutan" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">CROSS REF :</label>
              <input placeholder="CROSS REF" autocomplete="off" type="text" class="form-control has-feedback-left" id="crossreff" name="crossreff" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">EVALUATION TYPE <span style="color: red">*</span> :</label>
              <select required class="form-control select4" name="evaluation" id="evaluation">
                <option value=""></option>
               <?php foreach ($evaltype as $key ) :?>
                <option value="<?= $key['evaltype']?>"><?= $key['evaltype']?></option>
              <?php endforeach; ?>
              </select>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">OPERATOR <span style="color: red">*</span> : </label>
              <select required="" class="form-control Operator" id="operator" name="operator" >
                <option value=""></option>
             <?php foreach ($operator as $key ) :?>
                <option value="<?= $key['value']?>"><?= $key['operator']?></option>
              <?php endforeach; ?>
              </select>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">A/C Type <span style="color: red">*</span> : </label> 
              <select required="" id="actype" class="form-control selek2" name="actype"  >
                <option value=""></option>
                 <?php foreach ($actype as $key ) :?>
                <option value="<?= $key['value']?>"><?= $key['actype']?></option>
              <?php endforeach; ?>
              </select>    
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">ALERT TYPE SRI : </label>
              <select class="form-control" name="alert" id="alert" >
                <option value=""></option>
                <?php foreach ($alert as $key ) :?>
                <option value="<?= $key['alert']?>"><?= $key['alert']?></option>
              <?php endforeach; ?>
              </select>
            </div>
          </div>
          <br>
          <div class="row"> 
            <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">A/C REG : </label>
              <input maxlength="6"  autocomplete="off" type="text" class="form-control has-feedback-left" id="acreg" name="acreg" placeholder="A/C REG">
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
             <label for="">ATA <span style="color: red">*</span> : </label>
             <input required="" maxlength="2" autocomplete="off" type="number" class="form-control has-feedback-left"  id="ata" name="ata" placeholder="Code ATA">

           </div>
           <div class="col-md-8 col-sm-6 col-xs-12 form-group has-feedback">
             <label for="">SYSTEMS : </label>
             <input  autocomplete="off" type="text" class="form-control has-feedback-left" id="system" name="system" placeholder="Systems">
           </div>
         </div>
         <br>
         <div class="row" >  
           <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
             <label for="">PROBLEM TITLE <span style="color: red">*</span> : </label>
             <input required="" autocomplete="off" type="text" class="form-control has-feedback-left" name="problem_title" id="problem_title" placeholder="Problem Title" >
           </div>
         </div>
         <br>
         <div class="row" >
          <div class="col-md-12 col-sm-12 col-xs-12">
            <label for="">DETAIL OF PROBLEM :</label>
            <textarea  name="editor" id="editor" class="editor"></textarea>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="">PIC ASSIGN <span style="color: red">*</span> :</label>
              <br>
              <div class="form-group">
                <select required="" name="pc[]" id="pc" class="form-control">
                 
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">CC :</label>
              <br>
              <div class="form-group">
                <select name="cc[]" id="cc" class="form-control">
                
                </select>
              </div>
            </div>
          </div>
        </div>  
        <br>
        <div class="row" >
          <div class="col-md-7 col-sm-6 col-xs-12 form-group has-feedback">
           <label for="">RELATED EVALUATION :</label>
           <input type="text" placeholder="RELATED EVALUATION" autocomplete="off" class="form-control" name="releated" id="releated" >
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
          <label for="">TARGET DATE <span style="color: red">*</span> : </label>
          <input required="" type="date" class="form-control has-feedback-left"  id="target_date" name="target_date" >
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
          <label for="">UIC <span style="color: red">*</span> : </label>
          <select required="" class="form-control Operator" id="uic"  name="uic">
            <option></option>
            <?php foreach ($uic as $key) :?>
            <option value="<?= $key['uic'] ?>"><?= $key['uic'] ?></option>
          <?php endforeach ;?>
          </select>
        </div>
      </div>
      <br>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for=""> ATTACHMENTS :</label>
            <br>
            <button data-toggle="modal" type="button" data-target="#upload" style="background: #337AB7; color: white" class="form-control" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; <b>UPLOAD</b></button>
          </div>
        </div>
        <div class="col-md-6">
          <label for=""> ATTACHMENTS FILE  :</label>
          <table class="table table-striped" id="skuy">
            <span id="ee"></span>
          </table>
        </div>
      </div>

      <hr>
      <div class="row" >
        <div class="col-md-5"></div>
        <div class="col-md-4">
          <button style="margin-right: 15px;" type="submit" name="simpan_draft" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button>
          <button type="submit" name="assign" class="btn btn-success" ><i class="fa fa-send-o"></i>&nbsp;&nbsp; ASSIGN </button>
        </div>


      </form>
    </div>
  </div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="searchnumber" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-file-code-o"></i> &nbsp; DRAFT PROBLEM NUMBER   </h4>
      </div>
      <form method="POST" action="pd/s_d">
        <div class="modal-body">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <label>NUMBER DRAFT :</label>
                <input type="hidden" name="nopeg" value="<?php echo $this->session->userdata('nopeg') ?>" id="nopeg_act">
                <input autofocus="on" autocomplete="" id="number_draft" required=""  name="number_draft" type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit"  name="s_draft"><i class="fa fa-search"></i> Search</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!-- akhir modal -->
<script>
 CKEDITOR.replace( 'editor' );
CKEDITOR.config.removePlugins = 'elementspath';

</script>
