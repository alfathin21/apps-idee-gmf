 <?php $unit = $this->session->userdata('unit'); ?>
 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
 <div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query">
        <div class="ibox-title">
          <h5><i class="fa fa-search"></i>&nbsp; PROBLEM SEARCH  </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
           <div class="col-md-4 col-sm-6 col-xs-12">
             <label for="">NUMBER :</label>
             <input type="text" class="form-control" id="number" name="number" placeholder="Input Number Problem">
             <input type="hidden" class="form-control" id="unit" value="<?= $act ?>">
           </div>
           <div class="col-md-4 col-sm-6 col-xs-12 ">
             <label for="">CROSS REF :</label>
             <input type="text" class="form-control " id="crossreff" name="crossreff" placeholder="Input Cross Reff">
           </div>
           <div class="col-md-4 col-sm-6 col-xs-12 ">
             <label for="">EVALUATION TYPE :</label>
             <select class="form-control select4" name="evaluation" id="evaluation">
              <option selected=""></option>
              <?php foreach ($evaltype as $key ) :?>
                <option value="<?= $key['evaltype']?>"><?= $key['evaltype']?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="col-md-4 col-sm-6 col-xs-12">
           <label for="">ALERT TYPE SRI : </label>
           <select class="form-control" name="alert" id="alert">
            <option selected=""></option>
            <?php foreach ($alert as $key ) :?>
              <option value="<?= $key['alert']?>"><?= $key['alert']?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <label for="">OPERATOR : </label>
          <select class="form-control operator2" id="operator2" name="operator">
           <?php if ($unit == 'GA') : ?>
            <option selected="" readonly value="GA">GARUDA INDONESIA</option>
            <?php elseif ($unit == 'QG') :?>
              <option selected="" readonly value="QG">CITILINK</option>
              <?php elseif ($unit == 'IN') :?>
                <option selected="" readonly value="IN">NAM AIR</option>
                <?php elseif ($unit == 'SJ') :?>
                  <option selected="" readonly value="SJ">SRIWIJAYA</option>
                  <?php else : ?>
                    <option value=""></option>


                    <?php foreach ($operator as $key ) :?>
                      <option value="<?= $key['value']?>"><?= $key['operator']?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <label for="">A/C TYPE : </label>
                <select id="actype" class="form-control selek2" name="actype">
                 <option selected=""></option>
                 <?php foreach ($actype as $key ) :?>
                  <option value="<?= $key['value']?>"><?= $key['actype']?></option>
                <?php endforeach; ?>
              </select>    
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-2 col-sm-6 col-xs-12">
              <label for="">A/C REG : </label>
              <input type="text" class="form-control" id="acreg" name="acreg" placeholder="AC REG" >
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">UIC  : </label>
              <select class="form-control Operator" id="uic"  name="uic">
                <option></option>
                <?php foreach ($uic as $key) :?>
                  <option value="<?= $key['uic'] ?>"><?= $key['uic'] ?></option>
                <?php endforeach ;?>
              </select>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
             <label for="">PROBLEM TITLE : </label>
             <input type="text" class="form-control" id="problem_title" name="problem_title" placeholder="Problem Title" >
           </div>
         </div>
         <br>
         <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <label for="">STATUS : </label>
            <select name="status" class="form-control" id="status">
              <option value="">Select Status Problem</option>
              <option value="OPEN">OPEN</option>
              <option value="PROGRESS">PROGRESS</option>
              <option value="REVIEW">REVIEW</option>
              <option value="CLOSE">CLOSE</option>
            </select>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <label for="">PIC : </label>
            <select  name="pic"  id="pic" class="form-control" >

            </select>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <label for="">KEYWORD : </label>
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword" >
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <label for="">DATE FROM : </label>
            <input type="date" class="form-control"  id="date_from" name="date_from" >
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <label for="">DATE TO :</label>
            <input type="date" class="form-control" id="date_to" name="date_to" >
          </div>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <label for="">ATA :</label>
            <input type="number" placeholder="Ata Number" maxlength="2" class="form-control" id="ata2" name="ata" >
          </div>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <br>
            <button type="button" id="search" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp; SEARCH </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-line-chart"></i>&nbsp; SEARCH RESULT</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="result_search"></table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</form>

<!-- modal detail problem -->
<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-desktop"></i> &nbsp;PROBLEM DETAIL</h4>
      </div>
      <div class="modal-body" style="overflow-y:auto; height:70vh;">
        <form action="">
          <div class="row">
            <div class="col-md-3">
              <label for="">NUMBER:</label>
              <input readonly="" type="text" class="form-control" id="number_v"  name="number_v" >
            </div>
            <div class="col-md-3">
              <label for="">CROSS REFF :</label>
              <input readonly="" type="text" class="form-control" id="crossreff_v"  name="crossreff_v" >
            </div>
            <div class="col-md-3">
              <label for="">EVALUATION TYPE :</label>
              <input readonly="" type="text" class="form-control" id="eval_v"  name="eval_v" >
            </div>
            <div class="col-md-3">
              <label for="">ALERT TYPE :</label>
              <input readonly="" type="text" class="form-control" id="alert_sri"  name="alert_sri" >
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-3">
              <label for="">OPERATOR :</label>
              <input readonly="" type="text" class="form-control" id="operator_v"  name="operator_v" >
            </div>
            <div class="col-md-3">
              <label for="">A/C TYPE :</label>
              <input readonly="" type="text" class="form-control" id="actype_v"  name="actype_v" >
            </div>

            <div class="col-md-3">
              <label for="">A/C REG :</label>
              <input readonly="" type="text" class="form-control" id="acreg_v"  name="acreg_v" >
            </div>
            <div class="col-md-3">
              <label for="">ATA :</label>
              <input readonly="" type="text" class="form-control" id="ata_v"  name="ata_v" >
            </div>
          </div>
          <br>
          <div class="row">

            <div class="col-md-7">
              <label for="">PROBLEM TITLE :</label>
              <input readonly="" type="text" class="form-control" id="problem_title_v"  name="problem_title_v" >
            </div>


            <div class="col-md-5">
              <label for="">SYSTEMS :</label>
              <input readonly="" type="text" class="form-control" id="systems_v"  name="systems_v" >
            </div>
          </div>
          <br>


          <div class="row">
            <div class="col-md-10">
              <label for="">PIC :</label>
              <input readonly="" type="text" class="form-control" id="pic_v"  name="pic_v" >
            </div>
            <div class="col-md-2">
              <label for="">UIC :</label>
              <input readonly="" type="text" class="form-control" id="uic_v"  name="uic_v" >
            </div>
          </div>
          <br>


          <div class="row">
            <div class="col-md-3">
              <label for="">STATUS :</label>
              <input type="button" class="form-control "  id="status_v"  name="status_v" >
            </div>

          </div>
          <br>
          <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <label for="">REMARKS :</label>
                <ul class="list-group">
                  <li id="remarks_v" class="list-group-item"></li>

                </ul>
              </div>
            </div>

          </div>

          <div class="row" >
            <div class="col-md-12 col-sm-12 col-xs-12">
              <label for="">PROBLEM DESCRIPTION :</label>
              <textarea disabled="" name="problem_description_v" id="problem_description_V" class="problem_description_V"></textarea>
            </div>
          </div>

          <br>
          <div class="row" >
            <div class="col-md-12 col-sm-12 col-xs-12">
              <label for="">PROBLEM ANALYSIS :</label>
              <textarea disabled="" name="problem_analisis_v" id="problem_analisis_v" class="problem_analisis_v"></textarea>
            </div>
          </div>
          <br>
          <hr>
          <div class="row">
            <div class="col-md-3">
              <label for=""> Document Attachment :</label>
            </div>
            <div class="col-md-9">
              <div >
                <ul  id="beda">
                </ul>
              </div>
            </div>
          </div>
          <hr>
          <br>
          <table class="table table-bordered table-striped" id="table">
            <thead>
              <tr>
                <th class="text-center" width="5%">NO</th>
                <th class="text-center">SOLUTION</th>
                <th class="text-center"width="8%">UIC</th>
                <th class="text-center"width="4px">STATUS(%)</th>
                <th class="text-center"width="">REMARKS</th>
                <th class="text-center"width="">FILE ATTACHMENTS</th>
              </tr>
            </thead>
            <tbody>
              <?php for($i = 1; $i < 10 ; $i++) { ?>
                <tr>
                  <td id="no_v_<?=$i;?>"></td>
                  <td id="solution_v_<?=$i;?>"></td>
                  <td class="text-center" id="target_date_v_<?=$i;?>"></td>
                  <td class="text-center" id="status_v_<?=$i;?>"></td>
                  <td class="text-center" id="remarks_v_<?=$i;?>"></td>
                  <td class="text-center" id="file_v_<?=$i;?>"></td>
                </tr>
              <?php } ;?>

            </tbody>
            <tbody>
            </tbody>
          </table>


        </div>

        <div class="modal-footer">
          <a id="pdf_all" target="_blank" class="btn btn-primary"><i class="fa fa-file-powerpoint-o"></i> PDF ALL</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!--  -->

  <!-- modal update -->
  <div class="modal fade" aria-labelledby="update" aria-hidden="true" id="update" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
      <div class="modal-content ">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-external-link"></i> &nbsp; STATUS UPDATE</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto; height:70vh;">
          <form action="">
            <div class="row">
              <div class="col-md-3">
                <label for="">NUMBER:</label>
                <input type="text" disabled="" class="form-control" id="number_p"  name="number_p" >
              </div>
              <div class="col-md-3">
                <label for="">CROSS REFF:</label>
                <input type="text" disabled="" class="form-control" id="crossreff_p"  name="crossreff_p" >
              </div>
              <div class="col-md-3">
                <label for="">EVALUATION TYPE :</label>
                <input type="text" disabled="" class="form-control" id="eval_p"  name="eval_p" >
              </div>
              <div class="col-md-3">
                <label for="">ALERT TYPE SRI :</label>
                <input type="text" disabled="" class="form-control" id="alert_p"  name="alert_p" >
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-3">
                <label for="">OPERATOR :</label>
                <input type="text" disabled="" class="form-control" id="operator_p"  name="operator_p" >
              </div>
              <div class="col-md-3">
                <label for="">A/C TYPE :</label>
                <input type="text" disabled="" class="form-control" id="actype_p"  name="actype_p" >
              </div>
              <div class="col-md-3">
                <label for="">A/C REG :</label>
                <input type="text" disabled="" class="form-control" id="acreg_p"  name="acreg_p" >
              </div>
              <div class="col-md-3">
                <label for="">ATA :</label>
                <input type="text" disabled="" class="form-control" id="ata_p"  name="ata_p" >
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-9">
                <label for="">SYSTEMS :</label>
                <input type="text" disabled="" class="form-control" id="systems_p"  name="systems_p" >
              </div>
              <div class="col-md-3">
                <label for="">UIC :</label>
                <input type="text" class="form-control" disabled="" id="uic_p">

              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12">
                <label for="">PROBLEM TITLE :</label>
                <input type="text" disabled="" class="form-control" id="problem_title_p"  name="problem_title_p" >
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-9" >
                <label for="">PIC :</label>
                <input type="hidden" id="pic_update_hidden" class="form-control">
                <select name="pic_update" class="form-control" id="pic_update" multiple=""  style="width:100%;">      
                  <?php foreach ($te as $key) : ?>
                    <option value="<?= strtolower($key['EMAIL']) ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-3">
                <label for="">TARGET DATE :</label>
                <input type="date" class="form-control" id="target_date_update">
                <input type="hidden" class="form-control" id="target_date_update_hidden">
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">EVALUATION STATUS :</label>
                <select class="form-control select5" name="eval_status" id="eval_status">
                  <option value="OPEN">OPEN</option>
                  <option value="REVIEW">REVIEW</option>
                  <option value="PROGRESS">PROGRESS</option>
                  <option value="CLOSE">CLOSE</option>
                </select>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">ON WATCH :</label>
                <select class="form-control select5" name="onwatch" id="onwatch">
                  <option value="YES">YES</option>
                  <option value="NO">NO</option>
                </select>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label for="">REMARKS : </label>
                <input type="text" class="form-control" id="remarks_problem"  name="remarks_problem" >
                <input type="hidden" class="form-control" id="status_hidden"  name="status_hidden" >
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-8 form-group has-feedback">
                <label for="">COMMENTS REVIEW : </label>
                <input type="text" class="form-control" id="comments" name="comments">
              </div>
              <div class="col-md-4 form-group has-feedback">
                <label for="">SRI EFFECTIVENESS REVIEW : </label>
                <input type="text" class="form-control" id="sri" name="sri">
              </div>
            </div>
            <br>
            <div id="solutions"></div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <label for="">ATTACHMENTS FILE :</label>
                <div class="dropzone" id="zone_upload">
                  <div class="dz-message">
                   <h3> <i class="fa fa-files-o"></i> <b>Click or Drop  Files</b> to upload <i class="fa fa-hand-o-left"></i></h3>
                 </div>
               </div>
             </div>

             <div class="row">
               <div class="col-md-6">
                <label for=""> LAST FILE ATTACHMENTS :</label>
                <div>
                  <ul id="tampung">
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <br>



        </form>
      </div>
      <div class="modal-footer">
        <button id="save" type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- akhir modal update -->


<script>
  $(document).on("click", "#udate", function () {
    var number = $(this).data('number_d');
    $('.modal-body #number_p').val(number);
    $('.modal-body #crossreff_p').val($(this).data('cross_d'));
    $('.modal-body #eval_p').val($(this).data('eval_d'));
    $('.modal-body #alert_p').val($(this).data('alert_d'));
    $('.modal-body #ata_p').val($(this).data('ata_d'));
    $('.modal-body #actype_p').val($(this).data('actype_d'));
    $('.modal-body #operator_p').val($(this).data('operator_d'));
    $('.modal-body #acreg_p').val($(this).data('acreg_d'));
    $('.modal-body #systems_p').val($(this).data('systems_d'));
    $('.modal-body #problem_title_p').val($(this).data('problemtitle_d'));
    $('.modal-body #pic_p').val($(this).data('pic_d'));
    $('.modal-body #pic_update_hidden').val($(this).data('pic_d'));
    $('.modal-body #uic_p').val($(this).data('uic_d'));
    $('.modal-body #comments').val($(this).data('comments_d'));
    $('.modal-body #sri').val($(this).data('sri_d'));
    $('#eval_status').val($(this).data('status_d'));
    $('#onwatch').val($(this).data('onwatch'));
    $('#status_hidden').val($(this).data('status_d'));
    $('#target_date_update').val($(this).data('target_date_d'));
    $('#target_date_update_hidden').val($(this).data('target_date_d'));
    $('.modal-body #remarks_problem').val($(this).data('remarks_d'));
    var psao =  $(this).data('pic_d');
    var arr_pic = psao.split(",");
    var file = $(this).data('file_d');
    $("#solutions").html("");
    $("#tampung").html("");
    $(".modal-body #pic_update").select2({
      multiple: true,
      dropdownParent: $(".modal-body #pic_update").parent(),
    });
    $(".modal-body #pic_update").val(0).trigger('change');
    for (var i = 0; i < arr_pic.length; i++) {
      var $newOption = $("<option selected='selected'></option>").val(arr_pic[i]).text(arr_pic[i]);
      $(".modal-body #pic_update").append($newOption).trigger('change');
    }
    if (file !== '') {
     var arr_file = file.split(",");

     var count_arr_file= arr_file.length;
     var rl='<?= base_url('files/');?>';
     for(var o = 0; o < count_arr_file; o++){
       var token = arr_file[o];
       console.log(token);
       $.ajax({
        type:"POST",
        data:{token:token},
        url:"<?= base_url('pd/searchfile') ?>",
        dataType: 'json',
        success: function(data){
          var file_name = data.nama_file;
          var file_baru = data.nama_baru;
           if (file_baru == null ) {
             var io = '<li id= "'+file_name+'"><a style="margin-right:10px" href="'+rl+file_name+'"  target ="_blank" class="btn btn-sm btn-success">'+file_name+'</a><a id="opopop" type="button" data-unik1="'+number+'" data-delete="'+file_name+'" data-unik2="'+data.token+'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></li>';
           } else {
             var io = '<li id= "'+file_name+'"><a style="margin-right:10px" href="'+rl+file_baru+'"  target ="_blank" class="btn btn-sm btn-success">'+file_name+'</a><a id="opopop" type="button" data-unik1="'+number+'" data-delete="'+file_name+'" data-unik2="'+data.token+'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></li>';
           }
          var ooi = '</br>';
          $('#tampung').append(io); 
          $('#tampung').append(ooi);
        },
      });
     }

   }
   for (var i = 0; i < 9; i++) {
     var solution =  $(this).attr('data-solution'+i+'_d');
     if (solution !== '') {            
       $.ajax({
        type:"POST",
        data:{solution:solution},
        url:"<?= base_url('pd/searchsolutionupdate') ?>",
        dataType: 'json',
        success: function(data){
          var dropZoneUpload = data[0].numbersolution;
          var id_tokenFile = data[0].numbersolution+'d';
          var string_pas = '<div class="sol"><div class="row "><div class="col-md-7"><input type="hidden" id="numbersolution" value="'+data[0].numbersolution+'"><input type="hidden" id="'+id_tokenFile+'" ><input id="hidden_token_file" type="hidden" value="'+data[0].hidden+'" ><div class="form-group"><label for="">SOLUTIONS :</label><ul class="list-group"><li id="namesolution" class="list-group-item">'+data[0].solution+'</li></ul></div></div><div class="col-md-2"><div class="form-group"><label for="">STATUS (%) :</label><input type="number" id="percent" class="form-control" value="'+data[0].status+'"></div></div><div class="col-md-3"><div class="form-group"><label for="">REMARKS :</label><input id="remarks_solutions" value="'+data[0].remarks+'" type="text" class="form-control"></div></div><div class="col-md-12"><label>Attachments Solutions :</label><div class="dropzone" id="'+dropZoneUpload+'"><div class="dz-message"><h3><i class="fa fa-files-o"></i> <b>Click or Drop  Files</b> to upload Attachments Solutions <i class="fa fa-hand-o-left"></i></h3></div></div></div></div><hr><br>';
          $('#solutions').append(string_pas);
          var file_solution_last =  data[0].file;
          $('#tampung').append(file_solution_last);
          Dropzone.autoDiscover = false;
          var fileList = new Array;
          var i =0;
          var o; 
          var d = document.getElementById(dropZoneUpload);
          var s = document.getElementById(id_tokenFile);
          var rl='<?= base_url('files/');?>';
          var foto_upload= new Dropzone(d,{
            url: "<?= base_url('pe/proses_upload') ?>",
            maxFilesize: 4,
            method:"post",
            paramName:"userfile",
            dictInvalidFileType:"Type file ini tidak dizinkan",
            addRemoveLinks:true,
            init: function() {
              this.on("success", function(file, serverFileName,responseURL) {
                   var l = 0;
                var i =  responseURL.currentTarget;
                var b = i.response;
                var c = JSON.parse(b);
                var d = c.nama_awal;
                var e = c.nama_baru;
                var k;
                    fileList[l] = {"nama_file" : d, "fileId" : l, "nama_filebaru" : e};
                l++;
                for (k in fileList)
                {
                  var hasil = fileList[k]['nama_file'];
                  var oopl = document.getElementById(id_tokenFile);
                  $(oopl).val('File_ada');
                  var hasil_akhir = fileList[k]['nama_filebaru'];
                  var stre121 = '<li id="'+hasil+'"><a class="badge badge-success" target="_blank"  href='+rl+hasil_akhir+' >'+hasil+'</a></li></br>';
                  $("#tampung").append(stre121);
                  fileList.pop();
                }

              });
            },
          });
          foto_upload.on("sending",function(a,b,c){
           a.token=Math.random();
           a.number=  data[0].numbersolution;
           c.append("token_file",a.token);
           c.append("number",a.number);

           fileList.pop();
         })
          foto_upload.on("removedfile",function(a){
            var token=a.token;
            $.ajax({
              type:"post",
              data:{token:token},
              url:"<?= base_url('pe/remove_file') ?>",
              cache:false,
              dataType: 'json',
              success: function(data){
               var rm = document.getElementById(data);
               $(rm).remove();
             }
           });
          }); 

        },
      });            
} 
}
});
 $(document).on("click", "#save", function () {
   var pic                   =  $('#pic_update').val();
   var pic_hidden            =  $('#pic_update_hidden').val();
   var output_pic            =  pic.join();
   var eval_status           =  $('#eval_status').val();
   var number_p              =  $('#number_p').val();
   var numbersolution        =  $('#numbersolution').val();
   var percent               =  $('#percent').val();
   var remarks_solutions     =  $('#remarks_solutions').val();
   var remarks_problem       =  $('#remarks_problem').val();
   var target_date_update    =  $('#target_date_update').val();
   var comments              = $('#comments').val();
   var status_hidden         = $('#status_hidden').val();
   var onwatch         = $('#onwatch').val();
   var target_date_update_hidden     = $('#target_date_update_hidden').val();
   var sri                   = $('#sri').val();
   var arr = [];
   $(".sol").each(function(){  
    var id_poco               =   $(this).find('#numbersolution').val();
    var olk                   =   id_poco+'d';  
    var oiklk                 =  document.getElementById(olk); 
    var s = $(this).find('#numbersolution').val();
    var b = $(this).find('#percent').val();
    var c = $(this).find('#remarks_solutions').val();
    var d = $(this).find(oiklk).val();
    var e = $(this).find('#hidden_token_file').val();
    arr.push({
      n_s_s:s,
      per:b,
      rem:c,
      fil:d,
      hid:e
    })
  });
   if (arr.length > 0) {
    $.ajax({
      type:"post",
      data:{'comments' : comments,'arr':arr,'eval_status': eval_status,'number_p':number_p,'remarks_problem': remarks_problem,'output_pic': output_pic,'target_date_update': target_date_update,'sri': sri,'status_hidden':status_hidden,'target_date_update_hidden':target_date_update_hidden,'pic_hidden' : pic_hidden,'onwatch' : onwatch},
      url:"<?= base_url('pd/teas') ?>",
      success: function(){
        document.location.href = '<?= base_url('ps');?>'; 
      }
    });

  } else {
    $.ajax({
      type:"post",
      data:{'comments' : comments, 'eval_status': eval_status,'number_p':number_p,'remarks_problem': remarks_problem,'output_pic': output_pic,'target_date_update': target_date_update,'sri': sri,'status_hidden':status_hidden,'target_date_update_hidden':target_date_update_hidden,'pic_hidden' : pic_hidden,'onwatch' : onwatch},
      url:"<?= base_url('pd/teas') ?>",
      success: function(){
        document.location.href = '<?= base_url('ps');?>'; 
      }
    });
  }
});

 $(document).on('click', "#search", function() {
  if ($.fn.dataTable.isDataTable('#result_search')) {
    var table = $("#result_search").DataTable();
    table.destroy();
  }
  $('#result_search').html('');
  var number = $('#number').val();
  var crossreff = $('#crossreff').val();
  var unit = $('#unit').val();
  var evaluation = $('#evaluation').val();
  var alerttype = $('#alert').val();
  var operator = $('#operator2').val();
  var actype = $('#actype').val();
  var acreg = $('#acreg').val();
  var uic = $('#uic').val();
  var problem_title = $('#problem_title').val();
  var status = $('#status').val();
  var pic = $('#pic').val();
  var keyword = $('#keyword').val();
  var date_to = $('#date_to').val();
  var date_from = $('#date_from').val();
  var ata     = $('#ata2').val();
  var html = '';
  var pencarian_hasil = '<thead id ="head_cek">' +
  '<th class="text-center">NO</th>' +
  '<th class="text-center">NUMBER</th>' +
  '<th class="text-center">EVALUATION TYPE</th>' +
  '<th class="text-center">A/C TYPE</th>' +
  '<th class="text-center">PROBLEM TITLE</th>' +
  '<th class="text-center">PIC</th>' +
  '<th class="text-center">STATUS</th>' +
  '<th class="text-center">ACTION</th>' +
  '</tr></thead>' +
  '<tbody id="enakeun"></tbody>';
  $('#result_search').append(pencarian_hasil);
  if (unit == 0) {
    var table = $("#result_search").DataTable({
      retrieve: true,
      pagin: false,
      aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
      ajax: {
        "url": "<?= base_url('Ps/search') ?>",
        "type": "POST",
        "data": {
          "number": number,
          "ata"   : ata,
          "evaluation": evaluation,
          "operator": operator,
          "actype": actype,
          "acreg": acreg,
          "keyword": keyword,
          "uic": uic,
          "crossreff": crossreff,
          "pic": pic,
          "date_to": date_to,
          "date_from": date_from,
          "alerttype": alerttype,
          "status": status,
          "problem_title": problem_title,
        },
      },
      'columns': [{
        data: 'no'
      },
      {
        data: 'number'
      },
      {
        data: 'evaluationtype'
      },
      {
        data: 'actype'
      },
      {
        data: 'problemtitle'
      },   
      {
        data: 'PIC'
      },
      {
        data: 'status'
      },
      {
        data: 'action2'
      }
      ]
    });


  }else {
    var table = $("#result_search").DataTable({
      retrieve: true,
      pagin: false,
      aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
      ajax: {
        "url": "<?= base_url('Ps/search') ?>",
        "type": "POST",
        "data": {
          "number": number,
          "ata"   : ata,
          "evaluation": evaluation,
          "operator": operator,
          "actype": actype,
          "acreg": acreg,
          "keyword": keyword,
          "uic": uic,
          "crossreff": crossreff,
          "pic": pic,
          "date_to": date_to,
          "date_from": date_from,
          "alerttype": alerttype,
          "status": status,
          "problem_title": problem_title,
        },
      },
      'columns': [{
        data: 'no'
      },
      {
        data: 'number'
      },
      {
        data: 'evaluationtype'
      },
      {
        data: 'actype'
      },
      {
        data: 'problemtitle'
      },   
      {
        data: 'PIC'
      },
      {
        data: 'status'
      },
      {
        data: 'action1'
      }
      ]
    });

  } 


})

 $(document).on("click", "#view_p", function () {
  CKEDITOR.replace( 'problem_analisis_v' );
  CKEDITOR.replace( 'problem_description_V' );
  $("#no_v_0").text("");
  $("#beda").html("");
  $("#solution_v_0").text("");
  $("#target_date_v_0").text("");
  $("#status_v_0").text("");
  $("#no_v_1").text("");
  $("#solution_v_1").text("");
  $("#target_date_v_1").text("");
  $("#status_v_1").text("");
  $("#no_v_2").text("");
  $("#solution_v_2").text("");
  $("#file_v_0").text("");
  $("#file_v_1").text("");
  $("#file_v_2").text("");
  $("#file_v_3").text("");
  $("#file_v_4").text("");
  $("#file_v_5").text("");
  $("#file_v_6").text("");
  $("#file_v_7").text("");
  $("#file_v_8").text("");
  $("#file_v_9").text("");
  $("#target_date_v_2").text("");
  $("#status_v_2").text("");
  $("#no_v_3").text("");
  $("#solution_v_3").text("");
  $("#target_date_v_3").text("");
  $("#status_v_3").text("");
  $("#no_v_4").text("");
  $("#no_v_5").text("");
  $("#no_v_6").text("");
  $("#no_v_7").text("");
  $("#no_v_8").text("");
  $("#no_v_9").text("");
  $("#solution_v_4").text("");
  $("#solution_v_5").text("");
  $("#solution_v_6").text("");
  $("#solution_v_7").text("");
  $("#solution_v_8").text("");
  $("#solution_v_9").text("");
  $("#target_date_v_4").text("");
  $("#target_date_v_5").text("");
  $("#target_date_v_6").text("");
  $("#target_date_v_7").text("");
  $("#target_date_v_8").text("");
  $("#target_date_v_9").text("");
  $("#status_v_4").text("");
  $("#status_v_5").text("");
  $("#status_v_6").text("");
  $("#status_v_7").text("");
  $("#status_v_8").text("");
  $("#status_v_9").text("");
  $("#remarks_v_0").text("");
  $("#remarks_v_1").text("");
  $("#remarks_v").text("");
  $("#remarks_v_2").text("");
  $("#remarks_v_3").text("");
  $("#remarks_v_4").text("");
  $("#remarks_v_5").text("");
  $("#remarks_v_6").text("");
  $("#remarks_v_7").text("");
  $("#remarks_v_8").text("");
  $("#remarks_v_9").text("");
  $("#tampung").html("");
  var number = $(this).data('number');
  var crossreff = $(this).data('cross');
  var problemanalisis = $(this).data('problemanalisis');
  var actype = $(this).data('actype');
  var operator = $(this).data('operator');
  var acreg = $(this).data('acreg');
  var eval = $(this).data('eval');
  var alert = $(this).data('alert');
  var ata = $(this).data('ata');
  var systems = $(this).data('systems');
  var problemtitle = $(this).data('problemtitle');
  var detailproblem = $(this).data('detailproblem');
  var file = $(this).data('file');
  var cc = $(this).data('cc');
  var status = $(this).data('status');
  var solution0 = $(this).data('solution0');
  var solution1 = $(this).data('solution1');
  var solution2 = $(this).data('solution2');
  var solution3 = $(this).data('solution3');
  var solution4 = $(this).data('solution4');
  var solution5 = $(this).data('solution5');
  var solution6 = $(this).data('solution6');
  var solution7 = $(this).data('solution7');
  var solution8 = $(this).data('solution8');
  var solution9 = $(this).data('solution9');
  var remarks = $(this).data('remarks');
  var rl = '<?= base_url('/report?n_idee=') ;?>';
  CKEDITOR.instances['problem_description_V'].setData($(this).data('detailproblem'));
  CKEDITOR.instances['problem_analisis_v'].setData($(this).data('problemanalisis'));
  $(".modal-body #number_v").val( number );
  $("#pdf_all").attr("href", rl+number);
  $('.modal-body #pic_v').val($(this).data('pic'));
  $('.modal-body #uic_v').val($(this).data('uic'));
  $(".modal-body #crossreff_v").val( crossreff );
  $(".modal-body #ata_v").val( ata );
  $(".modal-body #eval_v").val( eval );
  $(".modal-body #operator_v").val( operator );
  $(".modal-body #acreg_v").val( acreg );
  $(".modal-body #actype_v").val( actype );
  $(".modal-body #alert_sri").val( alert );
  $(".modal-body #systems_v").val( systems );
  $(".modal-body #problem_title_v").val( problemtitle );
  $(".modal-body #remarks_v").prepend( remarks );
  if ($(this).data('status') == 'OPEN') {
    $('.modal-body #status_v').val($(this).data('status'));
    $('.modal-body #status_v').addClass('btn btn-success');
  } else if ($(this).data('status') == 'PROGRESS') {
    $('.modal-body #status_v').val($(this).data('status'));
    $('.modal-body #status_v').addClass('btn btn-warning');
  } else if ($(this).data('status') == 'REVIEW') {
   $('.modal-body #status_v').val($(this).data('status'));
   $('.modal-body #status_v').addClass('btn btn-success');
 } else if ($(this).data('status') == 'CLOSE') {
  $('.modal-body #status_v').val($(this).data('status'));
  $('.modal-body #status_v').addClass('btn btn-primary');
}
var file = $(this).data('file');
if (file !== '') {
 var arr_file = file.split(",");
 var count_arr_file= arr_file.length;
 var rl='<?= base_url('files/');?>';
 for(var o = 0; o < count_arr_file; o++){
   var token = arr_file[o];
   $.ajax({
    type:"POST",
    data:{token:token},
    url:"<?= base_url('pd/searchfile') ?>",
    dataType: 'json',
    success: function(data){

      var file_name = data.nama_file;
      var file_baru = data.nama_baru;

       if (file_baru == null) {
      var io = '<li><a href="'+rl+file_name+'" id= "soe" target ="_blank" class="badge badge-success">'+file_name+'</a></li>';  
      } else {
        var io = '<li><a href="'+rl+file_baru+'" id= "soe" target ="_blank" class="badge badge-success">'+file_name+'</a></li>';
      }
    
      var ooi = '</br>';
      $('#beda').append(io);
      $('#beda').append(ooi);

    },
  });
 }
}



for (var i = 0; i < 9; i++) {
 var b = $(this).data('solution'+i);

 if (b !== '') {
  var w = 1 ;
  $.ajax({
    type:"post",
    data:{solution:b},
    url:"<?= base_url('pd/searchsolution') ?>",
    dataType: 'json',
    success: function(data){ 
     var sl   = 'solution'+w;
     var no   = '#no_v_'+w;
     var nsl  = '#solution_v_'+w;
     var tgtd = '#target_date_v_'+w;
     var stst = '#status_v_'+w;
     var rmks = '#remarks_v_'+w;
     var fil  = '#file_v_'+w;
     $(no).append(w);
     $(nsl).append(data[0]['solution']);
     $(tgtd).append(data[0]['uic']);
     $(stst).append(data[0]['status']);
     $(rmks).append(data[0]['remarks']);
     $(fil).append(data[0]['file']);
     w ++;
   },
 });
}
}             
});
</script>

<script>
  Dropzone.autoDiscover = false;
  var fileList = new Array;
  var i =0;
  var o; 
  var rl='<?= base_url('files/');?>';
  var foto_upload= new Dropzone("#zone_upload",{
    url: "<?= base_url('pe/proses_upload') ?>",
    maxFilesize: 4,
    method:"post",
    paramName:"userfile",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
      this.on("success", function(file, serverFileName,responseURL) {
        var l = 0;
        var i =  responseURL.currentTarget;
        var b = i.response;
        var c = JSON.parse(b);
        var d = c.nama_awal;
        var e = c.nama_baru;
        var k;
        fileList[l] = {"nama_file" : d, "fileId" : l, "nama_filebaru" : e};
        l++;
        for (k in fileList)
        {
          var hasil = fileList[k]['nama_file'];
          var hasil_akhir = fileList[k]['nama_filebaru'];
          var stre121 = '<li id="'+hasil+'"><a class="badge badge-success" target="_blank"  href='+rl+hasil_akhir+' >'+hasil+'</a></li></br>';
          $("#tampung").append(stre121);
          fileList.pop();
        } 

      });
    },
  });

  foto_upload.on("sending",function(a,b,c){
   a.token=Math.random();
   a.number= $(".modal-body #number_p").val();
   c.append("token_file",a.token);
   c.append("number",a.number);
   fileList.pop();
 })
  foto_upload.on("removedfile",function(a){
    var token=a.token;
    $.ajax({
      type:"post",
      data:{token:token},
      url:"<?= base_url('pe/remove_file') ?>",
      cache:false,
      dataType: 'json',
      success: function(data){
       var rm = document.getElementById(data);
       $(rm).remove();
     }
   });
  }); 

  $(document).on("click", "#opopop", function () {
    var number = $(this).data('unik1');
    var file = $(this).data('unik2');
    var ui = $(this).data('delete');
    $.ajax({
     type:"POST",
     data:{number:number,file:file,ui:ui},
     url:"<?= base_url('Ps/deleteattach') ?>",
     dataType: 'json',
     success: function(data){
      var sao = document.getElementById(data);
      $(sao).remove();
    }
  });
  });

  $(document).on("click", "#point_of", function () {
    var number = $(this).data('unik1');
    var file = $(this).data('unik2');
    var ui = $(this).data('delete');
    $.ajax({
     type:"POST",
     data:{number:number,file:file,ui:ui},
     url:"<?= base_url('Ps/deleteattachsolution') ?>",
     dataType: 'json',
     success: function(data){
      var sao = document.getElementById(data);
      $(sao).remove();
    }
  });
  });


    $(document).on('keypress',function(e) {
    if(e.which == 13) { 
     if ($.fn.dataTable.isDataTable('#result_search')) {
      var table = $("#result_search").DataTable();
      table.destroy();
    }
    
    $('#result_search').html('');
    var number = $('#number').val();
    var crossreff = $('#crossreff').val();
    var unit = $('#unit').val();
    var evaluation = $('#evaluation').val();
    var alerttype = $('#alert').val();
    var operator = $('#operator2').val();
    var actype = $('#actype').val();
    var acreg = $('#acreg').val();
    var uic = $('#uic').val();
    var problem_title = $('#problem_title').val();
    var status = $('#status').val();
    var pic = $('#pic').val();
    var keyword = $('#keyword').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var ata     = $('#ata2').val();
    var html = '';
    var pencarian_hasil = '<thead id ="head_cek">' +
    '<th class="text-center">NO</th>' +
    '<th class="text-center">NUMBER</th>' +
    '<th class="text-center">EVALUATION TYPE</th>' +
    '<th class="text-center">A/C TYPE</th>' +
    '<th class="text-center">PROBLEM TITLE</th>' +
    '<th class="text-center">PIC</th>' +
    '<th class="text-center">STATUS</th>' +
    '<th class="text-center">ACTION</th>' +
    '</tr></thead>' +
    '<tbody id="enakeun"></tbody>';
    $('#result_search').append(pencarian_hasil);
    if (unit == 0) {
      var table = $("#result_search").DataTable({
        retrieve: true,
        pagin: false,
        aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
          "url": "<?= base_url('Ps/search') ?>",
          "type": "POST",
          "data": {
            "number": number,
            "ata"   : ata,
            "evaluation": evaluation,
            "operator": operator,
            "actype": actype,
            "acreg": acreg,
            "keyword": keyword,
            "uic": uic,
            "crossreff": crossreff,
            "pic": pic,
            "date_to": date_to,
            "date_from": date_from,
            "alerttype": alerttype,
            "status": status,
            "problem_title": problem_title,
          },
        },
        'columns': [{
          data: 'no'
        },
        {
          data: 'number'
        },
        {
          data: 'evaluationtype'
        },
        {
          data: 'actype'
        },
        {
          data: 'problemtitle'
        },   
        {
          data: 'PIC'
        },
        {
          data: 'status'
        },
        {
          data: 'action2'
        }
        ]
      });


    }else {
      var table = $("#result_search").DataTable({
        retrieve: true,
        pagin: false,
        aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
          "url": "<?= base_url('Ps/search') ?>",
          "type": "POST",
          "data": {
            "number": number,
            "ata"   : ata,
            "evaluation": evaluation,
            "operator": operator,
            "actype": actype,
            "acreg": acreg,
            "keyword": keyword,
            "uic": uic,
            "crossreff": crossreff,
            "pic": pic,
            "date_to": date_to,
            "date_from": date_from,
            "alerttype": alerttype,
            "status": status,
            "problem_title": problem_title,
          },
        },
        'columns': [{
          data: 'no'
        },
        {
          data: 'number'
        },
        {
          data: 'evaluationtype'
        },
        {
          data: 'actype'
        },
        {
          data: 'problemtitle'
        },   
        {
          data: 'PIC'
        },
        {
          data: 'status'
        },
        {
          data: 'action1'
        }
        ]
      });

    } 
  }
});
</script>


