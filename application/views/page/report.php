<?php
$get = $_GET['n_idee'];

$data = $this->Getdat_model->getnumber($get);
if (empty($data['acreg'])) {
	$acreg = '-';
} else {
	$acreg = $data['acreg'];
}
if (empty($data['alert'])) {
	$alert = '-'; 
} else {
	$alert = $data['alert'];
}
if (empty($data['crossreff'])) {
	$cross = '-';
} else {
	$cross = $data['crossreff'];
}
$temp_pic = $data['PIC'];
$new_s = rtrim($temp_pic,',');
$arr = explode(',', $new_s);

for ($i=0; $i < count($arr) ; $i++) { 
	$has = cek_email($arr[$i]).',';
	$hasil[]= $has;
	$kalimat = implode(" ",$hasil);
	$new = rtrim($kalimat,',');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Report Problem </title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?= base_url()?>assets/css/bootstrap.min.css" media="print" rel="stylesheet">
	<link rel="icon" type="image/png" href="<?= base_url()?>assets/images/icon.png">
	<style>
		.di{
			margin-left: -12px;
			margin-right: -12px;
		}
		#box{
			padding: 4px;
			border : 1px solid black;
		}
		.tulisan {
			font-size: 13px;
		} 
		#text{
			padding: 10px;
		}
	  #tu{
	  	font-size: 24px;
	  	font-weight: bold;
	  }
	</style>
</head>
<body>
	<div class="container">
		<h3 id="tu" class="text-center"><b>INTEGRATED DATABASE OF ENGINEERING EVALUATION</b></h3>
		<h4 class="text-center"><b>PROBLEM TITLE : <?= $data['problemtitle'] ?></b></h4>
		<h5 class="text-center"><b>PRINTED BY  : <?= $this->session->userdata('nama') ?></b></h5>
	</div>
	<br>
	<div class="di">
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="13%" id="box"><span class="tulisan">NUMBER </span></th>
					<td width="15%" style="border: 1px solid black;" class="text-center"><span><?= $data['number'] ?></span></td>
					<th style="border: 1px solid black;" width="13%" id="box"><span class="tulisan">CROSS REFF </span></th>
					<td width="13%" style="border: 1px solid black;" class="text-center"><span><?= $cross ?></span></td>
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">EVALUATION TYPE </span></th>
					<td width="14%" style="border: 1px solid black;" class="text-center"><span><?= $data['evaluationtype'] ?></span></td>
					<th style="border: 1px solid black;" width="10%" id="box"><span class="tulisan">ALERT TYPE</span></th>
					<td style="border: 1px solid black;" class="text-center"><span><?= $alert ?></span></td>
				</tr>
			</thead>
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="13%" id="box"><span class="tulisan">OPERATOR </span></th>
					<td width="15%" style="border: 1px solid black;" class="text-center"><span><?= cekoperator($data['operator']) ?></span></td>
					<th style="border: 1px solid black;" width="13%" id="box"><span class="tulisan">A/C TYPE</span></th>
					<td width="13%" style="border: 1px solid black;" class="text-center"><span><?= cekactype($data['actype']) ?></span></td>
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">A/C REG</span></th>
					<td width="14%" style="border: 1px solid black;" class="text-center"><span><?= $acreg ?></span></td>
					<th style="border: 1px solid black;" width="10%" id="box"><span class="tulisan">ATA</span></th>
					<td style="border: 1px solid black;" class="text-center"><span><?= $data['ata'] ?></span></td>
				</tr>
			</thead>
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">PROBLEM TITLE </span></th>
					<td width="57%" style="border: 1px solid black;" class="text-center"><span><?= html_entity_decode($data['problemtitle']) ?></span></td>
					<th style="border: 1px solid black;" width="15%" id="box"><span class="tulisan">SYSTEMS</span></th>
					<td width="22%" style="border: 1px solid black;" class="text-center"><span><?= $data['systems'] ?></span></td>
				</tr>
			</thead>
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">PIC </span></th>
					<td width="57%" style="border: 1px  solid black;" class="text-center"><div style="padding-top:100px;">
						<p style="padding-top:100px;">
							<?= $new ?>
						</p>
					</div></td>
					<th style="border: 1px solid black;" width="15%" id="box"><span class="tulisan">UIC</span></th>
					<td width="22%" style="border: 1px solid black;" class="text-center"><span><?= $data['UIC'] ?></span></td>
				</tr>
			</thead>
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">REMARKS </span></th>
					<td width="57%" style="border: 1px  solid black;" class="text-center"><?= $data['remarks'] ?></td>
					<th style="border: 1px solid black;" width="15%" id="box"><span class="tulisan">STATUS</span></th>
					<td width="22%" style="border: 1px solid black;" class="text-center"><span><?= $data['status'] ?></span></td>
				</tr>
			</thead>
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" width="14%" id="box"><span class="tulisan">PROBLEM DETAIL </span></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-justify" style="border: 1px solid black;"id="box">
						<?= html_entity_decode($data['detailproblem']) ?>
					</td>
				</tr>
			</tbody>
			
		</table>
		<table style="border: 1px solid black;" class="table table-bordered">
			<thead> 
				<tr style="border: 1px solid black;" >
					<th style="border: 1px solid black;" id="box"><span class="tulisan">PROBLEM ANALYSIS </span></th>
				</tr>
			</thead>
			<tbody>
				<tr >
					<td class="text-justify" style="border: 1px solid black;"id="box">
						<?= html_entity_decode($data['problemanalisis']) ?>
					</td>

				</tr>
			</tbody>
		</table>

		<?php for ($i=0; $i < 10; $i++) {  ?>
			<?php if (!empty($data['solution'.$i])) : ?>
				<?php 
				$solusiKeberapa = "Solution".$i;
				$row = $this->Getdat_model->join($solusiKeberapa,$data['solution'.$i]);
				?>
				<table style="border: 1px solid black;" class="table table-bordered">
					<thead> 
						<tr style="border: 1px solid black;" >
							<th  style="border: 1px solid black;"id="box"><span class="tulisan">SOLUTIONS </span></th>
							<th class="text-center" width="12%" style="border: 1px solid black;"id="box"><span class="tulisan">UIC</span></th>
							<th class="text-center" width="10%" style="border: 1px solid black;"id="box"><span class="tulisan">STATUS (%)</span></th>	
							<th class="text-center" width="18%" style="border: 1px solid black;"id="box"><span class="tulisan">REMARKS</span></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td  class="text-justify" style="border: 1px solid black;"id="box">
								<?= $row['solution'] ?>
							</td>
							<td class="text-center" style="border: 1px solid black;"id="box"><?= $row['uic'] ?></td>
							<td class="text-center" style="border: 1px solid black;"id="box"><?= $row['status'] ?> %</td>
							<td class="text-center" style="border: 1px solid black;"id="box"><?= $row['remarks'] ?> </td>
						</tr>
					</tbody>
				</table>
			<?php endif; ?>
		<?php } ?>


	</div>
</body>
</html>