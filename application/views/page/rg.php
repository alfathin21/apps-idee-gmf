<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-users"></i>&nbsp; ADMINISTRATOR / ROLE AND USER CONTROL</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-md-4">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modal_add_role"><i class="fa fa-gears"></i> &nbsp;ADD NEW ROLE</button>
            <hr>
            <table class = "table table-hover table-striped" id="role">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Role Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="show_role"></tbody>
            </table>
          </div>
          <div class="col-md-8">
            <button class="btn btn-success" data-toggle="modal" data-target="#modal_add"><i class="fa fa-users"></i> &nbsp;ADD NEW USER</button>
            <hr>
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hovered" id="srw">
                <thead>
                  <tr>
                    <th class="text-center" width="10px">NO</th>
                    <th width="80px" class="text-center">USERNAME</th>
                    <th class="text-center">NAMA</th>
                    <th  width="15px" class="text-center">ROLE</th>
                    <th width="15px" class="text-center">ACTION</th>
                  </tr>
                </thead>
                <tbody id="show_data"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



</div>







</div>
<!-- akhir modal -->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-users"></i> &nbsp; REGISTRASI USER</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">USERNAME : </label>
          <input type="text" class="form-control" id="username" autocomplete="off" >
        </div>
        <div class="form-group">
          <label for="">PASSWORD : </label>
          <input type="password" class="form-control" id="password" autocomplete="off">
        </div>
        <div class="form-group">
         <label for="">NAMA : </label>
         <input type="text" class="form-control" id="name" autocomplete="off" >
       </div>
       <div class="form-group">
        <label for="">E-MAIL : </label>
        <input type="text" class="form-control" id="email" autocomplete="off">
      </div>
     <!--  <div class="form-group">
        <label for="">JABATAN : </label>
        <input type="text" class="form-control" id="jabatan" autocomplete="off">
      </div> -->
      <div class="form-group">
        <label for="">CUSTOMER : </label>
        <select class="form-control" name="customer" id="customer">
         <option selected="">Select Customer</option>
         <option value="GA">GARUDA</option>
         <option value="QG">CITILINK</option>
         <option value="SJ">SRIWIJAYA</option>
         <option value="IN">NAM AIR</option>
         <option value="OT">OTHERS</option>
       </select>
     </div>
     <div class="form-group">
      <label for="">ROLE : </label>
      <select name="role" id="role_id" class="form-control">
        <option selected="">Select Role</option>
        <?php foreach ($role as $key ): ?>
          <option value="<?= $key['id'] ?>"><?= $key['role'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary"  id="save_user"><i class="fa fa-arrow-right"></i> SAVE USER</button>
  </div>
</form>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->









<!-- Modal Add Role -->
<!-- akhir modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-users"></i> &nbsp; ADD NEW ROLE</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">USERNAME : </label>
          <input type="hidden" disabled="" id="id_user" class="form-control" id="username2" autocomplete="off" >
          <input type="text" disabled="" class="form-control" id="username2" autocomplete="off" >
        </div>
        <div class="form-group">
         <label for="">NAMA : </label>
         <input type="text" disabled="" class="form-control" id="name2" autocomplete="off" >
       </div>
       <div class="form-group">
        <label for="">E-MAIL : </label>
        <input type="text" disabled="" class="form-control" id="email2" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="">JABATAN : </label>
        <input type="text" disabled="" class="form-control" id="jabatan2" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="">ROLE : </label>
        <select name="role"  id="role_id2" class="form-control">
          <option selected="">Select Role</option>
          <?php foreach ($role as $key ): ?>
            <option value="<?= $key['id'] ?>"><?= $key['role'] ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn btn-success" id="update_user"><i class="fa fa-arrow-right"></i> Update User</button>
    </div>
  </form>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Modal Add Role -->

<script type="text/javascript">
  var table;

  $(document).ready(function() {
    tampil_data_role();
   
    table = $('#srw').DataTable({ 

      "processing": true, 
      "serverSide": true, 

      "order": [], 

      "ajax": {
        "url": "<?= ('role/get_data_user')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ 0 ], 
        "orderable": false, 
      },
      ],
      "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]

    });

  });


  $('#username').on('change', function(){
    var username = $('#username').val();
    $.ajax({
      url: "<?= site_url('role/cekusername'); ?>",
      type: "POST",
      dataType: "JSON",
      data: {username: username},
      success: function (data) {
        if (data == 1) {
          alert('Username is already registered in the Database, Please change Username !');
          $('#username').val('');
          $('#username').focus();

        }
      },
    });
  })

  $('#save_user').on('click', function(){
    var username = $('#username').val();
    var jabatan = $('#jabatan').val();
    var name = $('#name').val();
    var password = $('#password').val();
    var customer = $('#customer').val();
    var role = $('#role_id').val();
    var email = $('#email').val();
    $.ajax({
      url: "<?= site_url('role/reg'); ?>",
      type: "POST",
      dataType: "JSON",
      data: {username: username,password: password,customer: customer,role: role,email: email,name: name,jabatan: jabatan},
      success: function (data) {
        $('#username').val("");
        $('#password').val("");
        $('#jabatan').val("");
        $('#name').val("");
        $('#name').val("");
        $('#email').val("");
        $("#role_id").html("<option value=''>-- No role selected --</option>");
        $("#customer").html("<option value=''>-- No customer selected --</option>");
        tampil_data_user();

      },
    });
  })
  $('#save_role').on('click', function(){
    var role = $('#add_role').val();
    $.ajax({
      url: "<?= site_url('Role/save_role'); ?>",
      type: "POST",
      dataType: "JSON",
      data: {role: role},
      success: function (data) {
        $('#add_role').val("");
        tampil_data_role();
      },
    });
  })

  $(function(){
    $("#myModal").on("show.bs.modal", function (event) {
      var button = $(event.relatedTarget);
      var nopeg = button.data("nopeg");
      var nama = button.data("nama");
      var email = button.data("email");
      var jabatan = button.data("jabatan");
      var role_id = button.data("role_id");
      var id = button.data("id");
      var modal = $(this);
      modal.find("#username2").val(nopeg);
      modal.find("#name2").val(nama);
      modal.find("#email2").val(email);
      modal.find("#jabatan2").val(jabatan);
      modal.find("#role_id2").val(role_id);
      modal.find("#id_user").val(id);

    });
  });


  $('#update_user').on('click', function(){
    var nopeg =  $('#username2').val();
    var role_id =  $('#role_id2').val();
    $.ajax({
      url: "<?= site_url('Role/update_user'); ?>",
      type: "POST",
      dataType: "JSON",
      data: {role_id: role_id,nopeg:nopeg},
      success: function (data) {
        document.location.href = '<?= base_url('role')?>'; 
      },
    });
  })
  function tampil_data_role(){
    $.ajax({
      type  : 'ajax',
      url   : '<?= base_url()?>/role/list_role',
      async : false,
      dataType : 'json',
      success : function(data){
        var html = '';
        var i;
        var no=1;
        for(i=0; i<data.length; i++){
          html += '<tr>'+
          '<td>'+no+'</td>'+
          '<td>'+data[i].role+'</td>'+
          '<td><a href="<?= base_url('access')?>?role_id= '+data[i].id+' "  class="badge badge-warning">Access</a>&nbsp;<a href="<?= base_url('access/delete')?>?role_id= '+data[i].id+' " class="badge badge-danger">Delete</a></td>'+
          '</tr>';
          no++;
        }
        $('#show_role').html(html);
      }
    });
  }


</script>


<!--  -->
<!-- /page content -->





