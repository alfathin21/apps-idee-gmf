<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">

          <h5><i class="fa fa-unlock-alt"></i>&nbsp; ROLE ACCESS : <?= $role['role'];?></h5>

          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
         <div class="row">
          <div class="col-md-12">
              <a href="<?= base_url('role') ?>" class="btn btn-sm btn-success"><i class="fa fa-reply"></i> Back </a>
            <table class = "table table-hover table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Menu</th>
                  <th>Akses</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1; 
                foreach ($menu as $key ) : ?>
                  <tr>
                    <td><?= $no; ?></td>
                    <td><?= $key['menu']; ?></td>
                    <td>
                     <div class="form-check">
                      <input
                      data-role = "<?= $role['id']; ?>"
                      data-menu= "<?= $key['id']; ?> "
                      type="checkbox" <?= akses_check($role['id'] ,  $key['id']); ?>  class="form-check-input">
                    </div>

                  </td>
                </tr>
                <?php 
                $no++;
              endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>




<script>
$('.form-check-input').on('click', function() {
  const menuId = $(this).data('menu');
  const roleId = $(this).data('role');
  $.ajax({
    url:"<?= base_url('access/ajax') ;?>",
    type:'post',
    data: {
      menuId: menuId,
      roleId: roleId
    },
    success: function(){
    document.location.href = '<?= base_url('access?role_id=');?>' + roleId; 
    }
  });
});
</script>















<!--  -->
<!-- /page content -->





