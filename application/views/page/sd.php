<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query">
        <div class="ibox-title">
          <h5><i class="fa fa-laptop"></i>&nbsp; DISPLAY STATUS / DISPLAY DETAIL </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
              <label for="">EVALUATION TYPE :</label>
              <select class="form-control select4" name="evaluation" id="evaluation">
                <option selected=""></option>
                <?php foreach ($evaltype as $key ) :?>
                  <option value="<?= $key['evaltype']?>"><?= $key['evaltype']?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label for="">OPERATOR : </label>
              <select class="form-control" id="operator2" name="operator2">
                <?php if ($this->session->userdata('unit') == 'GA') : ?>
                  <option selected="" readonly value="GA">GARUDA INDONESIA</option>
                  <?php elseif ($this->session->userdata('unit') == 'QG') : ?>
                    <option selected="" readonly value="QG">CITILINK</option> 
                    <?php elseif ($this->session->userdata('unit') == 'IN') : ?>
                      <option selected="" readonly value="IN">NAM AIR</option>
                      <?php elseif ($this->session->userdata('unit') == 'SJ') : ?>
                        <option selected="" readonly value="SJ">SRIWIJAYA</option>
                        <?php else : ?>
                          <option value=""></option>
                          <?php foreach ($operator as $key ) :?>
                            <option value="<?= $key['value']?>"><?= $key['operator']?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                    </div>
                    <div class="col-lg-4 col-md-4" id="status">
                      <label for="">STATUS <span style="color: red">*</span> :</label>
                      <br>
                      <label>
                        <input type="radio" name="depir" value="delay" id="radio_delay" onclick="check(this.value)"> <label class="input">Evaluation Status</label>
                        <label>
                          <input type="radio" name="depir" value="pirep" id="radio_pirep" onclick="check(this.value)"> <label class="input">Solution Status</label>
                        </div>

                      </div>
                      <br>
                      <div class="row">
        <!--     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <label for="">ALERT TYPE SRI : </label>
              <select class="form-control" name="alert" id="alert">
                <option selected=""></option>
                <option value="ALD">ALD</option>
                <option value="ALP">ALP</option>
                <option value="ALM">ALM</option>
                <option value="ALC">ALC</option>
                <option value="SDR">SDR</option>
                <option value="ACCDNT">ACCDNT</option>
                <option value="ENV">ENV</option>
                <option value="FTG">FTG</option>
                <option value="EXT REP">EXT REP</option>
                <option value="OPS">OPS</option>
              </select>
            </div> -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <label for="">PIC : </label>
              <br>
              <select required="" name="pc[]" id="pc" class="form-control"></select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <label for="">A/C TYPE : </label>
              <select id="actype" class="form-control selek2" name="actype">
                <option selected=""></option>
                <?php foreach ($actype as $key ) :?>
                  <option value="<?= $key['value']?>"><?= $key['actype']?></option>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="">UIC : </label>
              <select class="form-control Operator" id="uic" name="uic">
                <option></option>
                <?php foreach ($uic as $key) :?>
                  <option value="<?= $key['uic'] ?>"><?= $key['uic'] ?></option>
                <?php endforeach ;?>
              </select>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <label for="">ON WATCH : </label>
              <select class="form-control" id="onwatch" name="onwatch">
                <option value=""></option>
                <option value="YES">YES</option>
                <option value="NO">NO</option>
              </select>
            </div>


          </div>
          <br>
          <div class="row">


<!--             <div class="col-md-3 col-sm-6 col-xs-12 ">
              <label for="">CROSS REF :</label>
              <input type="text" class="form-control " id="crossreff" name="crossreff" placeholder="Insert Cross Reff">
            </div>
          -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <label for="">DATE FROM :</label>
            <input type="date" class="form-control" id="date_from" value="<?= '2019-01-01' ?>" name="date_from" placeholder="Revise Targe">
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <label for="">DATE TO : </label>
            <input type="date" class="form-control" id="date_to" value="<?='2019-12-31'?>" name="date_to">
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <label for="">KEYWORD : </label>
            <input type="hidden" id="role_id" value="<?= $this->session->userdata('unit') ?>">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword">
          </div>
          <div class="col-md-3">
            <br>
            <button style="margin-top: 5px;" id="display" type="submit" class="btn btn-primary"><i class="fa fa-desktop"></i>&nbsp; DISPLAY</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-bar-chart-o"></i>&nbsp; SEARCH RESULT DISPLAY STATUS DETAIL</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline table-responsive" id="result">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="source"></table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script>
  $(document).on('keypress',function(e) {
    if(e.which == 13) { 
     if ($.fn.dataTable.isDataTable('#source')) {
      var table = $("#source").DataTable();
      table.destroy();
    }

    $('#source').html('');
    var number = $('#number').val();
    var evaluation = $('#evaluation').val();
    var operator = $('#operator2').val();
    var actype = $('#actype').val();
    var keyword = $('#keyword').val();
    var crossreff = $('#crossreff').val();
    var pic = $('#pc').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var uic = $('#uic').val();
    var alerttype = $('#alert').val();
    var role_id = $('#role_id').val();
    var ours = [];
    var url = "<?= base_url('/sd/ec') ?>";
    var html = '';
    var no = 1;
    var output_component;
    var onwatch = $('#onwatch').val();
    var evaluation_status = '<thead id ="head_cek">' +
    '<th class="text-center">No</th>' +
    '<th class="text-center">Number</th>' +
    '<th class="text-center">Cross Reff</th>' +
    '<th class="text-center">A/C TYPE</th>' +
    '<th class="text-center">ATA</th>' +
    '<th class="text-center">Evaluation Type</th>' +
    '<th class="text-center">Assigment Month</th>' +
    '<th class="text-center">Problem Title</th>' +
    '<th class="text-center">Sri Effectiveness Review</th>' +
    '<th class="text-center">PIC</th>' +
    '<th class="text-center">Target Date</th>' +
    '<th class="text-center">Status</th>' +
    '<th class="text-center">Remarks</th>' +
    '<th class="text-center">Attach Doc</th>' +
    '</tr></thead>' +
    '<tbody id="enakeun"></tbody>';
    var solution_status = '<thead id ="head_cek">' +
    '<th class="text-center">No</th>' +
    '<th class="text-center">Number</th>' +
    '<th class="text-center">Cross Reff</th>' +
    '<th class="text-center">A/C TYPE</th>' +
    '<th class="text-center">ATA</th>' +
    '<th class="text-center">Evaluation Type</th>' +
    '<th class="text-center">Assigment Month</th>' +
    '<th class="text-center">Problem Title</th>' +
    '<th class="text-center">PIC</th>' +
    '<th class="text-center">SOLUTION</th>' +
    '<th class="text-center">UIC</th>' +
    '<th class="text-center">% Compl(current)</th>' +
    '<th class="text-center">Last Update</th>' +
    '<th class="text-center">Attach Doc</th>' +
    '<th class="text-center">Remarks</th>' +
    '</tr></thead>' +
    '<tbody id="enakeun"></tbody>';
    if (!document.getElementById("radio_delay").checked & !document.getElementById("radio_pirep").checked) {
      alert('Please select status !');
      return false;
    }
    if (document.getElementById("radio_delay").checked) {
      var output_component = 'evaluation_status';
      $('#source').append(evaluation_status);
    } else if (document.getElementById("radio_pirep").checked) {
      var output_component = 'solution_status';
      $('#source').append(solution_status);
    }
    if (output_component == 'evaluation_status') {
      var table = $("#source").DataTable({
        retrieve: true,
        pagin: false,
        //ajax with data to post
        dom: 'Bfrtip',
        buttons: [
        {extend: 'excel', title: 'Display Evaluation Status'},
        {extend: 'pdf', title: 'Display Evaluation Status', orientation: 'landscape',
        pageSize: 'A3'}
        ],
        aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
          "url": "<?= base_url('Sd/search_evaluation') ?>",
          "type": "POST",
          "data": {
            "number": number,
            "evaluation": evaluation,
            "operator": operator,
            "actype": actype,
            "keyword": keyword,
            "uic": uic,
            "crossreff": crossreff,
            "pic": pic,
            "date_to": date_to,
            "date_from": date_from,
            "alerttype": alerttype,
            "role_id": role_id,
            "ours": ours,
            "url": url,
            "onwatch": onwatch,
            "output_component": output_component,
          },
        },

        'columns': [{
          data: 'no'
        },
        {
          data: 'number'
        },
        {
          data: 'crossreff'
        },
        {
          data: 'actype'
        },
        {
          data: 'ata'
        },   
        {
          data: 'evaluationtype'
        },
        {
          data: 'EvalDate'
        },
        {
          data: 'problemtitle'
        },
        {
          data: 'srieffectivenees'
        },
        {
          data: 'PIC'
        },
        {
          data: 'TargetDate'
        },
        {
          data: 'status'
        },
        {
          data: 'remarks'
        },
        {
          data: 'file'
        }
        ]
      });
      $('body').addClass('mini-navbar');
    } else if (output_component == 'solution_status') {
      var table = $("#source").DataTable({
        dom: 'Bfrtip',
        buttons: [
        {extend: 'excel', title: 'Display Status Solution Status'},
        {extend: 'pdf', title: 'Display Status Solution Status', orientation: 'landscape',
        pageSize: 'A3'},
        ],
        aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
          "url": "<?= base_url('sd/search_solution') ?>",
          "type": "POST",
          "data": {
            "number": number,
            "evaluation": evaluation,
            "operator": operator,
            "actype": actype,
            "keyword": keyword,
            "uic": uic,
            "crossreff": crossreff,
            "pic": pic,
            "date_to": date_to,
            "date_from": date_from,
            "alerttype": alerttype,
            "role_id": role_id,
            "ours": ours,
            "url": url,
            "onwatch": onwatch,
            "output_component": output_component,
          },
        },
        'columns': [{
          data: 'no'
        },
        {
          data: 'number'
        },
        {
          data: 'crossreff'
        },
        {
          data: 'actype'
        },
        {
          data: 'ata'
        },
        {
          data: 'evaluationtype'
        },
        {
          data: 'EvalDate'
        },
        {
          data: 'problemtitle'
        },
        {
          data: 'PIC'
        }, 
        {
          data: 'namasolusi'
        },
        {
          data: 'uicsolusi'
        },
        {
          data: 'status_solusi'
        },
        {
          data: 'last_update'
        },
        {
          data: 'fil_sol'
        },
        {
          data: 'remarks_status'
        },
        ],
        'rowsGroup': [0, 1, 2, 3, 4, 5, 6, 7, 8]
      });


      var parsing_url = url + '?evaluation=' + evaluation + '&operator=' + operator + '&act=solusistatus';
      // $('#enakeun').html(html);
      $('body').addClass('mini-navbar');


    }
  }
});


$(document).on('click', "#display", function() {
 if ($.fn.dataTable.isDataTable('#source')) {
  var table = $("#source").DataTable();
  table.destroy();
}

$('#source').html('');
var number = $('#number').val();
var evaluation = $('#evaluation').val();
var operator = $('#operator2').val();
var actype = $('#actype').val();
var keyword = $('#keyword').val();
var crossreff = $('#crossreff').val();
var pic = $('#pc').val();
var date_to = $('#date_to').val();
var date_from = $('#date_from').val();
var uic = $('#uic').val();
var alerttype = $('#alert').val();
var role_id = $('#role_id').val();
var ours = [];
var url = "<?= base_url('/sd/ec') ?>";
var html = '';
var no = 1;
var output_component;
var onwatch = $('#onwatch').val();
var evaluation_status = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Number</th>' +
'<th class="text-center">Cross Reff</th>' +
'<th class="text-center">A/C TYPE</th>' +
'<th class="text-center">ATA</th>' +
'<th class="text-center">Evaluation Type</th>' +
'<th class="text-center">Assigment Month</th>' +
'<th class="text-center">Problem Title</th>' +
'<th class="text-center">Sri Effectiveness Review</th>' +
'<th class="text-center">PIC</th>' +
'<th class="text-center">Target Date</th>' +
'<th class="text-center">Status</th>' +
'<th class="text-center">Remarks</th>' +
'<th class="text-center">Attach Doc</th>' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
var solution_status = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Number</th>' +
'<th class="text-center">Cross Reff</th>' +
'<th class="text-center">A/C TYPE</th>' +
'<th class="text-center">ATA</th>' +
'<th class="text-center">Evaluation Type</th>' +
'<th class="text-center">Assigment Month</th>' +
'<th class="text-center">Problem Title</th>' +
'<th class="text-center">PIC</th>' +
'<th class="text-center">SOLUTION</th>' +
'<th class="text-center">UIC</th>' +
'<th class="text-center">% Compl(current)</th>' +
'<th class="text-center">Last Update</th>' +
'<th class="text-center">Attach Doc</th>' +
'<th class="text-center">Remarks</th>' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
if (!document.getElementById("radio_delay").checked & !document.getElementById("radio_pirep").checked) {
  alert('Please select status !');
  return false;
}
if (document.getElementById("radio_delay").checked) {
  var output_component = 'evaluation_status';
  $('#source').append(evaluation_status);
} else if (document.getElementById("radio_pirep").checked) {
  var output_component = 'solution_status';
  $('#source').append(solution_status);
}
if (output_component == 'evaluation_status') {
  var table = $("#source").DataTable({
    retrieve: true,
    pagin: false,
        //ajax with data to post
        dom: 'Bfrtip',
        buttons: [
        {extend: 'excel', title: 'Display Evaluation Status'},
        {extend: 'pdf', title: 'Display Evaluation Status', orientation: 'landscape',
        pageSize: 'A3'}
        ],
        aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
          "url": "<?= base_url('Sd/search_evaluation') ?>",
          "type": "POST",
          "data": {
            "number": number,
            "evaluation": evaluation,
            "operator": operator,
            "actype": actype,
            "keyword": keyword,
            "uic": uic,
            "crossreff": crossreff,
            "pic": pic,
            "date_to": date_to,
            "date_from": date_from,
            "alerttype": alerttype,
            "role_id": role_id,
            "ours": ours,
            "url": url,
            "onwatch": onwatch,
            "output_component": output_component,
          },
        },

        'columns': [{
          data: 'no'
        },
        {
          data: 'number'
        },
        {
          data: 'crossreff'
        },
        {
          data: 'actype'
        },
        {
          data: 'ata'
        },   
        {
          data: 'evaluationtype'
        },
        {
          data: 'EvalDate'
        },
        {
          data: 'problemtitle'
        },
        {
          data: 'srieffectivenees'
        },
        {
          data: 'PIC'
        },
        {
          data: 'TargetDate'
        },
        {
          data: 'status'
        },
        {
          data: 'remarks'
        },
        {
          data: 'file'
        }
        ]
      });
  $('body').addClass('mini-navbar');
} else if (output_component == 'solution_status') {
  var table = $("#source").DataTable({
    dom: 'Bfrtip',
    buttons: [
    {extend: 'excel', title: 'Display Status Solution Status'},
    {extend: 'pdf', title: 'Display Status Solution Status', orientation: 'landscape',
    pageSize: 'A3'},
    ],
    aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
    ajax: {
      "url": "<?= base_url('sd/search_solution') ?>",
      "type": "POST",
      "data": {
        "number": number,
        "evaluation": evaluation,
        "operator": operator,
        "actype": actype,
        "keyword": keyword,
        "uic": uic,
        "crossreff": crossreff,
        "pic": pic,
        "date_to": date_to,
        "date_from": date_from,
        "alerttype": alerttype,
        "role_id": role_id,
        "ours": ours,
        "url": url,
        "onwatch": onwatch,
        "output_component": output_component,
      },
    },
    'columns': [{
      data: 'no'
    },
    {
      data: 'number'
    },
    {
      data: 'crossreff'
    },
    {
      data: 'actype'
    },
    {
      data: 'ata'
    },
    {
      data: 'evaluationtype'
    },
    {
      data: 'EvalDate'
    },
    {
      data: 'problemtitle'
    },
    {
      data: 'PIC'
    }, 
    {
      data: 'namasolusi'
    },
    {
      data: 'uicsolusi'
    },
    {
      data: 'status_solusi'
    },
    {
      data: 'last_update'
    },
    {
      data: 'fil_sol'
    },
    {
      data: 'remarks_status'
    },
    ],
    'rowsGroup': [0, 1, 2, 3, 4, 5, 6, 7, 8]
  });


  var parsing_url = url + '?evaluation=' + evaluation + '&operator=' + operator + '&act=solusistatus';
      // $('#enakeun').html(html);
      $('body').addClass('mini-navbar');


    }
  });
function check(depir) {
  if(depir == "pirep"){

  }
  else if (depir == "delay"){
   

  }

}
</script>
