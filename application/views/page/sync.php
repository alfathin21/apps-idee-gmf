<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-database"></i>&nbsp; ADMINISTRATOR / Synchronization DATABASE TDAM</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
               <form action="<?= base_url('Batch') ?>" method="POST">
                  <button type="submit" name="tekan" class="btn btn-success">&nbsp;&nbsp;COPY DATA DATABASE TDAM &nbsp;&nbsp; <i class="fa fa-hand-o-left"></i></button>
               </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
