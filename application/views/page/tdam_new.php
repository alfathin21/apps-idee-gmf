<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-database"></i>&nbsp; PROBLEM DEFINITION / NEW ISSUE TDAM</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
             <div class="table-responsive">
                <table class="table table-bordered table-striped table-hovered" id="scrto">
                <thead>
                  <tr>
                    <th class="text-center">NO</th>
                    <th class="text-center">EVENT ID</th>
                    <th  class="text-center">AC TYPE </th>
                    <th  class="text-center">AC REG </th>
                    <th  class="text-center">ATA</th>
                    <th  class="text-center">FLT NO </th>
                    <th  class="text-center">PROBLEM TITLE</th>
                    <th  class="text-center">TARGET DATE</th>
                    <th  class="text-center">ACTION</th>
                  </tr>
                </thead>
                <tbody id="scr"></tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
         //datatables
        table = $('#scrto').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?= ('tdm/get_data_user')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
            "aLengthMenu" : [[5, 25, 50, -1], [5, 25, 50, "All"]]
 
        });
 
    });

</script>