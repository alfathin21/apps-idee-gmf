<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-upload"></i>&nbsp; Upload attachments file : <b><i>Please upload in format CSV,DOCX,DOC,PDF,JPG,PNG,JPEG</i></b></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-12">
             <form id="my-awesome-dropzone" class="dropzone">
              <div class="dropzone-previews"></div>
            </form>
            <div>
              <div class="m text-right">
                
                <button type="button" id="finish" class="btn btn-success"><i class="fa fa-folder-o"></i>&nbsp; Finished Uploading</button></div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var fileList = new Array;
  var i =0;
  var o; 
  var rl='<?= base_url('files/');?>';
  var foto_upload= new Dropzone(".dropzone",{
    url: "<?= base_url('Upload/proses_upload') ?>",
    maxFilesize: 9000,
    method:"post",
    paramName:"userfile",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
      this.on("success", function(file) {
        fileList[i] = {"name" : file.name, "fileId" : i};
        i++;
    
      });
    },
  });
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    c.append("token_file",a.token);
    fileList.pop();
  })
  foto_upload.on("removedfile",function(a){
    var token=a.token;
    $.ajax({
      type:"post",
      data:{token:token},
      url:"<?= base_url('pe/remove_file') ?>",
      cache:false,
      dataType: 'json',
      success: function(data){
       var rm = document.getElementById(data);
       $(rm).remove();
     },
     error: function(){
  

    }
  });
  });  
</script>

<script>
  $('#finish').on('click', function(){
       $.ajax({
                type:"post",
                url:"<?= base_url('upload/finish') ?>",
                success: function(){
                  document.location.href = '<?= base_url('upload');?>'; 
                }
  });
  });
</script>