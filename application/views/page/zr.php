 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>

 <div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-mail-reply-all"></i>&nbsp; PROBLEM DEFINITION / REV ISSUE</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <form class="form-horizontal form-label-left input_mask" method="post" action="<?= base_url('pd/savedraft') ?>">
            <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
               <input type="hidden" id="revisi" name="revisi" value="<?= $revisi['number'] ?>">
               <label for="">NUMBER : </label></label>
               <input type="hidden" name="file_gabung" value="<?= $revisi['file'] ?>">
               <input type="text" readonly="" class="form-control" value="<?= $number; ?>" id="number" required name="number" >
             </div>
             <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">CROSS REF :</label>
              <input readonly="" value="<?= $revisi['crossreff'] ?>" type="text" class="form-control" id="crossreff" name="crossreff" >
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">EVALUATION TYPE :</label>
              <input type="text" class="form-control" readonly="" id="evaluation2" name="evaluation" value="<?= $revisi['evaluationtype'] ?>">
            </div>
          </div>
          <br>
          <div class="row"> 
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">OPERATOR : </label>
              <select  selected class="form-control Operator" id="operator" name="operator" >
                <option selected readonly value="<?= $revisi['operator'] ?>"><?= cekoperator( $revisi['operator']) ?></option>
                <option disabled value="GA">GARUDA INDONESIA</option>
                <option disabled value="QG">CITILINK</option>
                <option disabled value="SJ">SRIWIJAYA</option>
                <option disabled value="IN">NAM AIR</option>
              </select> 
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">A/C Type : </label>
              <select id="actype" class="form-control selek2" name="actype"  >
                <option selected readonly value="<?= $revisi['actype'] ?>"><?= cekactype( $revisi['actype']) ?></option>
                <option disabled value="ATR">ATR72-600</option>
                <option disabled value="330">A330</option>
                <option disabled value="320">A320-200</option>
                <option disabled value="733">B737-300</option>
                <option disabled value="735">B737-500</option>
                <option disabled value="738">B737-800</option>
                <option disabled value="739">B737-900</option>
                <option disabled value="73M">B737-MAX</option>
                <option disabled value="777">B777-300</option>
                <option disabled value="CRJ">CRJ1000</option>
              </select>    
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <label for="">ALERT TYPE SRI : </label>
              <input type="text" class="form-control" readonly="" id="" name="alert" value="<?= $revisi['alert'] ?>">  
            </div>
          </div>
          <br>
          <div class="row"> 
            <div class="col-md-2 col-sm-6 col-xs-12">
              <label for="">A/C REG : </label>
              <input readonly="" value="<?= $revisi['acreg'] ?>" type="text" class="form-control" id="acreg" name="acreg" >
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
             <label for="">ATA : </label>
             <input readonly="" type="text" value="<?= $revisi['ata'] ?>" class="form-control" id="ata" name="ata">
           </div>
           <div class="col-md-8 col-sm-6 col-xs-12">
             <label for="">SYSTEMS : </label>
             <input type="text" readonly="" class="form-control" value="<?= $revisi['systems'] ?>" id="system" name="system">
           </div>
         </div>
         <br>
         <div class="row" >  
           <div class="col-md-12 col-sm-12 col-xs-12">
             <label for="">PROBLEM TITLE : </label>
             <input readonly="" value="<?= $revisi['problemtitle'] ?>"  type="text" class="form-control" name="problem_title" id="problem_title" >
           </div>
         </div>
         <br>
         <div class="row" >
          <div class="col-md-12 col-sm-12 col-xs-12">
            <label for="">DETAIL OF PROBLEM :</label>
            <textarea name="editor" id="detail" class="editor"><?= $revisi['detailproblem'] ?></textarea>
          </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-6">
            <label for="">PIC ASSIGN * :</label>
            <select required="" name="pc[]" id="pc" class="form-control" multiple="">
              <?php
              $new_string = rtrim($revisi['PIC'],',');
              $e  = explode(',', $new_string);

              ?>
              <?php for ($i=0; $i < count($e) ; $i++) {  ?>
                <option value="<?= $e[$i] ?>" <?= 'selected' ?>> <?= $e[$i] ?> </option>
              <?php }; ?>   
              <?php foreach ($te as $key) : ?>
                <option value="<?= $key['EMAIL'] ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="col-md-6">
           <label for="">CC :</label>
           <select required="" name="cc[]" id="cc" class="form-control" multiple="">
            <?php
            $new_string = rtrim($revisi['cc'],',');
            $e  = explode(',', $new_string);
            ?>
            <?php foreach ($e as $key )  :?>
              <option value="<?= $key ?>" <?= 'selected' ?>> <?= $key?> </option>
            <?php endforeach; ?>   
            <?php foreach ($te as $key) : ?>
              <option value="<?= $key['EMAIL'] ?>"><?= $key['nopeg'].' | '.$key['nama'].' | '. str_replace('JKT', '', $key['unit']).' | '.$key['jabatan'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>  
      <br>
      <div class="row" >
        <div class="col-md-7 col-sm-6 col-xs-12">
         <label for="">RELATED EVALUATION :</label>
         <input type="text" autocomplete="off" class="form-control" name="releated" id="releated" >
       </div>
       <div class="col-md-3 col-sm-6 col-xs-12">
        <label for="">TARGET DATE : </label>
        <input type="date"  class="form-control has-feedback-left" id="target_date" name="target_date" value="<?= strftime('%Y-%m-%d', strtotime($revisi['TargetDate'])); ?>">

      </div>
      <div class="col-md-2 col-sm-6 col-xs-12">
        <label for="">UIC : </label>
        <select  class="form-control Operator" id="uic"  name="uic">
          <option selected="" value="<?= $revisi['UIC'] ?>"><?= $revisi['UIC'] ?></option>
          <option value="TEA-1">TEA-1</option>
          <option value="TEA-2">TEA-2</option>
          <option value="TEA-3">TEA-3</option>
          <option value="TEA-4">TEA-4</option>
          <option value="TER-1">TER-1</option>
          <option value="TER-2">TER-2</option>
          <option value="TER-3">TER-3</option>
          <option value="TER-4">TER-4</option>
          <option value="TER-5">TER-5</option>
          <option value="TED-1">TED-1</option>
          <option value="TED-2">TED-2</option>
          <option value="TED-3">TED-3</option>
          <option value="TED-4">TED-4</option>
          <option value="TED-5">TED-5</option>
        </select>
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-6">
        <label for=""> ATTACHMENTS :</label>
        <br>
        <button data-toggle="modal" type="button" data-target="#upload" style="background: #337AB7; color: white" class="form-control" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; <b>UPLOAD</b></button>
      </div>
      <div class="col-md-6">
       <label for=""> ATTACHMENTS FILE  :</label>
       <table class="table table-striped" id="skuy">
        <?php if (!empty($revisi['file'] )) : ?>
          <?php 
          $new_s = rtrim($revisi['file'],',');
          $exp = explode(',',$new_s);
          ?>
          <?php  for($i = 0; $i < count($exp); $i++){
            $row= $this->Getdat_model->getfile($exp[$i]); 
            ?>
            <tbody>
                <?php if ($row != NULL or !empty($row)): ?>
                         <?php if ($row['nama_baru'] == null) {  
                    $link = $row['nama_file']; 
                  } else {
                    $link = $row['nama_baru'];
                  } ?>

               <tr id="<?= $row['nama_file']; ?>">
                <td><button class="badge badge-success" type="button" id="file_open" data-toggle="modal" data-target="#modal2" data-target="#modal" data-draft="<?= $link; ?>"><?= $row['nama_file']; ?></button></td>
                <td>
                  <a id="opopop" type="button" data-unik1="<?= $revisi['number'] ?>" data-delete="<?= $row['nama_file'] ?>" data-unik2="<?= $row['token'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
          </tbody>
        <?php } ?>     
      <?php endif ?>
    </table>
  </div>
</div>

<br>
<hr>
<div class="row" >
  <div class="col-md-5"></div>
  <div class="col-md-6">
    <button name="simpan_draft" type="submit" class="btn btn-primary" style="margin-right: 15px;"><i class="fa fa-save"></i>&nbsp;&nbsp; SAVE</button>
    <button  name="assign" type="submit" class="btn btn-success"><i class="fa fa-send-o"></i>&nbsp;&nbsp; ASSIGN</button>
  </div>
  
</div>

</form>
</div>
</div>
</div>
</div>
</div>


<script>
 CKEDITOR.replace( 'editor' );
  CKEDITOR.config.removePlugins = 'elementspath';
   $(document).on("click", "#opopop", function () {
    var number = $(this).data('unik1');
    var file = $(this).data('unik2');
    var ui = $(this).data('delete');
    $.ajax({
     type:"POST",
     data:{number:number,file:file,ui:ui},
     url:"<?= base_url('Ps/deleteattach') ?>",
     dataType: 'json',
     success: function(data){
        var sao = document.getElementById(data);
        $(sao).remove();
     }
   });
  });
</script>