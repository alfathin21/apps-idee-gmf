<div class="footer">
  <div class="pull-right">
    PT. GMF AeroAsia - Reliability Management <strong>Copyright</strong> &copy; 2019  V. 1.0
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="exits" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-search"></i> &nbsp; SEARCH SOLUTIONS BASED ON ACTYPE AND ATA CODE</h4>
      </div>
      <form method="POST" action="<?= base_url('zr') ?>">
        <div class="modal-body">
          <div class="row">
           <div class="col-md-4">
            <div class="form-group ">
              <label for="">A/C TYPE * : </label> 
              <select required="" id="typeac" class="form-control typeac" name="typeac"  required="" >
                <option value="">SELECT ACTYPE</option>
                <option value="ATR">ATR72-600</option>
                <option value="330">A330</option>
                <option value="320">A320-200</option>
                <option value="733">B737-300</option>
                <option value="735">B737-500</option>
                <option value="738">B737-800</option>
                <option value="739">B737-900</option>
                <option value="73M">B737-MAX</option>
                <option value="777">B777-300</option>
                <option value="CRJ">CRJ1000</option>
              </select>    
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="">ATA * :</label>
              <input type="number" class="form-control" maxlength="2" required="" id="atacode" name="atacode">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <br>
              <button style="margin-top:5px" type="button" id="ss"  class="btn btn-success"><i class="fa fa-search"></i>  SEARCH SOLUTIONS </button>
            </div>
          </div> 
        </div>
        <br>
        <hr>
        <h4 class="text-center">SEARCH RESULT</h4>
        <hr>
        <div class="row">
          <div class="col-md-12" id="table-solusi">
            <table class="table table-responsive  table-striped" id="result_solutions">
              <thead>
                <tr>
                  <th>NUMBER</th>
                  <th>SOLUTIONS</th>
                  <th>UIC</th>
                  <th>TARGET DATE</th>
                  <th>REVISI DATE</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody id="show">
              </tbody>
            </table>
          </div> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      </div>
    </form>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!-- akhir modal -->

<!-- modal2 -->
<!-- akhir modal -->
<div class="modal fade" id="modal3" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-search"></i> &nbsp; </h4>
      </div>
      <form>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <img class="img img-thumbnail" id="prev3">
          </div>
        </div>
        <div class="modal-footer">
          <a  class="btn btn-primary" target="_blank" id="file_open3"><i class="fa fa-arrow-right"></i> Open File</button>
            <a class="btn btn-success" target="_blank"  id="download_file3"><i class="fa fa-download"></i> Download</a>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- akhir modal 2 -->
<!--  -->

<!-- akhir modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-search"></i> &nbsp; </h4>
      </div>
      <form>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <img class="img img-thumbnail" id="view_rev">
          </div>
        </div>
        <div class="modal-footer">
          <a  class="btn btn-primary" target="_blank" id="ol"><i class="fa fa-arrow-right"></i> Open File</button>
            <a class="btn btn-success" target="_blank"  id="dl"><i class="fa fa-download"></i> Download</a>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- akhir modal 2 -->

<!--  -->


<!-- pe -->
<!-- modal -->
<div class="modal fade" id="search" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-search"></i> &nbsp; SEARCH NUMBER PROBLEM DEFINITION</h4>
      </div>
      <form method="POST" action="<?= base_url('zr') ?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="">NUMBER : </label>
            <input autofocus="" required="" type="text" autocomplete="off" class="form-control" name="number">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit" name="search"><i class="fa fa-search"></i> Search</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!-- akhir modal -->
<!-- modal -->
<!-- modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-search"></i> &nbsp; </h4>
      </div>
      <form method="POST" action="<?= base_url('zr') ?>">
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <img src="" id="prev" class="img img-thumbnail">
          </div>
        </div>
        <div class="modal-footer">
          <a  class="btn btn-primary" target="_blank" id="open_file"><i class="fa fa-arrow-right"></i> Open File</button>
            <a class="btn btn-success" target="_blank"  id="download"><i class="fa fa-download"></i> Download</a>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- akhir modal -->

<!-- modal -->
<div class="modal fade" id="upload" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-upload"></i> &nbsp;  Click or Drop your Attachment !</h4>
      </div>
      <div class="modal-body">  
        <div class="dropzone">
          <div class="dz-message">
           <h3> <i class="fa fa-files-o"></i> <b>Click or Drop  Files</b> to upload <i class="fa fa-hand-o-left"></i></h3>
         </div>
       </div>

     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
    </div>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!-- akhir modal -->

<!-- problem search -->


<!--problem search -->
<script src="<?= base_url()?>assets/jq.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url()?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= base_url()?>assets/js/inspinia.js"></script>
<script src="<?= base_url()?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/gritter/jquery.gritter.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= base_url()?>assets/source/select_source.js"></script>
<script src="<?= base_url()?>assets/select2/select2.full.min.js"></script>
<script src="<?= base_url()?>assets/sweet.js"></script>
<script src="<?= base_url()?>assets/dataTables.rowsGroup.js"></script>
<script src="<?= base_url()?>assets/source/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/source/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/source/jszip.min.js"></script>
<script src="<?= base_url()?>assets/source/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/source/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/source/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/source/buttons.print.min.js"></script>


