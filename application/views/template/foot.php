<script src="<?= base_url()?>assets/sweet.js"></script>
<script src="<?= base_url()?>assets/ckeditor/ckeditor.js"></script>
<script>
const flashData = $('.flash-data').data('flashdata');
 if (flashData == 'Evaluation status is update') {
     Swal({
    title: 'Success',
    text:   'Evaluation status is update !',
    type: 'success'
  });
 }
 if(flashData == 'Problem is update') {
     Swal({
    title: 'Success',
    text:   'Problem is update',
    type: 'success'
  });
 } 
 if(flashData == 'Upload file success') {
     Swal({
    title: 'Success',
    text:   'Upload file success',
    type: 'success'
  });
 }
 if(flashData == 'Evaluation not found !') {
     Swal({
    title: 'Warning !',
    text:   'Number evaluation is not found !',
    type: 'warning'
  });
 } 
 if(flashData == 'Number draft not found !') {
     Swal({
    title: 'Warning !',
    text:   'Number draft is not found !',
    type: 'warning'
  });
 }
 if(flashData == 'Access forbiden !') {
     Swal({
    title: 'Sorry !',
    text:   'Access page forbiden !',
    type: 'warning'
  });
 }
 if(flashData == 'PROBLEM EVALUATION IS SEND') {
     Swal({
    title: 'Success',
    text:   'Problem Evaluation success to send ',
    type: 'success'
  });
 }

if(flashData == 'Draft not is Update !') {
     Swal({
    title: 'Error !',
    text:   'Draft not is Update !',
    type: 'error'
  });
 }
 
 if(flashData == 'Draft is Update !') {
     Swal({
    title: 'Success !',
    text:   'Draft is Update !',
    type: 'success'
  });
 }
if(flashData == 'Problem successfully deleted') {
     Swal({
    title: 'Success',
    text:   'Problem successfully deleted',
    type: 'success'
  });
 }
 if(flashData == 'Akses berhasil diganti') {
     Swal({
    title: 'Success',
    text:   'Access role changed !',
    type: 'success'
  });
 }
 if(flashData == 'Role success delete') {
     Swal({
    title: 'Success',
    text:   'Role success deleted !',
    type: 'success'
  });
 }
  if(flashData == 'Role user change') {
     Swal({
    title: 'Success',
    text:   'Role user change',
    type: 'success'
  });
 }  
 if(flashData == 'Problem Definition TDAM is save to draft !') {
     Swal({
          title: 'Success',
          text:   'Problem Definition TDAM is save to draft !',
          type: 'success'
      });
 }
if(flashData == 'Successful Synchronization') {
     Swal({
          title: 'Success',
          text:   'Successful Synchronization !',
          type: 'success'
      });
 }
 if(flashData == 'Update Success !') {
     Swal({
          title: 'Success',
          text:   'Update Success to HIDDEN !',
          type: 'success'
      });
 }
  if(flashData == 'Update Success Open !') {
     Swal({
          title: 'Success',
          text:   'Update Success to OPEN !',
          type: 'success'
      });
 }  
 if(flashData == 'Save Success !') {
     Swal({
          title: 'Success',
          text:   'Save Success !',
          type: 'success'
      });
 }
 $('#problem_title').on('keypress', function (e) {
   var x = document.getElementById("problem_title");
   x.value = x.value.toUpperCase();
 });
  $('#problem_title').on('change', function (e) {
   var x = document.getElementById("problem_title");
   x.value = x.value.toUpperCase();
 });
 $('#acreg').on('change', function (e) {
   var x = document.getElementById("acreg");
   x.value = x.value.toUpperCase();
 });
 function generate(){
  var operator= $('#operator').val();
  var actype= $('#actype').val();
  var ata = $('#ata').val();
  $.ajax({
    url: "<?= base_url('pd/generate'); ?>",
    type: "POST",
    dataType: "JSON",
    data: {operator: operator,actype: actype},
    success: function (response) {
     document.getElementById("number").value = operator+"-"+actype+"-"+ata+"-"+response+"-"+"00";
     document.getElementById("urutan").value = response;
   },
   error: function (jqXHR, textStatus, errorThrown) {

   }
 });
}
$('#ata').on('change', function (e) {
  var actype = $('#actype').val();
  var operator = $('#operator').val();
  if (actype == '' | operator == '') {
    alert('Please select actype');
    $('#ata').focus();
    return false;
  } else if (operator == ''){
    alert('Please select operator');
    $('#ata').focus();
    return false;
  } else {
   generate(); 
 }
})
$('#ss').click(function(){
  var ata = $('#atacode').val();
  var typeac = $('#typeac').val();
  $.ajax({
    url: "<?= site_url('pe/getsolusi'); ?>",
    type: "POST",
    dataType: "JSON",
    data: {ata: ata,typeac: typeac},
    success: function (data) {
      var html = '';
      var i;   
      if (data == '') {
        html += '<tr>'+
        '<td class="text-center" colspan="7">SOLUTIONS NOT FOUND !</td>'+
        '</tr>';
        $('#atacode').val('');
        $('#typeac').val('');
        $('#show').html(html);
      } else {
        for(i=0; i<data.length; i++){
          html += '<tr>'+
          '<td>'+data[i].numbersolution+'</td>'+
          '<td>'+data[i].solution+'</td>'+
          '<td>'+data[i].uic+'</td>'+
          '<td>'+data[i].targetdate+'</td>'+
          '<td>'+data[i].revisidate+'</td>'+
          '<td style="text-align:center;">'+  
          '<a class="btn btn-primary edit-solusi btn-sm" data-id="'+data[i].numbersolution+'" data-solution="'+data[i].solution+'" data-uic="'+data[i].uic+'" data-targetdate="'+data[i].targetdate+'" data-revisidate="'+data[i].revisidate+'"><i class="fa fa-plus"></i></aa>'+
          '</td>'+
          '</tr>';
        }
        $('#atacode').val('');
        $('#typeac').val('');
        $('#show').html(html);
      }
    }      
  });
});
$('#table-solusi').on("click", ".edit-solusi", function(e){
  e.preventDefault();
  var solusinumber =  $(this).data('id');
  var solusi =  $(this).data('solution');
  var uic =  $(this).data('uic');
  var targetdate =  $(this).data('targetdate');
  var revisidate =  $(this).data('revisidate');
  tambah2(solusinumber,solusi,uic,targetdate,revisidate);
    alert('Solution successfully added !');
});



if (flashData == 'Number problem is not found !' || flashData == 'Please insert number !' || flashData == 'Solutions saved !') {
  Swal({
    title: 'Sorry !',
    text:   flashData,
    type: 'error'
  }); 
} else if (flashData == 'E-mail is send !') {
 Swal({
  title: 'Success !',
  text:   flashData,
  type: 'success'
});
} else if (flashData == 'Solution was successfully added !'){
 Swal({
  title: 'Success !',
  text:   flashData,
  type: 'success'
});
} else if (flashData == 'Number Draft is not found !'){
    Swal({
        title: 'ERROR !',
        text:   'Number Draft is not found !',
        type: 'error'
      });
}else if (flashData == 'Please Insert Number Draft !'){
    Swal({
        title: 'ERROR !',
        text:   'Number Draft is not found !',
        type: 'error'
      });
}
   if (flashData == 'Draft is Saved !') {
     Swal({
      title: 'Success !',
      text:   'Draft is saved !',
      type: 'success'
    });
   }
if (flashData == 'ASSIGN IS SEND') {
     Swal({
      title: 'Success !',
      text:   'ASSIGN IS SEND !',
      type: 'success'
    });

   }
</script>
<script>
  $(document).ready(function() {
   $('#evaluation').select2({
    placeholder: "Select Evaluation Type",
    allowClear: true
  });
   $('#operator').select2({
    placeholder: "Select Operator",
      allowClear: true

      });
   $('#operator2').select2({
     placeholder: "Select Operator",
       allowClear: true
   });
   $('#uic3').select2({
    placeholder: "Select UIC",
    allowClear: true
  }); 
    $('#uic').select2({
    placeholder: "Select UIC",
    allowClear: true
  });

$('#pc').select2({
  placeholder: 'Select PIC',
  multiple: true,
  ajax: {
    url: '<?= base_url('Te') ?>',
    dataType: 'json',
    delay: 150,
    processResults: function (data) {
      return {
        results: data
      };
    },
    cache: true
  }
});
$('#pic3').select2({
  placeholder: 'Select PIC',
  multiple: true,
  ajax: {
    url: '<?= base_url('Te') ?>',
    dataType: 'json',
    delay: 150,
    processResults: function (data) {
      return {
        results: data
      };
    },
    cache: true
  }
});
$('#cc3').select2({
  placeholder: 'Select PIC',
  multiple: true,
  ajax: {
    url: '<?= base_url('Te') ?>',
    dataType: 'json',
    delay: 150,
    processResults: function (data) {
      return {
        results: data
      };
    },
    cache: true
  }
});

$('#cc').select2({
  placeholder: 'Select PIC',
  multiple: true,
  ajax: {
    url: '<?= base_url('Te') ?>',
    dataType: 'json',
    delay: 150,
    processResults: function (data) {
      return {
        results: data
      };
    },
    cache: true
  }
});




$('#pic').select2({
  placeholder: 'Select PIC',
  multiple: true,
  ajax: {
    url: '<?= base_url('Te') ?>',
    dataType: 'json',
    delay: 150,
    processResults: function (data) {
      return {
        results: data
      };
    },
    cache: true
  }
});
   $('#actype').select2({
    placeholder: "Select AC Type",
      allowClear: true

  });
   $('#alert').select2({
    placeholder: "Select Alert",
    allowClear: true
  });
 
   $('#crossreff').select2({
     placeholder: "Select Event ID",
     allowClear: true
   });
   $('#pic').select2({
    placeholder: "Select PIC"
  }); 

   $('#uic').select2({
    placeholder: "Select UIC",
    allowClear: true
  });

   $('#typeac').select2({
    placeholder: "Select PIC",
    allowClear: true
  });

   
   $('#table').DataTable({
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
    {extend: 'excel', title: 'ExampleFile'},
    {extend: 'pdf', title: 'ExampleFile'},
    ],
    aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]]
  });  
   $('#tdam').DataTable({
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
    {extend: 'excel', title: 'ExampleFile'},
    {extend: 'pdf', title: 'ExampleFile'},
    ],
    aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]]
  });

   $('#ts').DataTable({
      aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]]
   });
    $('#ts2').DataTable({
      aLengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]]
   });
   $('#solution').DataTable();
   $('#xhr').DataTable();
   $('#pb_source').DataTable({
     "aLengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
   });

   $('#pd_source').DataTable({
    "aLengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
  });
   $('#zhr').DataTable();
   $('#pro_search').DataTable();
   const flashData = $('.flash-data').data('flashdata');

 });
</script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var fileList = new Array;
  var i =0;
  var o; 
  var rl='<?= base_url('files/');?>';
  var foto_upload= new Dropzone(".dropzone",{
    url: "<?= base_url('pe/proses_upload') ?>",
    maxFilesize: 4,
    method:"post",
    paramName:"userfile",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
     this.on("success", function(file, serverFileName,responseURL) {
        var l = 0;
        var i =  responseURL.currentTarget;
        var b = i.response;
        var c = JSON.parse(b);
        var d = c.nama_awal;
        var e = c.nama_baru;
        var k;
        fileList[l] = {"nama_file" : d, "fileId" : l, "nama_filebaru" : e};
        l++;
        for (k in fileList)
        {
          var hasil = fileList[k]['nama_file'];
          var hasil_akhir = fileList[k]['nama_filebaru'];
          var stre121 = '<tr><td><button class="badge badge-success" type="button" id="'+hasil+'" data-toggle="modal" data-target="#modal" data-pre="'+hasil_akhir+'">'+hasil+'</button></td></tr>';
          $("#skuy").append(stre121);
          fileList.pop();
          var idPE = document.getElementById(hasil);
          $(idPE).click(function(){
            var o = $(this).data('pre');
            $('#prev').attr('src', rl+o);
            $("#open_file").attr("href", rl+o);
            $("#download").attr("href", rl+o);
          }); 
        }


      });
    },
  });
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    a.number= $("#number").val();
    c.append("token_file",a.token);
    c.append("number",a.number);
    fileList.pop();
  })
  foto_upload.on("removedfile",function(a){
    var token=a.token;
    $.ajax({
      type:"post",
      data:{token:token},
      url:"<?= base_url('pe/remove_file') ?>",
      cache:false,
      dataType: 'json',
      success: function(data){
       var rm = document.getElementById(data);
       $(rm).remove();
     },
     error: function(){
  

    }
  });
  });  
  $("#skuy").on("click", "#open", function(e) {
    e.preventDefault();
    var rl='<?= base_url('files/');?>';
    var o = $(this).data('pre2');
    $('#prev2').attr('src', rl+o);
    $("#open_file2").attr("href", rl+o);
    $("#download2").attr("href", rl+o);
  }); 

   $("#skuy").on("click", "#file_open", function(e) {
    e.preventDefault();
    var rl='<?= base_url('files/');?>';
    var o = $(this).data('draft');
    $('#view_rev').attr('src', rl+o);
    $("#ol").attr("href", rl+o);
    $("#dl").attr("href", rl+o);
   
  });
  $("#skuy").on("click", "#open3", function(e) {
    e.preventDefault();
    var rl='<?= base_url('files/');?>';
    var o = $(this).data('pre3');
    $('#prev3').attr('src', rl+o);
    $("#file_open3").attr("href", rl+o);
    $("#download_file3").attr("href", rl+o);
    
  });
</script>


</body>
</html>