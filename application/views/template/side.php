<body>
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" style="position:fixed" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav metismenu sticky-top" id="side-menu">
          <!-- menu nama admin -->
          <li class="nav-header" >
            <div class="dropdown profile-element"> <span>
              <?php if ($this->session->userdata('unit') == 'GA') :?>
                <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_ga.jpeg" width="63" />
                <?php elseif ($this->session->userdata('unit') == 'QG') : ?>
                  <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_qg.jpg" width="63" />
                  <?php elseif ($this->session->userdata('unit') == 'Full access') : ?>
                    <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/images/user.png" width="63x" />
                    <?php elseif ($this->session->userdata('unit') == 'SJ') : ?>
                      <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_sj.jpg" width="63" />
                      <?php elseif ($this->session->userdata('unit') == 'IN') :?>
                        <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_in.jpg" width="63" />
                        <?php elseif ($this->session->userdata('unit') == 'OT') :?>
                          <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/user.png" width="63" />
                          <?php else : ?>
                            <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="https://talentlead.gmf-aeroasia.co.id/images/avatar/<?php echo $this->session->userdata('nopeg');?>.jpg" width="63x" />
                          <?php endif; ?>
                        </span>
                        <a class="dropdown-toggle" href="#">
                          <span class="clear"> <span class="block m-t-xs"> <strong style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="font-bold"><?= $this->session->userdata('nama');?></strong>
                          </span>
                          <?php if ($this->session->userdata('unit') == 'GA') : ?>
                            <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : GARUDA </b></span> </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'QG') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : CITILINK </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'SJ') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : SRIWIJAYA </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'IN') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : NAM AIR </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'OT') : ?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">UNIT : OTHERS</b></span> </span> </a>
                              <?php else : ?>
                               <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">UNIT : <?= $this->session->userdata('unit') ?></b></span> </span> </a>
                             <?php endif; ?>
                           </div>
                         </li>
                         <!-- QUERY MENU -->
                         <?php
                         $role_id = $this->session->userdata['role_id'] ;
                         $queryMenu = "SELECT `user_menu`.`id`, `menu`,`icon_menu`,`url2`
                         FROM `user_menu` JOIN `user_access_menu`
                         ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                         WHERE `user_access_menu`.`role_id` = $role_id
                         ORDER BY `user_access_menu`.`menu_id` 
                         ";
                         $menu = $this->db->query($queryMenu)->result_array();
                         ?>
                         <?php foreach ($menu as $key) :  ?>
                          <li style="margin-top: -3px;">
                           <a href="<?= $key['url2'] ?>"><i class="<?= $key['icon_menu'] ?>"></i> <span class="nav-label"><?= $key['menu'] ?></span> <span class="fa arrow"></span></a>
                           <?php 
                           $m_id = $key['id'] ;
                           $querysubmenu = "SELECT *
                           FROM `user_sub_menu` JOIN `user_menu`
                           ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                           WHERE `user_sub_menu`.`menu_id` = $m_id
                           AND `user_sub_menu`.`is_active` = 1
                           ";
                           $submenu = $this->db->query($querysubmenu)->result_array();
                           if ($submenu == '' or empty($submenu)) : 
                            ?>
                          </li>
                          <?php else : ?>
                           <ul class="nav nav-second-level collapse">
                             <?php foreach ($submenu as $yox) : ?>
                              <?php if ($yox['title'] == 'Rev Issue') : ?>
                               <li ><a  data-toggle="modal" data-target="#search"><i class="<?= $yox['icon'] ?>"></i> <?= $yox['title'] ?></a></li>
                               <?php else : ?>
                                <li ><a href="<?= $yox['url'] ?>"><i class="<?= $yox['icon'] ?>"></i> <?= $yox['title'] ?></a></li>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </ul>
                        </li>
                      <?php endif; ?> 
                    <?php endforeach; ?>
                    <!-- Akhir Menu -->
                    </ul>
                  </div>
                </nav>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                  <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                      <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                      </div>
                      <ul class="nav navbar-top-links navbar-right">
                        <!-- Notif E-mail -->
                        <?php if ($role_id == 8) :  ?>
                          <li>
                            <a href="<?= base_url('auth/logout') ?>">
                              <i class="fa fa-sign-out"></i> Log out
                            </a>
                          </li>
                          <?php else : ?>
                            <li class="dropdown">
                              <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i>  <span class="label label-warning">
                                  <?php
                                  $count =  $this->Getdat_model->getproblem($this->session->userdata('email'));
                                  echo count($count); 
                                  ?>
                                </span>
                              </a>
                              <ul class="dropdown-menu dropdown-messages">
                                <?php 
                                $hasil = $this->Getdat_model->getproblem($this->session->userdata('email')); 
                                ?>
                                <?php foreach ($hasil as $key ) : ?>
                                  <li>
                                    <div class="dropdown-messages-box">
                                      <div class="media-body">

                                       <strong>Problem Number :</strong><a href="<?= base_url('follow?number=')?><?= $key['number'] ?>"><?= $key['number'] ?></a><br>
                                     </div>
                                   </div>
                                 </li>
                                 <li class="divider"></li>
                               <?php endforeach; ?>
                             </ul>
                           </li>
                           <!-- notif message -->
                           <li>
                            <a href="<?= base_url('auth/logout') ?>">
                              <i class="fa fa-sign-out"></i> Log out
                            </a>
                          </li>

                        <?php endif; ?>

                      </ul>
                      <!-- Akhir notif -->
                    </nav>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">